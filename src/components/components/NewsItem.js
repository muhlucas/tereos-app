import React, { Component } from 'react';
import {
    StyleSheet,
    TouchableOpacity,
    View,
    Text,
    Image,
    ImageBackground,
    Dimensions
} from 'react-native';
import ImageBox from './ImageBox';

var width = Dimensions.get('window').width;
var height = Dimensions.get('window').height;

export default class NewsItem extends Component {

  constructor(props) {
      super(props);

      this.state = {

      }

      this.onClick = this.onClick.bind(this);
  }

  onClick() {
    if(typeof this.props.onClick != 'undefined') {
      this.props.onClick();
    }
  }

  render() {
    const { title, content, image, onNext } = this.props;

    return (
         <TouchableOpacity style={styles.cardBox}
          onPress={onNext} >
              <ImageBox style={styles.img}
                  image={image}
                  background={true}
              >
                <View>
                  <Text style={styles.textTitle}>{title}</Text>
                  <Text style={styles.textContent}>{content.toUpperCase()}</Text>
                </View>
              </ImageBox>
        </TouchableOpacity>
      );
    }
  }

const styles = StyleSheet.create({
  cardBox: {
    marginBottom:20,
    borderRadius: 80,
    alignItems:'center'
  },
  img: {
    width:'90%',
    paddingBottom:20
  },
  /* img:{
    width:'90%',
    borderRadius: 5,
    marginRight: 20,
    marginLeft: 20,
    position:'absolute',
    flex:1,
  }, */

  textTitle:{
    fontSize:9,
    color:'white',
    marginTop:180,
    marginLeft:20,
    fontFamily: 'Montserrat',
  },

  textContent:{
    fontSize:15,
    color:'white',
    marginTop:1,
    marginLeft:20,
    fontWeight:'bold',
    width:'70%',
    lineHeight:24,
    fontFamily: 'Montserrat',
  },
});
