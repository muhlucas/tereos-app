import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { StyleSheet,View,TextInput,Text ,ScrollView, TouchableWithoutFeedback, Dimensions} from 'react-native';
import { Col, Row, Grid } from 'react-native-easy-grid';
import Image from 'react-native-remote-svg';
import ReportsHomeButton from '../../components/ReportsHomeButton';
import AnimateNumber from 'react-native-animate-number';
import Chart from '../../libs/component/Chart';
import HomeTab from '../../../routing/tabs/HomeTab';

var width = Dimensions.get('window').width;
var height = Dimensions.get('window').height;

export default class ReportsHome extends Component {

  render() {
    const { navigate, state } = this.props.navigation;

    return (
      <View style={{backgroundColor:'#F7F7F7'}}>
        <ScrollView style={styles.scrollViewContainer}>
          <View>
            <Text style={styles.title}>Relatórios</Text>
          </View>
          <ReportsHomeButton
            onClick={() => { navigate('FinancialReports', {title: 'Relatórios Financeiros', back: 'ReportsHomeScreen'}); }}
            image={require('../../../assets/img/icon_relatorios_financeiros.png')}
            color={'#005596'}
            title={'Relatórios Financeiros'}
          />
          <ReportsHomeButton
            onClick={() => { navigate('ReportsByFarm', {title: 'Produção por fazenda', back: 'ReportsHomeScreen'}); }}
            color={'#3ca9dd'}
            image={require('../../../assets/img/icon_planta_branco.png')}
            title={'Relatório de Produção por fazenda'}
          />
          <ReportsHomeButton
            onClick={() => { navigate('DailyReport', {title: 'Produção diário', back: 'ReportsHomeScreen'}); }}
            color={'#fbb913'}
            image={require('../../../assets/img/icon_relatorio_producao_diario.png')}
            title={'Relatório de Produção diário'}
          />
        </ScrollView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
    scrollViewContainer: {

    },
    container: {
      justifyContent:'center',
      alignItems:'center',
    },
    topContainer: {
        flexDirection: 'row',
        margin:0,
        marginTop:10,
        justifyContent:'space-around',
        alignItems: 'center',
        marginBottom:10,

    },
    tab: {
        padding:0,
        alignItems: 'center',

    },
    activeIconStyle: {

    },
    inactiveIconStyle: {

    },

    activeTextStyle: {

    },
    inactiveTextStyle: {

    },
    img:{
      borderColor:'#D4D4D4',
      width:190,
      height:52
    },
    title:{
      fontSize:23,
      marginTop:10,
      marginBottom:15,
      marginLeft:15,
      fontWeight:'bold',
      color:'#005499',
      fontFamily: 'Montserrat',
    },
    rate:{
      fontSize:13,
      fontWeight:'bold',
      color:'black',
      fontFamily: 'Montserrat'
    },
    middle:{
      backgroundColor:'#EBEBEB',
      alignItems:'center',
      justifyContent:'center',
      fontFamily: 'Montserrat'
    },
    middleTitle:{
      color:'#005499',
      fontSize:32,
      fontWeight:'bold',
      paddingTop:20,
      paddingBottom:8,
      alignItems:'center',
      justifyContent:'center',
      fontFamily: 'Montserrat'
    },
    accountimg:{
      width:45,
      height:45,
      justifyContent: 'flex-end',
      alignItems:'flex-end',
      marginRight:30,
      fontFamily: 'Montserrat',
    },
    account:{
      justifyContent: 'flex-end',
      alignItems:'flex-end',
      marginTop:10,
      height:60
    },
    Luis:{
      justifyContent: 'flex-end',
      alignItems:'flex-end',
      color:'#005499',
      fontSize:14,
      marginRight:20
    },
    cardBox: {
      marginTop:18,
      marginBottom:18,
    },
    NameItem:{
      backgroundColor:'white',
      paddingTop:22,
      paddingBottom:22,
      paddingLeft:20,
      paddingRight:20,
      marginLeft:15,
      marginRight:15,
      justifyContent:'center',
      alignItems:'center',
      borderRadius:4,
      borderColor:'#F2F6F7',
      borderWidth:2

    },
    name:{
      fontSize:10,
      color:'black',
      fontFamily: 'Montserrat',
    },
    boxTitle: {
      fontSize:12,
      color:'black',
      fontFamily: 'Montserrat',
      fontWeight:'bold',
      marginBottom:5
    },
    goalName:{
      color:'#00D5F7',
      fontSize:50,
      fontWeight:'bold',
      marginTop:-8,
      fontFamily: 'Montserrat',
    },
    ton:{
      fontWeight:'bold',
      fontSize:20,
      color:'#005499',
      fontFamily: 'Montserrat',
    },
    goalton:{
      color:'#005499',
      fontSize:12,
      fontWeight:'bold',
      fontFamily: 'Montserrat',
    },
    row:{
      flexDirection:'row',
      alignItems:'center',
      justifyContent:'space-between',
      marginTop:-20,
      marginLeft:15,
      marginRight:15,
      flex:1
    },
    column: {
      flexDirection:'row',
      width:width
    },
    gridRowContent: {
      fontSize:8,
      padding:0,
      margin:0
    },
    TextContent1Small: {
      backgroundColor:'white',
      fontSize:9,
      marginLeft:1,
      marginTop:20,
      paddingTop:15,
      paddingBottom:15,
      fontWeight:'bold',
      paddingLeft:18,
      color:'black',
      fontFamily: 'Montserrat',
    },
    TextContent1:{
      backgroundColor:'white',
      fontSize:12,
      marginLeft:1,
      marginTop:20,
      paddingTop:15,
      paddingBottom:15,
      fontWeight:'bold',
      paddingLeft:18,
      color:'#005499',
      fontFamily: 'Montserrat',
    },
    NumberContent1:{
      backgroundColor:'white',
      fontSize:12,
      marginRight:1,
      marginTop:20,
      paddingTop:15,
      paddingBottom:15,
      fontWeight:'bold',
      justifyContent: 'flex-end',
      color:'#005499',
      fontFamily: 'Montserrat',
    },
    TextContent2:{
      backgroundColor:'white',
      fontSize:12,
      paddingTop:15,
      paddingBottom:15,
      paddingLeft:30,
      marginLeft:1,
      marginTop:20,
      fontWeight:'bold',
      color:'#FF892A',
      alignItems:'center',
      justifyContent:'center',
      fontFamily: 'Montserrat',
    },
    NumberContent2:{
      backgroundColor:'white',
      fontSize:12,
      marginRight:1,
      marginTop:20,
      paddingTop:15,
      paddingBottom:15,
      fontWeight:'bold',
      justifyContent: 'flex-end',
      color:'#FF892A',
      fontFamily: 'Montserrat',
    },
    goalmemo:{
      marginTop:40,
      fontSize:8,
      justifyContent:'flex-start',
      paddingRight:80,
      alignItems:'flex-start',
      fontFamily: 'Montserrat',
    },
    name2:{
      fontWeight:'bold',
      fontSize:26,
      color:'#005499',
      fontFamily: 'Montserrat',
    },
    goalName2:{
      color:'#00D5F7',
      fontSize:50,
      fontWeight:'bold',
      marginTop:-4,
      fontFamily: 'Montserrat',
    },
});
