#!/bin/bash

kill $(lsof -n -i4TCP:8081 | grep -v 'PID' | awk '{print $2}')
rm -f android/app/src/main/assets/index.android.bundle
rm -f android/app/build/intermediates/assets/release/index.android.bundle
rm -f android/app/build/intermediates/assets/debug/index.android.bundle
rm -f android/app/src/main/assets/index.android.bundle
#killall node
react-native start > /dev/null 2>&1 & 
sleep 6
curl "http://localhost:8081/index.android.bundle?platform=android" -o "android/app/src/main/assets/index.android.bundle"
(cd android/ && ./gradlew assembleDebug)
kill $(lsof -n -i4TCP:8081 | grep -v 'PID' | awk '{print $2}')
