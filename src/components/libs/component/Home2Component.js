import React, { Component } from 'react';
import {
    TouchableOpacity,
    View,
    Text,
    Dimensions
} from 'react-native';
import Image from 'react-native-remote-svg';
import { StyleSheet,ScrollView} from 'react-native';
import AnimateNumber from 'react-native-animate-number';

var width = Dimensions.get('window').width;
var height = Dimensions.get('window').height;

export default class Home2Component extends Component {
  static propTypes = {

  }

  static defaultProps = {

  }
  constructor(props) {
      super(props);
  }

/* eslint-disable */
  render() {

    const { day, month, pageNumber, text, optional, values } = this.props;

    if(!values) {
      return (<View />);
    }

    var valuesContent = values.map(function(item, idx) {

      if(item.negative) {
        value = (item.value.substring(0, 1) == '-' || item.value == '0,00') ? item.value : ('-'+item.value);
        
        return (<View style={styles.row}>
            <Text style={styles.TextContent1} >{item.label}</Text>
            <Text style={styles.TextContent2} >{value}</Text>
        </View>);
      }

      return (<View style={styles.row}>
          <Text style={styles.TextContent3} >{item.label}</Text>
          <Text style={styles.TextContent4} >{item.value}</Text>
      </View>);

    });

    return (
      <View style={styles.Container}>
          <View style={styles.Item}>

                  {/* <View style={styles.TitleItem}>
                    <Text style={styles.title}>Consecana Mensal acum.</Text>
                    <Text style={styles.title}>(R$/kg ATR) </Text>
                    <Text style={styles.goalTitle}>{this.props.mainNumber}</Text>
                  </View> */}

                  <View style={styles.NameItem}>
                    <Text style={styles.name}>{this.props.title}</Text>
                    <Text style={styles.goalName}>{this.props.number}</Text>
                    {/* {this.props.note ?
                      <Text style={styles.note}>{this.props.note}</Text>
                      :
                      null
                    } */}
                  </View>
                  {valuesContent}
                  {/*
                  <View style={styles.row}>
                      <Text style={styles.TextInput1} >Valor Bruto</Text>
                      <Text style={styles.TextInput2} >1.300.000</Text>
                  </View>
                  <View style={styles.row}>
                      <Text style={styles.TextContent1} >CTT</Text>
                      <Text style={styles.TextContent2} >235.000</Text>
                  </View>
                  <View style={styles.row}>
                      <Text style={styles.TextContent1} >Associação</Text>
                      <Text style={styles.TextContent2} >25.000</Text>
                  </View>
                  <View style={styles.row}>
                      <Text style={styles.TextContent1} >Outros Serviços</Text>
                      <Text style={styles.TextContent2} >0</Text>
                  </View>
                  <View style={styles.row}>
                      <Text style={styles.TextContent3} >Retenção</Text>
                      <Text style={styles.TextContent4} >208.000</Text>
                  </View>
                  <View style={styles.row}>
                      <Text style={styles.TextInput1} >Valor Líquido</Text>
                      <Text style={styles.TextInput2} >832.000</Text>
                  </View>
                  */}
          </View>
    </View>
      );
    }
  }

  const styles = StyleSheet.create({
    TitleItem:{
      backgroundColor:'#00D5F7',
      width:'93%',
      marginLeft:15,
      marginLeft:15,
      justifyContent:'center',
      alignItems:'center',
      marginTop:20,
      borderRadius:4,
      paddingTop:26,
      paddingBottom:36,
      paddingLeft:20,
      paddingRight:20,
      fontFamily: 'Montserrat',
    },
    title:{
      color:'white',
      fontSize:13,
      fontWeight:'bold',
      fontFamily: 'Montserrat',

    },
    goalTitle:{
      color:'#0068A9',
      fontSize:48,
      fontWeight:'bold',
      fontFamily: 'Montserrat',
    },

    NameItem:{
      backgroundColor:'white',
      width:'93%',
      paddingTop:29,
      paddingBottom:24,
      paddingLeft:20,
      paddingRight:20,
      marginLeft:15,
      marginRight:15,
      fontWeight:'bold',
      justifyContent:'center',
      alignItems:'center',
      marginTop:14,
      borderRadius:4,
      borderColor:'#F2F6F7',
      borderWidth:2,
      fontFamily: 'Montserrat',
    },
    name:{
      fontWeight:'bold',
      fontSize:13,
      color:'black',
      fontFamily: 'Montserrat',
    },
    note: {
      fontSize:10,
      alignItems:'flex-start',
      justifyContent:'flex-start',
      textAlign:'left',
      color:'black',
      fontFamily: 'Montserrat',
      width:'93%',
    },
    goalName:{
      color:'#00D5F7',
      // fontSize:45,
      fontSize:width/9,
      fontWeight:'bold',
      marginTop:-2,
      fontFamily: 'Montserrat',
    },
    row:{
      flexDirection:'row',
      alignItems:'center',
      justifyContent:'center',
      marginTop:-20,
      fontFamily: 'Montserrat',
    },
    TextInput1:{
      backgroundColor:'#0068A9',
      fontSize:12,
      padding:22,
      width:'49.5%',
      marginLeft:14,
      marginRight:2,
      marginTop:20,
      fontWeight:'bold',
      color:'white',
      paddingLeft:33,
      fontFamily: 'Montserrat',
    },
    TextInput2:{
      backgroundColor:'#0068A9',
      fontSize:12,
      padding:22,
      width:'42%',
      marginRight:14,
      marginTop:20,
      fontWeight:'bold',
      color:'white',
      paddingLeft:40,
      fontFamily: 'Montserrat',
    }
    ,
    TextContent1:{
      backgroundColor:'white',
      fontSize:12,
      padding:14,
      width:'49.5%',
      marginLeft:14,
      marginRight:2,
      marginTop:20,
      fontWeight:'bold',
      color:'#FF2525',
      paddingLeft:33,
      fontFamily: 'Montserrat',
    },
    TextContent2:{
      backgroundColor:'white',
      fontSize:12,
      padding:14,
      width:'42%',
      marginRight:14,
      marginTop:20,
      fontWeight:'bold',
      color:'#FF2525',
      paddingLeft:40,
      fontFamily: 'Montserrat',
    }
    ,
    TextContent3:{
      backgroundColor:'white',
      fontSize:12,
      padding:14,
      width:'49.5%',
      marginLeft:14,
      marginRight:2,
      marginTop:20,
      fontWeight:'bold',
      color:'#005499',
      paddingLeft:33,
      fontFamily: 'Montserrat',
    },
    TextContent4:{
      backgroundColor:'white',
      fontSize:12,
      padding:14,
      width:'42%',
      marginRight:14,
      marginTop:20,
      fontWeight:'bold',
      color:'#005499',
      paddingLeft:40,
      fontFamily: 'Montserrat',
    },
    Item:{
        marginBottom:20,
        fontFamily: 'Montserrat',
    }
  });
