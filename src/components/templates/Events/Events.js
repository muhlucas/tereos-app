import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { StyleSheet, View, Text, Image, ScrollView, TouchableOpacity, TouchableHighlight, Dimensions, Modal, Platform, Linking, Alert } from 'react-native';
import Spinner from 'react-native-loading-spinner-overlay';
import HTMLView from 'react-native-htmlview';
import NoDataButton from '../../components/NoDataButton';

import EventItem from '../../libs/component/EventItem';

import Api from '../../../lib/Api';

import User from '../../../lib/User';
import Helper from '../../../lib/Helper';

var width = Dimensions.get('window').width;
var height = Dimensions.get('window').height;

function renderNode(node, index, siblings, parent, defaultRenderer) {
  if (node.name == 'a') {
    const a = node.attribs;
    return (
      <Text key={index} style={{ alignItems: 'center', backgroundColor: '#2d3a43', padding: 30, color: 'white', fontFamily: 'Arial', fontWeight: 'bold', fontSize: 16 }} onPress={() => Linking.openURL(a.href)}>
        Clique Aqui
        </Text>
    );
  }
}

export default class Event extends Component {

  static propTypes = {
    navigation: PropTypes.shape({
      navigate: PropTypes.func
    })
  }

  static defaultProps = {
    navigation: {
      navigate: () => { }
    }
  }

  constructor() {
    super();

    this.state = {
      modalVisible: false,
      modalContent: '',
      events: [],
      isLoading: false,
      emptyData: false,
      netinfo: null
    }

    this.openDetail = this.openDetail.bind(this);
    this.onConfirm = this.onConfirm.bind(this);
    this.onInterest = this.onInterest.bind(this);
  }

  componentDidMount() {
    this.loadEvents();
  }



  async loadEvents() {
    let _this = this;
    let id = await User.getId();

    _this.setState({ isLoading: true }, function () {
      Api.getEvents(id)
        .then(function (data) {

          if (typeof data == 'undefined') {
            setTimeout(() => {
              alert("Usuário sem permissão");
            }, 100);
            throw 'Usuário sem permissão';
          }

          if (data) {
            _this.setState({ events: data, isLoading: false, emptyData: false });
          } else {
            _this.setState({ events: [], isLoading: false, emptyData: true });
          }

          console.log('events', data);
        })
        .catch(function (err) {
          _this.setState({ isLoading: false, emptyData: true });
        });
    });
  }

  openDetail() {
    this.setState({ modalVisible: true });
  }

  async onConfirm(eventId) {

    let userId = await User.getId();
    // Api.removeEventPresence(eventId, userId).
    // then(function(data) {
    Api.confirmEventPresence(eventId, userId)
      .then(function (data) {
        Alert.alert('Presença confirmada');
        console.log('data event', data);
      })
    // });
    // alert('onConfirm');
  }

  async onInterest(eventId) {
    console.log('this.state.interested', this.state.interested);
    let userId = await User.getId();
    Api.removeEventPresence(eventId, userId).
      then(function (data) {
        console.log('url removeEventPresence');
        Api.confirmEventPresence(eventId, userId)
          .then(function (data1) {
            console.log('url confirmEventPresence');
            Api.confirmEventInterest(eventId, userId)
              .then(function (data2) {
                console.log('url confirmEventInterest');
                Alert.alert('Interesse confirmado');
                console.log('data event', data2);
              });
          })
      })
    // alert('onInterest');
  }

  async onUncheck(eventId) {

    let userId = await User.getId();
    Api.removeEventPresence(eventId, userId).
      then(function (data) {
        console.log('url removeEventPresence');
      })
  }

  render() {
    var _this = this;

    let emptyData = this.state.emptyData;

    let eventsContent = this.state.events.map(function (item) {
      console.log('item', item);
      name = Helper.removeHtml(item.Nome);
      local = Helper.removeHtml(item.Local);

      var day = item.DataEvento.substr(8, 2);
      var month = parseInt(item.DataEvento.substr(5, 2));

      var monthStr = Helper.getMonthShort(month).toUpperCase();
      var eventId = item.Id;

      // var extraContent = Helper.removeHtml(item.Descricao);
      var extraContent = Helper.clearNewLine(Helper.removeImgFromPTags(item.Descricao));

      let confirmed = (item.Status == 2);
      let interested = (item.Status == 1 && !confirmed);

      // extraContent = extraContent.replace(/(<p[^>]+?>|<p>|<\/p>)/img, "");
      // extraContent = extraContent.replace(/<\/p>/g, '');

      return (
        <EventItem
          day={day}
          month={monthStr}
          itemTitle={name}
          firstText={''}
          text={"Local: " + local}
          optional="SAIBA MAIS +"
          interested={interested}
          onInterest={() => { _this.onInterest(eventId); }}
          confirmed={confirmed}
          onConfirm={() => { _this.onConfirm(eventId); }}
          onUncheck={() => { _this.onUncheck(eventId); }}
          onDetail={() => { _this.setState({ modalVisible: true, modalContent: extraContent }); }}
          value={0}
        />
      )
    });

    return (
      <View style={styles.container}>
        <ScrollView>

          {this.state.isLoading ?
            <View style={{ flex: 1 }}>
              <Spinner visible={true} textStyle={{ color: '#FFF' }} />
            </View>
            : null}
          <Modal
            animationType="slide"
            transparent={false}
            visible={this.state.modalVisible}
            onRequestClose={() => {
              this.setState({ modalVisible: false });
            }}
          >
            <View style={styles.modalContainer}>
              <View style={styles.modalTopBar}>
                <TouchableOpacity onPress={() => {
                  this.setState({ modalVisible: false });
                }}>
                  <Image style={styles.img1}
                    source={require('../../../assets/img/close.png')}
                  />
                </TouchableOpacity>
              </View>

              <ScrollView style={styles.modalContent}>
                <View style={styles.bullet}>
                  <Text style={styles.modalTitle}>PROGRAMAÇÃO</Text>

                  {/*<WebView source={{ html: "<h1>Hello</h1>" }} />*/}

                  <HTMLView
                    value={'<div style="font-family: Calibri,sans-serif; font-size:12px; color: black; text-align: justify">' + _this.state.modalContent + '</div>'}
                    //addLineBreaks={false}
                    //paragraphBreak="<br/>"
                    //stylesheet={htmlStyles}
                    renderNode={renderNode}
                  />
                </View>
              </ScrollView>
            </View>
          </Modal>
          <View>
            <Text style={styles.title}>Eventos</Text>
          </View>
          <View style={styles.Item}>
            {emptyData ?
              <NoDataButton onPress={_this.loadNews} />
              : null}
            {eventsContent}
          </View>
        </ScrollView>
      </View>
    );
  }
}


const htmlStyles = StyleSheet.create({
  /*div: {
    fontFamily: 'Calibri,sans-serif',
    fontSize: 12,
    color: 'black',
    textAlign: 'justify'
  },*/
  /*a: {
    backgroundColor: '#2d3a43',
    color: 'white',
    fontSize: 20,
    paddingLeft: 40,
    paddingRight: 40,
    textAlign: 'justify'
  }*/
});


const styles = StyleSheet.create({
  container: {
    backgroundColor: '#F7F7F7',
    color: '#cc00ff',
    fontFamily: 'Montserrat',
  },
  title: {
    fontSize: 23,
    marginTop: 10,
    marginBottom: 15,
    marginLeft: 15,
    fontWeight: 'bold',
    color: '#005499',
    fontFamily: 'Montserrat',
  },
  marginTop: {
    marginTop: 30
  },
  modalContainer: {
    backgroundColor: '#EAEAEA',
    marginTop: 22,
    ...Platform.select({
      android: {
        height: height - 52
      },
      ios: {
        height: height - 22
      }
    }),
  },
  modalTopBar: {
    backgroundColor: 'white',
    height: 50,
    padding: 20,
    width: width
  },
  modalContent: {
    paddingRight: 10,
    paddingLeft: 10
  },
  modalTitle: {
    fontFamily: 'Montserrat',
    fontSize: 14,
    color: '#005499',
    fontWeight: 'bold',
    marginBottom: 10
  },
  bullet: {
    fontFamily: 'Montserrat',
    fontSize: 14,
    color: 'black',
    paddingTop: 9,
    paddingBottom: 10
  },
  bold: {
    fontWeight: 'bold'
  },
  img1: {
    width: 84,
    height: 21,
    marginBottom: 10
  }
});
