import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { StyleSheet, View, TextInput, Text, ScrollView, TouchableOpacity, Platform, TouchableWithoutFeedback } from 'react-native';
import Image from 'react-native-remote-svg';
import Spinner from 'react-native-loading-spinner-overlay';
import Switch from 'react-native-customisable-switch';
import { TextInputMask } from 'react-native-masked-text';
import SimplePicker from 'react-native-simple-picker';
import DialogAndroid from 'react-native-dialogs';
import OneSignal from 'react-native-onesignal';

import Helper from '../../../lib/Helper';

export default class EditProfile extends Component {

  static propTypes = {
      navigation: PropTypes.shape({
          navigate: PropTypes.func
      })
  }

  static defaultProps = {
      navigation: {
          navigate: () => {}
      }
  }

  constructor(){
      super();

      this.state = {
        profileData: null,
        playerId: null,
        name: null,
        email: null,
        mobilePhone: null,
        cpf: null,
        address: null,
        addressNumber: null,
        addressComplement: null,
        zipcode: null,
        neighborhood: null,
        city: null,
        cityName: null,
        state: null,
        switchValue: null,
        pushNotifications: null,
        states: [],
        cities: [],
        citiesLabels: [],
        isLoading: false,
      }

      this.getUserDetails = this.getUserDetails.bind(this);
      this.updatePlayerId = this.updatePlayerId.bind(this);
      this.getNotifications = this.getNotifications.bind(this);
      this.onIds = this.onIds.bind(this);
      this.updateUserDetails = this.updateUserDetails.bind(this);
  }

  componentDidMount() {
    this.getUserDetails();
    this.getNotifications();
    this.setState({states: Helper.getStates()});
  }

  async loadCities() {
    let _this = this;

    let state = _this.state.state;

    if(state) {
      Api.getCities(state)
      .then(function(data) {
        console.log('cities 1', data);

        cities = [];
        citiesLabels = [];
        for(idx in data) {
          city = data[idx];
          cities.push(city.Codigo.toString());
          citiesLabels.push(city.Nome);
        }

        console.log('cities 2', cities);
        console.log('citiesLabels 2', citiesLabels);

        _this.setState({cities: cities, citiesLabels: citiesLabels});
      })
      .catch(function(err) {
        _this.setState({isLoading: false});
      });
    } else {
      _this.setState({state: 'UF'});
      // alert('Por favor, escolha um estado primeiro')
    }

  }

  async getUserDetails() {
    let _this = this;

    let id = await User.getId();
    let playerId = await User.getPlayerId();

    let notificationPreference = await User.getNotificationPreference();

    this.setState({isLoading: true, playerId: playerId}, function() {
      Api.getProfile(id, false)
      .then(function(data) {

        console.log('data profile', data);
        address = (data.Endereco) ? data.Endereco.trim() : '';
        addressNumber = (data.Numero) ? data.Numero.trim() : '';
        addressComplement = (data.Complemento) ? data.Complemento.trim() : '';
        neighborhood = (data.BAIRRO) ? data.BAIRRO.trim() : '';
        zipcode = (data.CEP) ? data.CEP.trim() : '';
        cityName = (data.CIDADE) ? data.CIDADE.trim() : '';
        state = (data.UF) ? data.UF.trim() : '';

        cpf = Helper.formatCPF(data.CPF);
        phone = (data.Telefone) ? Helper.formatPhoneNumber(data.Telefone) : '';
        mobilePhone = (data.Celular1) ? Helper.formatPhoneNumber(data.Celular1) : '';

        _this.setState({
          userId: id,
          profileData: data,
          name: data.Nome,
          email: data.Email,
          address: address,
          phone: phone,
          mobilePhone: mobilePhone,
          cpf: cpf,
          zipcode: zipcode,
          address: address,
          addressNumber: addressNumber,
          addressComplement: addressComplement,
          neighborhood: neighborhood,
          zipcode: zipcode,
          city: null,
          cityName: cityName,
          cityCode: data.CodigoCidade,
          state: state,
          isLoading: false,
          switchValue: (notificationPreference == 'ok')
        }, function() {
          _this.loadCities();
        });
      })
      .catch(function(err) {
        _this.setState({isLoading: false});
      });
    })
  }

  async updateUserDetails() {
    let _this = this;

    let id = await User.getId();

    let data = this.state.profileData;

    if(data != null) {
      values = {
        "Id": data.Id,
        "Nome": data.Nome,
        "CPF": data.CPF,
        "Email": data.Email,
        "Endereco": this.state.address,
        "Numero": this.state.addressNumber,
        "BAIRRO": this.state.neighborhood,
        "CIDADE": data.CIDADE,
        "CodigoCidade": this.state.cityCode,
        "CEP": (this.state.zipcode) ? this.state.zipcode.replace('-', '') : '',
        "UF": this.state.state,
        "Complemento": this.state.addressComplement,
        "Telefone": this.state.phone,
        "Celular1": this.state.mobilePhone,
        "Foto": data.Foto,
        "Gestor": null
      };

      console.log('values', values);

      this.setState({isLoading: true}, function() {
        Api.updateProfile(id, values)
        .then(function(data) {
          console.log('data', data);
          _this.setState({isLoading: false});
        });
      })
    }
  }

  async updateNotificationPreference() {
    let _this = this;

    let switchValue = this.state.switchValue;

    if(switchValue) {
      OneSignal.init("66a4e40c-0fe9-4180-ae76-59396f7919b9");
      OneSignal.configure();
      // OneSignal.promptForPushNotificationPermissions();
      OneSignal.addEventListener("ids", this.onIds);

    } else {
      let playerId = await User.getPlayerId();
      Api.removePlayerId(id, playerId)
      .then(function(data) {
        User.removeNotificationPreference();
      });
    }
  }

  async updateNotification(id, codigo, value) {
    let _this = this;
    Api.updateNotification(id, codigo, value)
    .then(function(data) {

    });
  }

  onIds(device) {
    let _this = this;
    let playerId = device.userId;
    if(playerId.length) {
      _this.updatePlayerId(playerId);
    }
  }

  async updatePlayerId(playerId) {
    let userId = await User.getId();

    User.setPlayerId(playerId);

    if(userId != null && playerId != null) {
      console.log('onesignal userId', userId);
      console.log('onesignal playerId', playerId);
      Api.updatePlayerId(userId, playerId)
      .then(function(data) {
        User.enableNotificationPreference();
      });
    }
  }

  async showPicker() {
    let _this = this;

    if(Platform.OS == 'ios') {
      _this.refs.picker.show();
    } else {
      let options = [];
      let states = this.state.states;
      for(idx in states) {
        state = states[idx];
        options.push({label: state, id: state});
      }

      const { selectedItem } = await DialogAndroid.showPicker('Escolha um Estado', null, {
          items: options
      });
      if (selectedItem) {
          _this.setState({
            state: selectedItem.id
          }, function() {
            _this.loadCities();
          });
      }
    }
  }

  async showPicker2() {
    let _this = this;

    console.log('cities to be shown', _this.state.cities);

    if(Platform.OS == 'ios') {
      _this.refs.picker2.show();
    } else {
      let options = [];
      let cities = this.state.cities;
      let citiesLabels = this.state.citiesLabels;
      for(idx in cities) {
        city = cities[idx];
        cityLabel = citiesLabels[idx];
        options.push({label: cityLabel, id: city});
      }

      const { selectedItem } = await DialogAndroid.showPicker('Escolha uma Cidade', null, {
          items: options
      });
      if (selectedItem) {
        console.log('cities option', option);
        let option = selectedItem.id;

        cities = _this.state.cities;
        citiesLabels = _this.state.citiesLabels;
        idx = cities.indexOf(option);
        city = citiesLabels[idx];

        this.setState({
          cityName: city,
          cityCode: option
        }, function() {
          // _this.loadReports();
        });
      }
    }
  }

  async getNotifications(){
    let _this = this;
    let userId = await User.getId();
    Api.getNotifications(userId).then(response => {
      console.log("notifications", response);
      this.setState({pushNotifications: response});
    });
  }

  viewPushes(){
    let _this = this;
    let userId = this.state.userId;
    let pushes = {}; 
    pushes.pushNotifications = this.state.pushNotifications === null ? [] : this.state.pushNotifications;

    let pushViews = [];

    for(let i = 0; i < pushes.pushNotifications.length; i ++){
      console.log("notificacao", pushes.pushNotifications);
      let ativo = pushes.pushNotifications[i].Ativo;
      let nome = pushes.pushNotifications[i].Nome;
      let codigo = pushes.pushNotifications[i].Codigo;       
      pushViews.push(
      
        <View style={[styles.row, {marginTop: 5, marginBottom:5,  marginLeft:15}]}>

          <Switch
            value={ativo}
            onChangeValue={() => {
              pushes.pushNotifications[i].Ativo = !ativo;
              this.setState(pushes, function() { _this.updateNotification(userId, codigo, !ativo); });
            }}
            activeBackgroundColor={'#3ca9dd'}
            padding={false}
            switchWidth={50}
          />

          <View style={styles.labelReceiveNotification}>
            <Text style={styles.textReceiveNotification}>{nome}</Text>
          </View>

        </View>);
    }

    return pushViews;
  }

 render() {
    let _this = this;
    let switchValue = (this.state.switchValue === null) ? (this.state.playerId != null) : this.state.switchValue;

    let pushViews = this.viewPushes();

    return (
      <View style={styles.container}>
        <ScrollView>
        {this.state.isLoading ?
          <Spinner visible={true} textStyle={{color: '#FFF'}} />
        : null }
          <View style={styles.content}>
            <Text style={styles.title}>Dados de Contato</Text>
            <View style={{width:'93%', marginTop:20, marginBottom:7, backgroundColor:'#E9E9E9',height:1,marginLeft:14,marginRight:14}}/>
            <TextInput
              style={styles.TextInput}
              value={this.state.phone}
              placeholder="Telefone Fixo"
              underlineColorAndroid='transparent'
              onChangeText={(text) => {this.setState({phone: text})}}
            />
            <TextInput
              style={styles.TextInput}
              value={this.state.mobilePhone}
              placeholder="Telefone Celular"
              underlineColorAndroid='transparent'
              onChangeText={(text) => {this.setState({mobilePhone: text})}}
              />
            <TextInput
              style={styles.TextInput}
              value={this.state.email}
              placeholder="Email"
              underlineColorAndroid='transparent'
              onChangeText={(text) => {this.setState({email: text})}}
              />
          </View>

          <View style={styles.content}>
            <Text style={styles.title2}>Endereço</Text>
            <View style={{width:'93%', marginTop:11, marginBottom:6, backgroundColor:'#E9E9E9',height:1,marginLeft:14,marginRight:14}}/>

            <TextInputMask
              style={styles.TextInput}
              refInput={(ref) => {this.cep = ref;}}
              type={'zip-code'}
              maxLength={9}
              placeholder="CEP"
              underlineColorAndroid='transparent'
              onChangeText={text => this.setState({ zipcode: text })}
              value={this.state.zipcode}
            />
            <View style={styles.row}>
              <TextInput
                style={styles.TextInput1}
                value={this.state.address}
                placeholder="Rua"
                underlineColorAndroid='transparent'
                onChangeText={(text) => {this.setState({address: text})}}
                />
              <TextInput
                style={styles.TextInput2}
                value={this.state.addressNumber}
                placeholder="N°"
                underlineColorAndroid='transparent'
                onChangeText={(text) => {this.setState({addressNumber: text})}}
                />
            </View>
            <TextInput
              value={this.state.neighborhood}
              style={styles.TextInput}
              placeholder="Bairro"
              underlineColorAndroid='transparent'
              onChangeText={(text) => {this.setState({neighborhood: text})}}
            />
            <TextInput style={styles.TextInput}
              value={this.state.addressComplement}
              placeholder="Complemento"
              underlineColorAndroid='transparent'
              onChangeText={(text) => {this.setState({addressComplement: text})}}
            />
            <SimplePicker
              ref={'picker'}
              options={this.state.states}
              confirmText={'Confirmar'}
              cancelText={'Cancelar'}
              itemStyle={{
                fontSize: 25,
                marginLeft: 10,
                textAlign: 'left',
                fontWeight: 'bold',
              }}
              onSubmit={(option) => {
                let _this = this;
                this.setState({
                  state: option,
                }, function() {
                  _this.loadCities();
                  // _this.loadReports();
                });
              }}
            />

            <View style={[styles.row, {marginBottom:20}]}>
              <TouchableWithoutFeedback
                onPress={() => {
                  this.showPicker();
                }}>
                  <Text
                    value={this.state.state}
                    style={styles.TextInput3}
                    placeholder="UF"
                    underlineColorAndroid='transparent'>{this.state.state}
                  </Text>
              </TouchableWithoutFeedback>

              <SimplePicker
                ref={'picker2'}
                options={this.state.cities}
                labels={this.state.citiesLabels}
                confirmText={'Confirmar'}
                cancelText={'Cancelar'}
                itemStyle={{
                  fontSize: 25,
                  marginLeft: 10,
                  textAlign: 'left',
                  fontWeight: 'bold',
                }}
                onSubmit={(option) => {
                  console.log('cities option', option);

                  let _this = this;
                  cities = _this.state.cities;
                  citiesLabels = _this.state.citiesLabels;
                  idx = cities.indexOf(option);
                  city = citiesLabels[idx];

                  this.setState({
                    cityName: city,
                    cityCode: option
                  }, function() {
                    // _this.loadReports();
                  });
                }} />
                <TouchableWithoutFeedback
                  onPress={() => {
                    this.showPicker2();
                  }}>
                      <Text
                        style={styles.TextInput4}
                        underlineColorAndroid='transparent'>
                        {this.state.cityName ? this.state.cityName : 'Cidade'}
                      </Text>
                </TouchableWithoutFeedback>
            </View>

            {pushViews}

            <TouchableOpacity style={{alignItems:'center',justifyContent:'center'}}
              onPress={() => {this.updateUserDetails();}}
            >
             <Text style={styles.button}>Salvar</Text>
            </TouchableOpacity>
            <View style={{marginTop:20}}/>

          </View>
        </ScrollView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
    container: {
      backgroundColor:'white'
    },
    TextInput:{
      backgroundColor:'white',
      fontSize:15,
      padding:10,
      width:'93%',
      marginLeft:14,
      marginRight:14,
      marginTop:10,
      borderColor:'#A9B8D6',
      borderWidth:1,
      fontFamily: 'Montserrat',
    },
    title:{
      fontSize:21,
      marginTop:12,
      marginBottom:0,
      marginLeft:15,
      fontWeight:'bold',
      color:'#005499',
      fontFamily: 'Montserrat',
    },
    title2:{
      fontSize:21,
      marginTop:32,
      marginBottom:0,
      marginLeft:15,
      fontWeight:'bold',
      color:'#005499',
      fontFamily: 'Montserrat',
    },
    close:{
      height:53,
      padding:17,
      backgroundColor:'white'
    },
    img:{
      width:84,
      height:21
    },
    content:{
      backgroundColor:'#F7F7F7',
      fontFamily: 'Montserrat',
    },
    button:{
      backgroundColor:'#00ABE0',
      fontSize:15,
      paddingTop:15,
      paddingBottom:15,
      width:'93%',
      marginTop:10,
      textAlign:'center',
      borderColor:'#A9B8D6',
      borderWidth:1,
      alignItems:'center',
      justifyContent:'center',
      color:'white',
      fontFamily: 'Montserrat',
    },
    row:{
      flexDirection:'row'
    },
    TextInput1:{
      backgroundColor:'white',
      fontSize:15,
      padding:10,
      width:'63%',
      marginLeft:14,
      marginRight:2,
      marginTop:10,
      borderColor:'#A9B8D6',
      borderWidth:1,
      fontFamily: 'Montserrat',
    },
    TextInput2:{
      backgroundColor:'white',
      fontSize:15,
      padding:10,
      width:'29%',
      marginLeft:2,
      marginRight:14,
      marginTop:10,
      borderColor:'#A9B8D6',
      borderWidth:1,
      fontFamily: 'Montserrat',
    },
    TextInput3:{
      backgroundColor:'white',
      fontSize:15,
      padding:10,
      width:'29%',
      marginLeft:14,
      marginRight:2,
      marginTop:10,
      borderColor:'#A9B8D6',
      borderWidth:1,
      fontFamily: 'Montserrat',
    },
    TextInput4:{
      backgroundColor:'white',
      fontSize:15,
      padding:10,
      width:'63%',
      marginLeft:2,
      marginRight:14,
      marginTop:10,
      borderColor:'#A9B8D6',
      borderWidth:1,
      fontFamily: 'Montserrat',
    },
    labelReceiveNotification: {
      fontFamily: 'Montserrat',
      marginLeft:10,
      marginTop:6
    },
    textReceiveNotification: {
      fontFamily: 'Montserrat',
    }
});
