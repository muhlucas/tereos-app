import React, {Component} from 'react';
import {
    Text,
    View,
    Dimensions,
    ImageBackground,
    TextInput,
    TouchableOpacity,
    Alert,
    AlertIOS,
    Platform,
    KeyboardAvoidingView,
    AsyncStorage,
    ScrollView
} from 'react-native';
import Image from 'react-native-remote-svg';
import Switch from 'react-native-customisable-switch';
import {TextInputMask} from 'react-native-masked-text';

import DeviceInfo from 'react-native-device-info';

import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';
import Spinner from 'react-native-loading-spinner-overlay';

import User from '../../../lib/User';

import styles from './styles';
import OneSignal from "react-native-onesignal";

var width = Dimensions.get('window').width;
var height = Dimensions.get('window').height;

export default class Login extends Component {

    constructor(props) {
        super(props);
        
        
        const appVersion = DeviceInfo.getVersion();
        const buildNumber = DeviceInfo.getBuildNumber();

        this.state = {
            switchValue: false,
            cpf: null,
            password: null,
            isLoading: false,
            isCheckingAuthentication: true,
            appVersion: appVersion,
            buildNumber: buildNumber,
            playerId: null,
        }

        this.openLogin = this.openLogin.bind(this);
        this.openForgotPassword = this.openForgotPassword.bind(this);
        this.checkUserLogged = this.checkUserLogged.bind(this);
        this.updatePlayerId = this.updatePlayerId.bind(this);
        this.onIds = this.onIds.bind(this);
    }

    componentDidMount() {
        // check if user is logged before show the screen
        this.checkUserLogged();
    }

    async checkUserLogged() {
        let _this = this;

        let obj = await User.getUser();

        if (obj != null && typeof obj.remainLogged != 'undefined' && obj.remainLogged == true) {
            _this.props.navigation.navigate('App');
        }

        let playerId = await User.getPlayerId();

        _this.setState({isCheckingAuthentication: false, playerId: playerId});
    }

    async openLogin() {
        let _this = this;

        var {cpf, password} = this.state;
        // console.log('openLogin', cpf);

        cpf = (cpf) ? Helper.cleanCpfPunctuation(cpf) : '';

        if (cpf.length == 0 || password.length == 0) {
            alert('Por favor, preencha o login e senha');
            return;
        }

        var objUser = {cpf: cpf, password: password};

        // objUser = {cpf: '14721652844', password: '123456'};
         //objUser = {cpf: '16051122834', password: 'ib191919'};

        this.setState({isLoading: true}, function () {

            AsyncStorage.setItem('@login', JSON.stringify(objUser), function () {

                Api.login().then(function (token) {

                    Api.getProfile(objUser.cpf, true).then(function (data) {

                        console.log('data login', data);

                        if (typeof data == 'undefined') {
                            throw 'Usuário sem permissão';
                        }

                        // getting manager id/cpf
                        objUser.managerId = (data.Gestor) ? data.Gestor.CPF : null;

                        // remember me
                        objUser.remainLogged = _this.state.switchValue;

                        AsyncStorage.setItem('@login', JSON.stringify(objUser), function () {

                            _this.setState({isLoading: false}, function () {
                                OneSignal.init("66a4e40c-0fe9-4180-ae76-59396f7919b9");
                                OneSignal.configure();
                                // OneSignal.promptForPushNotificationPermissions();
                                OneSignal.addEventListener("ids", _this.onIds);
                                _this.props.navigation.navigate('App');
                            })
                        });
                    }).catch(function (e) {
                        _this.setState({isLoading: false}, function () {
                            setTimeout(() => {
                                alert('Erro usuário: ' + e);
                            }, 100);
                        });
                    })
                }).catch(function (e) {
                    _this.setState({isLoading: false}, function () {
                        setTimeout(() => {
                            alert('Erro login: ' + e);
                        }, 100);
                    });
                })
            });
        });
    }

    onIds(device) {
        let _this = this;
        let playerId = device.userId;
        if (playerId.length) {
            _this.updatePlayerId(playerId);
        }
    }

    async updatePlayerId(playerId) {
        let userId = await User.getId();

        User.setPlayerId(playerId);

        if (userId != null && playerId != null) {
            console.log('onesignal userId', userId);
            console.log('onesignal playerId', playerId);
            Api.updatePlayerId(userId, playerId)
                .then(function (data) {
                    User.enableNotificationPreference();
                });
        }
    }

    openForgotPassword() {
        this.props.navigation.navigate('ForgotPassword');
    }

    render() {
        const {switchValue, appVersion, buildNumber, playerId} = this.state;
        const {navigate, state} = this.props.navigation;

        if (this.state.isCheckingAuthentication) {
            return (<View/>);
        }

        return (
            <KeyboardAwareScrollView style={styles.container} behavior="padding" enabled>
                {this.state.isLoading ?
                    <View style={{flex: 1}}>
                        <Spinner visible={true} textStyle={{color: '#FFF'}}/>
                    </View>
                    : null}
                <View style={{alignItems: 'center', height:height}}>
                    <Image source={require('../../../assets/img/bg2.png')} style={styles.backgroundImage}/>
                    <Image style={styles.logo}
                           source={require('../../../assets/img/logo_full.png')}
                    />
                    <View style={styles.loginForm}>
                        <View>
                            <Text style={styles.headerAccountAccess}>Acesse sua conta</Text>
                        </View>

                        <TextInput
                            style={styles.TextInput}
                            placeholder="CPF ou Nome de usuário"
                            underlineColorAndroid='transparent'
                            onChangeText={cpf => this.setState({cpf})}
                            value={this.state.cpf}
                        />

                        <TextInput
                            style={styles.TextInput}
                            placeholder="Senha"
                            underlineColorAndroid='transparent'
                            secureTextEntry={true}
                            onChangeText={password => this.setState({password})}
                            value={this.state.password}
                        />


                        <TouchableOpacity
                            onPress={() => {
                                this.openForgotPassword();
                            }}
                            style={styles.boxForgotPassword}>
                            <Text style={styles.textForgotPassword}>Esqueci minha senha</Text>
                        </TouchableOpacity>

                        <View style={styles.boxKeepLogged}>
                            <View style={styles.labelKeepLogged}>
                                <Text style={styles.textKeepLogged}>Manter-se logado</Text>
                            </View>

                            <Switch
                                value={switchValue}
                                onChangeValue={() => this.setState({switchValue: !switchValue})}
                                activeBackgroundColor={'#3ca9dd'}
                                padding={false}
                                switchWidth={50}
                            />

                        </View>

                        <TouchableOpacity
                            style={styles.button}
                            onPress={() => {
                                this.openLogin()
                            }}
                        >
                            <Text style={styles.textButton}>Entrar</Text>
                        </TouchableOpacity>

                    </View>

                </View>

                <View style={styles.version}>
                    <Text
                        style={styles.versionText}>{(playerId && __DEV__ == true) ? 'Player ID: ' + playerId + ' - ' : null} Versão {appVersion}{/* - Build {buildNumber} */}</Text>
                </View>

            </KeyboardAwareScrollView>
        );
    }
}
