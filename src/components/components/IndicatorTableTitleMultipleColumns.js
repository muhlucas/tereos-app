import React, { Component } from 'react';
import {
    StyleSheet,
    TouchableOpacity,
    View,
    Image,
    Text
} from 'react-native';
import { Col, Row, Grid } from 'react-native-easy-grid';

export default class IndicatorTable extends Component {

  constructor(props) {
      super(props);

      this.state = {

      }
  }

  render() {
    const { title, labels, values, showButton, buttonAction } = this.props;

    if(labels.length == 0) {
      return (<View />);
    }

    var labelsContent = labels.map(function(item, idx) {
      return (<Col style={{borderBottomWidth:1, borderColor:'#EAEAEA'}}>
        <Text style={styles.LabelRow1}>{item}</Text>
      </Col>)
    })

    console.log('values', values);

    if(values == null || values.length == 0) {
      return (<View />);
    }

    var valuesContent = values.map(function(item, idx) {
      value = item.value;

      variation = null;
      if(typeof item.variation != 'undefined' && (item.variation == 'up' || item.variation == 'down')) {
        variation = item.variation;

        if(item.variation == 'down') {
          image = require('../../assets/img/arrow_down.png');
        } else {
          image = require('../../assets/img/arrow_up.png');
        }  
      }

      return (<Col>
          <View style={{flex:1, flexDirection:'row', alignSelf:'center'}}>
            <Text style={styles.ValueContent1}>{value}</Text>
            {variation ?
              <Image source={image} style={styles.icon}/>
              :
              null
            }
          </View>
      </Col>);
    })

    return (
      <View style={styles.cardBox}>
        <View style={styles.NameItem}>
          <Text style={styles.cardTitle}>{title.toUpperCase()}</Text>
          {showButton && buttonAction ?
            <TouchableOpacity style={styles.button} onPress={buttonAction}>
              <Text style={styles.buttonText}>{showButton}</Text>
            </TouchableOpacity>
          :
            null
          }
          {/* <Text style={styles.goalton}>{'(kg/tonelada)'}</Text> */}
        </View>
        <View style={styles.row}>
          <Grid>
            <Row>
              <Col>
                <Grid>
                  {labelsContent}
                </Grid>
              </Col>
            </Row>
            <Row>
              <Col>
                <Grid>
                  {valuesContent}
                </Grid>
              </Col>
            </Row>
          </Grid>
        </View>
      </View>
      );
    }
  }

const styles = StyleSheet.create({
  cardBox: {
    marginTop:0,
    marginBottom:20,
    marginLeft:15,
    marginRight:15,
    borderRadius:5,
    borderColor:'#EAEAEA',
    borderWidth:1,
    backgroundColor:'white',
  },
  NameItem:{
    paddingTop:22,
    paddingBottom:22,
    paddingLeft:20,
    paddingRight:20,
    flexDirection:'row',
    justifyContent:'space-between'
  },
  cardTitle:{
    fontWeight:'bold',
    fontSize:12,
    color:'#005499',
    fontFamily: 'Montserrat',

  },
  button: {
    backgroundColor:'#005596',
    borderRadius:20,
    padding:5,
    width:100,
    marginTop:-5
  },
  buttonText: {
    fontFamily: 'Montserrat',
    fontSize:10,
    fontWeight:'bold',
    color:'#FFF',
    textAlign:'center',
  },
  goalton:{
    color:'#005499',
    fontSize:12,
    fontWeight:'bold',
    fontFamily: 'Montserrat',
  },
  row:{
    flexDirection:'row',
    alignItems:'center',
    justifyContent:'space-between',
    marginTop:-20,
    flex:1
  },
  TextContent1:{
    fontSize:10,
    marginLeft:1,
    marginTop:20,
    paddingTop:15,
    paddingBottom:15,
    fontWeight:'bold',
    textAlign:'center',
    color:'#005499',
    fontFamily: 'Montserrat',
  },
  LabelRow1:{
    fontSize:10,
    marginTop:20,
    paddingTop:15,
    paddingBottom:15,
    fontWeight:'bold',
    textAlign:'center',
    color:'#005499',
    fontFamily: 'Montserrat',
  },
  ValueContent1:{
    fontSize:12,
    paddingTop:10,
    paddingBottom:15,
    fontWeight:'bold',
    textAlign:'center',
    color:'#000',
    fontFamily: 'Montserrat',
  },
  NumberContent1:{
    fontSize:12,
    marginRight:1,
    marginTop:20,
    paddingTop:15,
    paddingBottom:15,
    fontWeight:'bold',
    justifyContent: 'flex-end',
    color:'#005499',
    fontFamily: 'Montserrat',
  },
  TextContent2:{
    fontSize:12,
    paddingTop:15,
    paddingBottom:15,
    paddingLeft:30,
    marginLeft:1,
    marginTop:20,
    fontWeight:'bold',
    color:'#FF892A',
    alignItems:'center',
    justifyContent:'center',
    fontFamily: 'Montserrat',
  },
  NumberContent2:{
    fontSize:12,
    marginRight:1,
    marginTop:20,
    paddingTop:15,
    paddingBottom:15,
    fontWeight:'bold',
    justifyContent: 'flex-end',
    color:'#FF892A',
    fontFamily: 'Montserrat',
  },
  icon: {
    marginTop:8,
    marginLeft:2,
    width:20,
    height:20,
  }
});
