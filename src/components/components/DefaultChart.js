import React, { Component } from 'react';
import {
    StyleSheet,
    TouchableOpacity,
    View,
    Text,
    Image,
    Dimensions
} from 'react-native';
import ChartView from '../../lib/react-native-highcharts';
import Helper from '../../lib/Helper';

var width = Dimensions.get('window').width;
var height = Dimensions.get('window').height;

export default class DefaultChart extends Component {

  render() {
    const { title, data, months } = this.props;

    if(months) {
      var xAxis = {
        categories: months
      }
    } else {
      xAxis = {
        type: "category",
        showLastLabel: true,
        // step:1,

      }

      if(data.length > 15) {
        xAxis.tickInterval = 5;
      }

    }

    var Highcharts='Highcharts';
    var conf={
            chart: {
                type: 'line',
                animation: Highcharts.svg, // don't animate in old IE
                marginRight: 10,
                line: {
                    color:'red'
                },
            },
            title: {
                text: ''
            },
            xAxis: xAxis,
            yAxis: {
                title: {
                    text: ''
                },
                plotLines: [{
                    value: 1,
                    width: 1,
                    color: '#0f568f'
                }],
                labels: {
                    format: '{value}',
                    formatter: function(){
                        return (this.value) ? this.value.toLocaleString('pt-br') : null;
                    }
                },

            },
            tooltip: {
                formatter: function () {
                    // return Highcharts.numberFormat(this.y, 2);
                    return (this.y) ? this.y.toLocaleString('pt-br') : null;
                }
            },
            legend: {
                enabled: false
            },
            marker: {
                enabled: true,
                radius: 10
            },
            credits: {
              enabled: false
            },
            exporting: {
                enabled: false
            },
            series: [{
                name: title,
                data: data,
                color: '#005499',
                lineWidth:2.2,
                marker: {
                  enabled: true,
                  fillColor:'#00D5F7',
                  lineWidth:3,
                }
                /* data: (function () {
                    // generate an array of random data
                    var data = [],
                        time = (new Date()).getTime(),
                        i;

                    for (i = -19; i <= 0; i += 1) {
                        data.push({
                            x: time + i * 1000,
                            y: Math.random()
                        });
                    }
                    return data;
                }()) */
            }]
        };

    const options = {
        global: {
            useUTC: false,
        },
        lang: {
            decimalPoint: ',',
            thousandsSep: '.'
        },
    };

    return (
      <ChartView style={{height:200}} config={conf} options={options}></ChartView>
    );
  }
}
