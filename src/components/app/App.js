import React, { Component } from 'react';
import { StatusBar, View, Text, Alert, AppState, AsyncStorage } from 'react-native';
import { Provider } from 'react-redux';
import OneSignal from 'react-native-onesignal';

import { AppNavigator } from '../../routing/routes';
import store from '../../redux/store';

Text.allowFontScaling=false;

class App extends Component {
    constructor(props) {
      super(props);

      this.state = {
        appState: 'active',
        allowOpenAppCustomPopup: true,
      }

      this.updatePlayerId = this.updatePlayerId.bind(this);
      this.onIds = this.onIds.bind(this);
      this.onReceived = this.onReceived.bind(this);
      this.onOpened = this.onOpened.bind(this);
    }

    componentDidMount() {
      console.disableYellowBox = true;
      AppState.addEventListener('change', this._handleAppStateChange);
    }

    componentWillMount() {
       OneSignal.init("66a4e40c-0fe9-4180-ae76-59396f7919b9");
       OneSignal.addEventListener('received', this.onReceived);
       OneSignal.addEventListener('opened', this.onOpened);
       OneSignal.addEventListener('ids', this.onIds);
       OneSignal.inFocusDisplaying(0);

       // User.enableNotificationPreference();
       // OneSignal.configure();
    }

    componentWillUnmount() {
       OneSignal.removeEventListener('received', this.onReceived);
       OneSignal.removeEventListener('opened', this.onOpened);
       OneSignal.removeEventListener('ids', this.onIds);
       AppState.removeEventListener('change', this._handleAppStateChange);
    }

    onReceived(notification) {
      let _this = this;
      let appState = _this.state.appState;

      // if(appState == 'active') {
      _this.setState({allowOpenAppCustomPopup: false}, function() {
        _this.showNotificationAlert(notification);
      });
      // }

      if(appState != 'active') {
        _this.setState({allowOpenAppCustomPopup: true});
      }

      console.log("onesignal Notification received: ", notification);
    }

    onOpened(openResult) {
      // return false;
      let _this = this;
      // alert('Mensagem: ' + openResult.notification.payload.body);

      let allowOpenAppCustomPopup = _this.state.allowOpenAppCustomPopup;

      if(allowOpenAppCustomPopup) {
       _this.showNotificationAlert(openResult.notification);

       console.log('onesignal Message: ', openResult.notification.payload.body);
       console.log('onesignal Data: ', openResult.notification.payload.additionalData);
       console.log('onesignal isActive: ', openResult.notification.isAppInFocus);
       console.log('onesignal openResult: ', openResult);
      }

      _this.setState({allowOpenAppCustomPopup: true});
    }

    onIds(device) {
      console.log('onesignal Device info: ', device);

      // setting player id
      if(typeof device.userId != 'undefined' && device.userId != null) {

        let _this = this;

        AsyncStorage.setItem('@player_id', device.userId, function() {
          _this.updatePlayerId();
          User.enableNotificationPreference();
        });
      }
    }

    showNotificationAlert(notification) {
      Alert.alert(
        notification.payload.title ? notification.payload.title : 'Amigo Produtor',
        notification.payload.body,
        [
          {text: 'OK', onPress: () => console.log('OK Pressed')},
        ],
        { cancelable: false }
      );
    }

    async updatePlayerId() {
      console.log('onesignal updatePlayerId');
      let userId = await User.getId();
      let playerId = await User.getPlayerId();

      // console.log('onesignal userId0', userId);
      // console.log('onesignal playerId0', playerId);

      if(userId != null && playerId != null) {
        console.log('onesignal userId', userId);
        console.log('onesignal playerId', playerId);
        Api.updatePlayerId(userId, playerId)
        .then(function(data) {
          console.log('onesignal return player id', data);
        });
      }
    }

    _handleAppStateChange = (nextAppState) => {
      if (this.state.appState.match(/inactive|background/) && nextAppState === 'active') {
        console.log('App has come to the foreground!')
      }
      this.setState({appState: nextAppState});
    }

    render() {
        return (
            <View style={{ flex: 1 }}>
              <StatusBar
                    backgroundColor="rgba(0,0,0,0)"
                    translucent
                    barStyle="light-content"
                />
                <Provider store={store}>
                    <AppNavigator />
                </Provider>
            </View>
        );
    }
}

export default App;
