import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { StyleSheet, View, TextInput, Text, ScrollView, TouchableWithoutFeedback, Dimensions, RefreshControl } from 'react-native';
import { Button } from 'native-base';
import { Col, Row, Grid } from 'react-native-easy-grid';
import Image from 'react-native-remote-svg';
import IndicatorTableTitleMultipleColumns from '../../components/IndicatorTableTitleMultipleColumns';
import NoDataButton from '../../components/NoDataButton';
import AnimateNumber from 'react-native-animate-number';
import Spinner from 'react-native-loading-spinner-overlay';

import Chart from '../../libs/component/Chart';
import HomeTab from '../../../routing/tabs/HomeTab';
import Helper from '../../../lib/Helper';

var width = Dimensions.get('window').width;
var height = Dimensions.get('window').height;

export default class Indicators extends Component {

  constructor(props) {
    super();

    this.state = {
      dataNY11: null,
      dataConsecana: null,
      dolar: null,
      euro: null,
      igpm: null,
      isLoading: false,
      emptyData: false
    };

    this.loadIndexes = this.loadIndexes.bind(this);
  }

  componentDidMount() {
    this.loadIndexes();
  }

  loadIndexes() {

    console.log('emptyData loadIndexes');
    let _this = this;

    startDate = Helper.getCurrentDateInverse(); // dd-mm-yyyy
    endDate = startDate;

    var dataConsecana = [];

    _this.setState({ isLoading: true });

    Api.getLastConsecanaIndex()
      .then(function (data1) {

        if (typeof data1 == 'undefined') {
          setTimeout(() => {
            alert("Usuário sem permissão");
          }, 100);
          throw 'Usuário sem permissão';
        }

        console.log('emptyData indicators 1', data1);

        if (typeof data1 != 'undefined' && data1 != null && data1.length) {
          console.log('indicators 2', data1);
          item = data1[0];

          var month = Helper.getMonth(parseInt(item.Mes));
          dataConsecana = [
            { value: month },
            { value: item.ValorMensal },
            { value: item.ValorAcumulado }
          ];
        }

        Api.getIndexes()
          .then(function (data) {

            if (typeof data == 'undefined') {
              setTimeout(() => {
                alert("Usuário sem permissão");
              }, 100);
              throw 'Usuário sem permissão';
            }

            console.log('emptyData indicators', data);

            if (data) {
              var dataNY11 = [
                { value: Helper.getMonthNameByMonthYear(data.NY11DataAtualizacao) },
                { value: Helper.formatNumber(data.NY11Valor) },
                { value: Helper.formatIndex(data.NY11ValorVariacao), variation: data.NY11DirecaoVariacao }
              ];
              var dolar = [
                { value: 'US$ ' + Helper.formatNumber(data.DolarComercialValorVenda, 4) },
                { value: 'US$ ' + Helper.formatNumber(data.DolarComercialValorCompra, 4) },
                { value: Helper.formatIndex(data.DolarComercialValorVariacao), variation: data.DolarComercialDirecaoVariacao }
              ];
              var euro = [
                { value: '€ ' + Helper.formatNumber(data.EuroValorVenda, 4) },
                { value: '€ ' + Helper.formatNumber(data.EuroValorCompra, 4) },
                { value: Helper.formatIndex(data.EuroValorVariacao), variation: data.EuroDirecaoVariacao }
              ];
              var igpm = [
                { value: Helper.getMonthNameByMonthYear(data.IGPMDataAtualizacao) },
                { value: Helper.formatNumber(data.IGPMValor) },
                { value: Helper.formatIndex(data.IGPMValorVariacao), variation: data.IGPMDirecaoVariacao }
              ];

              _this.setState({
                dataConsecana: dataConsecana,
                dataNY11: dataNY11,
                dolar: dolar,
                euro: euro,
                igpm: igpm,
                isLoading: false,
                emptyData: false
              });
            } else {
              console.log('emptyData data', data);
              _this.setState({
                isLoading: false,
                emptyData: true
              });
            }
          })
          .catch(function (err) {
            _this.setState({ isLoading: false, emptyData: true });
          });


        /* var data1 = data.indicadorNy;
        if(data1 != null && data1.Indices.length) {
          var month = Helper.getMonth(parseInt(data1.Indices[0].Data.substr(5, 2)));
          var dataNY11 = [month, Helper.formatNumber(data1.Indices[0].Settle), data1.Indices[0].Change];
        } else {
          dataNY11 = ['-', '0,00', '0,00'];
        }
  
        var data2 = data.indicadoresConsecana;
        var month = Helper.getMonth(parseInt(data2[0].Mes));
        var dataConsecana = [month, data2[0].ValorMensal, data2[0].ValorAcumulado];
  
        var dataDolarVenda = data.DolarVenda[0];
        var dataDolarCompra = data.DolarCompra[0];
  
        if(dataDolarVenda && dataDolarCompra) {
          var dolar = [
            'US$ ' + Helper.formatNumber(dataDolarVenda.Valores[0].SValor, 4),
            'US$ ' + Helper.formatNumber(dataDolarCompra.Valores[0].SValor, 4),
            dataDolarVenda.Variacao + '%'
          ];
        } else {
          var dolar = ['-', '-', '-'];
        }
  
        var dataEuroVenda = data.EuroVenda[0];
        var dataEuroCompra = data.EuroCompra[0];
  
        if(dataEuroVenda && dataEuroCompra) {
          var euro = [
            '€ ' + Helper.formatNumber(dataEuroVenda.Valores[0].SValor, 4),
            '€ ' + Helper.formatNumber(dataEuroCompra.Valores[0].SValor, 4),
            dataEuroVenda.Variacao + '%'
          ];
        } else {
          var euro = ['-', '-', '-'];
        }
  
        console.log('dataNY11 1', dataNY11);
        console.log('dataConsecana 1', dataConsecana);
        console.log('dolar 1', dolar);
        console.log('euro 1', euro);
  
        _this.setState({dataNY11: dataNY11, dataConsecana: dataConsecana, dolar: dolar, euro: euro, isLoading: false});
        */
      })
      .catch(function (err) {
        _this.setState({ isLoading: false, emptyData: true });
      });
  }

  render() {
    let _this = this;
    const { navigate, state } = this.props.navigation;

    var { dataNY11, dataConsecana, dolar, euro, igpm, emptyData } = this.state;

    console.log('emptyData', emptyData);

    return (
      <View style={{ backgroundColor: '#F7F7F7' }}>
        <ScrollView style={styles.scrollViewContainer}>
          <View>
            <Text style={styles.title}>Indicadores</Text>
          </View>
          {this.state.isLoading ?
            <View style={{ flex: 1 }}>
              <Spinner visible={true} textStyle={{ color: '#FFF' }} />
            </View>
            : null}

          {emptyData ?
            <NoDataButton onPress={_this.loadIndexes} />
            : null}

          {dataConsecana ?
            <IndicatorTableTitleMultipleColumns
              title={'Consecana SP'}
              labels={['Mês', 'Mensal', 'Acumulada']}
              values={dataConsecana}
              showButton={'Ver histórico'}
              buttonAction={() => { navigate('IndicatorsReportScreen', { title: 'Histórico', back: 'IndicatorsScreen' }); }}
            />
            : null
          }
          {dataNY11 ?
            <IndicatorTableTitleMultipleColumns
              title={'NY#11'}
              labels={['Mês', 'Índice', 'Variação']}
              values={dataNY11}
            />
            : null
          }
          {dolar ?
            <IndicatorTableTitleMultipleColumns
              title={'Dólar Comercial'}
              labels={['Venda', 'Compra', 'Variação Compra']}
              values={dolar}
            />
            : null}
          {euro ?
            <IndicatorTableTitleMultipleColumns
              title={'Euro'}
              labels={['Venda', 'Compra', 'Variação Compra']}
              values={euro}
            />
            : null}
          {!this.state.isLoading ?
            <IndicatorTableTitleMultipleColumns
              title={'IGP-M'}
              labels={['Mês', 'Índice', 'Variação']}
              values={igpm}
            />
            : null}
        </ScrollView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  scrollViewContainer: {

  },
  container: {
    color: '#cc00ff',
    justifyContent: 'center',
    alignItems: 'center',
  },
  topContainer: {
    flexDirection: 'row',
    margin: 0,
    marginTop: 10,
    justifyContent: 'space-around',
    alignItems: 'center',
    marginBottom: 10,

  },
  tab: {
    padding: 0,
    alignItems: 'center',

  },
  activeIconStyle: {

  },
  inactiveIconStyle: {

  },

  activeTextStyle: {

  },
  inactiveTextStyle: {

  },
  img: {
    borderColor: '#D4D4D4',
    width: 190,
    height: 52
  },
  title: {
    fontSize: 23,
    marginTop: 10,
    marginBottom: 15,
    marginLeft: 15,
    fontWeight: 'bold',
    color: '#005499',
    fontFamily: 'Montserrat',
  },
  rate: {
    fontSize: 13,
    fontWeight: 'bold',
    color: 'black',
    fontFamily: 'Montserrat'
  },
  middle: {
    backgroundColor: '#EBEBEB',
    alignItems: 'center',
    justifyContent: 'center',
    fontFamily: 'Montserrat'
  },
  middleTitle: {
    color: '#005499',
    fontSize: 32,
    fontWeight: 'bold',
    paddingTop: 20,
    paddingBottom: 8,
    alignItems: 'center',
    justifyContent: 'center',
    fontFamily: 'Montserrat'
  },
  accountimg: {
    width: 45,
    height: 45,
    justifyContent: 'flex-end',
    alignItems: 'flex-end',
    marginRight: 30,
    fontFamily: 'Montserrat',
  },
  account: {
    justifyContent: 'flex-end',
    alignItems: 'flex-end',
    marginTop: 10,
    height: 60
  },
  Luis: {
    justifyContent: 'flex-end',
    alignItems: 'flex-end',
    color: '#005499',
    fontSize: 14,
    marginRight: 20
  },
  cardBox: {
    marginTop: 18,
    marginBottom: 18,
  },
  NameItem: {
    backgroundColor: 'white',
    paddingTop: 22,
    paddingBottom: 22,
    paddingLeft: 20,
    paddingRight: 20,
    marginLeft: 15,
    marginRight: 15,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 4,
    borderColor: '#F2F6F7',
    borderWidth: 2

  },
  name: {
    fontSize: 10,
    color: 'black',
    fontFamily: 'Montserrat',
  },
  boxTitle: {
    fontSize: 12,
    color: 'black',
    fontFamily: 'Montserrat',
    fontWeight: 'bold',
    marginBottom: 5
  },
  goalName: {
    color: '#00D5F7',
    fontSize: 50,
    fontWeight: 'bold',
    marginTop: -8,
    fontFamily: 'Montserrat',
  },
  ton: {
    fontWeight: 'bold',
    fontSize: 20,
    color: '#005499',
    fontFamily: 'Montserrat',
  },
  goalton: {
    color: '#005499',
    fontSize: 12,
    fontWeight: 'bold',
    fontFamily: 'Montserrat',
  },
  row: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    marginTop: -20,
    marginLeft: 15,
    marginRight: 15,
    flex: 1
  },
  column: {
    flexDirection: 'row',
    width: width
  },
  gridRowContent: {
    fontSize: 8,
    padding: 0,
    margin: 0
  },
  TextContent1Small: {
    backgroundColor: 'white',
    fontSize: 9,
    marginLeft: 1,
    marginTop: 20,
    paddingTop: 15,
    paddingBottom: 15,
    fontWeight: 'bold',
    paddingLeft: 18,
    color: 'black',
    fontFamily: 'Montserrat',
  },
  TextContent1: {
    backgroundColor: 'white',
    fontSize: 12,
    marginLeft: 1,
    marginTop: 20,
    paddingTop: 15,
    paddingBottom: 15,
    fontWeight: 'bold',
    paddingLeft: 18,
    color: '#005499',
    fontFamily: 'Montserrat',
  },
  NumberContent1: {
    backgroundColor: 'white',
    fontSize: 12,
    marginRight: 1,
    marginTop: 20,
    paddingTop: 15,
    paddingBottom: 15,
    fontWeight: 'bold',
    justifyContent: 'flex-end',
    color: '#005499',
    fontFamily: 'Montserrat',
  },
  TextContent2: {
    backgroundColor: 'white',
    fontSize: 12,
    paddingTop: 15,
    paddingBottom: 15,
    paddingLeft: 30,
    marginLeft: 1,
    marginTop: 20,
    fontWeight: 'bold',
    color: '#FF892A',
    alignItems: 'center',
    justifyContent: 'center',
    fontFamily: 'Montserrat',
  },
  NumberContent2: {
    backgroundColor: 'white',
    fontSize: 12,
    marginRight: 1,
    marginTop: 20,
    paddingTop: 15,
    paddingBottom: 15,
    fontWeight: 'bold',
    justifyContent: 'flex-end',
    color: '#FF892A',
    fontFamily: 'Montserrat',
  },
  goalmemo: {
    marginTop: 40,
    fontSize: 8,
    justifyContent: 'flex-start',
    paddingRight: 80,
    alignItems: 'flex-start',
    fontFamily: 'Montserrat',
  },
  name2: {
    fontWeight: 'bold',
    fontSize: 26,
    color: '#005499',
    fontFamily: 'Montserrat',
  },
  goalName2: {
    color: '#00D5F7',
    fontSize: 50,
    fontWeight: 'bold',
    marginTop: -4,
    fontFamily: 'Montserrat',
  },
});
