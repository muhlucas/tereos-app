import React, { Component } from 'react';
import PropTypes from 'prop-types';
import {
  StyleSheet,
  View,
  TextInput,
  Text,
  ScrollView,
  TouchableWithoutFeedback,
  Dimensions,
  AsyncStorage,
  PixelRatio,
  PushNotificationIOS,
  Alert } from 'react-native';
import { Col, Row, Grid } from 'react-native-easy-grid';
import Image from 'react-native-remote-svg';
import Home2Component from '../../libs/component/Home2Component';
import AnimateNumber from 'react-native-animate-number';
// import Chart from '../../libs/component/Chart';
import DefaultChart from '../../components/DefaultChart';
import HomeTab from '../../../routing/tabs/HomeTab';
import OneSignal from 'react-native-onesignal';
import Spinner from 'react-native-loading-spinner-overlay';

import Api from '../../../lib/Api';
import User from '../../../lib/User';

var width = Dimensions.get('window').width;
var height = Dimensions.get('window').height;

export default class Home1 extends Component {

  constructor(props) {
    super();

    this.state = {
      data1: [],
      dataAtr: [],
      data2: [],
      tonDelivered: 0,
      tonDeliveredYear: 0,
      lastAtrReported: '',
      atrProvisional: 0,
      atrRelative: 0,
      provider1Q: 0,
      provider2Q: 0,
      plant1Q: 0,
      plant2Q: 0,
      updatedAt: '',
      monthUpdatedAt: '',
      isLoading: false
    };
  }

  componentDidMount() {
    let _this = this;

    _this.setState({isLoading: true}, function() {
      _this.loadMainReport();
      _this.loadAppOffline();
      // _this.updatePlayerId();

      // clearing badges
      // PushNotificationIOS.setApplicationIconBadgeNumber(0);
      // OneSignal.configure();
    });
  }

  async loadAppOffline() {
    let _this = this;

    try {

      var id = await User.getId();
      var year = Helper.getCurrentYear();
      var pastMonth = Helper.getPastMonth();
      var month = Helper.getCurrentMonth();
      // news
      startDate = Helper.get6MonthsAgoDate();
      endDate = Helper.getCurrentDate();
      pageIndex = 0;

      promises = [
        Api.getMonthlySummaryData(id, year, pastMonth),
        Api.getAnualSummaryData(id, year),
        Api.getIndexes(),
        Api.getLastConsecanaIndex(),
        Api.getFullConsecanaIndexes(),
        Api.getEvents(id),
        Api.getNews(startDate, endDate, pageIndex),
        Api.getNews(startDate, endDate, pageIndex+3),
        Api.getDailyProductionReport(id, year, month)
      ]

      Promises.all(promises)
      .then(function(data) {
        // do nothing
      }).catch(function(err) {
        // do nothing
      });

    } catch(err) {
      // do nothing
    }
  }

  async loadMainReport() {
    var id = await User.getId();
    var month = Helper.getCurrentMonth();
    var year = Helper.getCurrentYear();

    let _this = this;

    Api.getMainReport(id, year, month)
    .then(function(data) {

      if(data) {
        report1 = [];
        reportAtr = [];
        tonDelivered = 0;
        updatedAt = null;
        monthUpdatedAt = null;
        for(idx in data) {
          item = data[idx];
          day = parseInt(item.Data.substr(8,2));
          ton = item.Ton;
          atr = item.Atr;
          report1.push({x: day, y: ton});
          reportAtr.push({x: day, y: atr});

          tonDelivered += parseInt(ton);
          updatedAt = item.Data.substr(8,2) + '/' + item.Data.substr(5,2);
          monthUpdatedAt = Helper.getMonth(parseInt(item.Data.substr(5,2)));
        }

        /*
        // mock for all days
        report1 = [
          {x: 1, y: 59},
          {x: 2, y: 201},
          {x: 3, y: 40},
          {x: 4, y: 504},
          {x: 5, y: 300},
          {x: 6, y: 30},
          {x: 7, y: 300},
          {x: 8, y: 222},
          {x: 9, y: 200},
          {x: 10, y: 222},
          {x: 11, y: 259},
          {x: 12, y: 444},
          {x: 13, y: 832},
          {x: 14, y: 102},
          {x: 15, y: 159},
          {x: 16, y: 100},
          {x: 17, y: 204},
          {x: 18, y: 234},
          {x: 19, y: 100},
          {x: 20, y: 80},
          {x: 21, y: 59},
          {x: 22, y: 399},
          {x: 23, y: 200},
          {x: 24, y: 102},
          {x: 25, y: 190},
          {x: 26, y: 59},
          {x: 27, y: 120},
          {x: 28, y: 430},
          {x: 29, y: 250},
          {x: 30, y: 590},
          {x: 31, y: 59},
        ] */

        console.log('data1 report', report1);

        updatedAt = (updatedAt == null) ? '' : updatedAt;
        monthUpdatedAt = (monthUpdatedAt == null) ? '' : monthUpdatedAt;

        try {
          AsyncStorage.multiSet([['@dashboardUpdateAt', updatedAt], ['@dashboardMonthUpdateAt', monthUpdatedAt]], function() {
            _this.setState({
                data1: report1,
                tonDelivered: tonDelivered,
                updatedAt: updatedAt,
                monthUpdatedAt: monthUpdatedAt,
                dataAtr: reportAtr,
                // isLoading: false
              });
          });
        } catch(e) {
          _this.setState({isLoading: false});
        }

      } else {
        _this.setState({isLoading: false});
      }

      var tonDeliveredYear = 0;

      _this.setState({isLoading: true}, function() {
        Api.getReportMonth(id, year)
        .then(function(data) {
          console.log('reportMonth data', data);

          if(data) {
            dataByMonth = [];

            for(idx in data.DadosHome) {
              item = data.DadosHome[idx];

              if(item.Mes == null) {
                continue;
              }

              month = parseInt(item.Mes);

              ton = (item.Toneladas) ? parseInt(item.Toneladas) : 0;
              atr = (item.ATR) ? parseFloat(item.ATR.replace(',', '.')) : 0;

              if(typeof dataByMonth[month] == 'undefined') {
                dataByMonth[month] = {ton: ton, atr: atr};
                dataByMonth[month] = {ton: ton, atr: atr};
              } else {
                dataByMonth[month].ton += ton;
              }

              tonDeliveredYear += ton;
            }

            report2 = [];
            report3 = [];
            var lastAtrReported = '';
            var sumAtr = 0;
            var avgAtr = 0;
            for(idx in dataByMonth) {
              item = dataByMonth[idx];
              ton = item.ton;
              atr = item.atr;
              report2.push({x: parseInt(idx), y:ton});
              report3.push({x: parseInt(idx), y:atr});
              lastAtrReported = atr;
              sumAtr += atr;
            }

            avgAtr = data.AtrMedia;

            _this.setState({
              data2: report2,
              data3: report3,
              tonDeliveredYear: tonDeliveredYear,
              lastAtrReported: (lastAtrReported) ? lastAtrReported.toLocaleString('pt-br') : null,
              avgAtr: avgAtr,
              // isLoading: false
            });

            console.log('data2', data);
            console.log('dataByMonth', dataByMonth);
            console.log('report2', report2);
            console.log('report3', report3);
          }

          _this.setState({isLoading: true}, function() {
            var month = Helper.getCurrentMonth();
            Api.getHomeInfo(id, year, month)
            .then(function(data) {
              if(data) {
                obj = {
                  atrProvisional: (data.AtrProvisorio == null) ? '0,00' : Helper.formatNumber(data.AtrProvisorio),
                  atrRelative: (data.AtrRelativo == null) ? '0,00' : data.AtrRelativo,
                  provider1Q: (data.Fornecedor1Q == null) ? '0,00' : data.Fornecedor1Q,
                  provider2Q: (data.Fornecedor2Q == null) ? '0,00' : data.Fornecedor2Q,
                  plant1Q: (data.Usina1Q == null) ? '0,00' : data.Usina1Q,
                  plant2Q: (data.Usina2Q == null) ? '0,00' : data.Usina2Q,
                };
                _this.setState(obj);
              }

              _this.setState({isLoading: false});
            }).catch(function(err) {
              _this.setState({isLoading: false});
            });
          });

        }).catch(function(err) {
          _this.setState({isLoading: false});
        });
      });

    }).catch(function(err) {
      _this.setState({isLoading: false});
    });


  }

  render() {
    const { navigate, state } = this.props.navigation;

    var data1 = this.state.data1;
    var dataAtr = this.state.dataAtr;
    var data2 = this.state.data2;
    var data3 = this.state.data3;

    months = [];
    for(idx in data2) {
      item = data2[idx];
      months[item.x] = Helper.getMonthShort(item.x);
    }

    return (
      <View style={{backgroundColor:'#F7F7F7'}}>
        {this.state.isLoading ?
          <Spinner visible={true} textStyle={{color: '#FFF'}} />
        : null }
        <HomeTab navigation={this.props.navigation}/>
        <ScrollView style={styles.scrollViewContainer}>
        {data1.length ?
          <View style={styles.container}>
            <Text style={styles.title}>{this.state.monthUpdatedAt}</Text>
            <Text style={styles.name}>Atualizado até</Text>
            <Text style={styles.rate}>{this.state.updatedAt}</Text>
          </View>
        : // nao retornou no ws, exibe ultima atualizacao como a data atual
          <View style={styles.container}>
            <Text style={styles.title}>{this.state.isLoading ? ' ' : Helper.getMonth(Helper.getCurrentMonth())}</Text>
            <Text style={styles.name}>Atualizado até</Text>
            <Text style={styles.rate}>{this.state.isLoading ? '-' : Helper.getCurrentDateShort()}</Text>
          </View>
        }

        <View style={styles.cardBox}>
          {data1.length ?
            <View>
              <View style={styles.NameItem}>
                <Text style={styles.boxTitle}>Cana Entregue</Text>
                <Text style={styles.goalName}>{Helper.formatNumber(this.state.tonDelivered, 0)}<Text style={{fontSize:12, fontWeight:'normal'}}>ton</Text></Text>
              </View>
              <View style={styles.ChartItem}>
                <DefaultChart title={'Cana entregue'} data={data1} />
              </View>
            </View>
          :
          <View>
            <View style={styles.NameItem}>
              <Text style={styles.boxTitle}>Cana Entregue</Text>
              <Text style={styles.goalNameSmall}>{this.state.isLoading ? 'Carregando' : 'Em levantamento'}</Text>
            </View>
            <View style={styles.ChartItem}>
              <View style={styles.chartNoData}>
                <Text style={styles.chartMessage}>
                  {this.state.isLoading ? 'Carregando...' : 'Não há dados disponíveis no momento para o gráfico.'}
                </Text>
              </View>
            </View>
          </View>
          }
        </View>

        <View style={[styles.cardBox, {marginBottom:0}]}>
          <View style={styles.NameItem}>
            <Text style={styles.ton}>ATR</Text>
            <Text style={styles.goalton}>{'(kg/tonelada)'}</Text>
          </View>
          <View style={styles.row}>
            <Grid>
                <Col>
                  <Grid>
                    <Col size={70}>
                      <Text style={styles.TextContent1}>Fornecedor 1ªQ</Text>
                    </Col>
                    <Col size={30}>
                      <Text style={styles.NumberContent1}>{Helper.formatNumber(this.state.provider1Q, 0)}</Text>
                    </Col>
                  </Grid>
                </Col>
                <Col>
                  <Grid>
                    <Col size={70}>
                      <Text style={styles.TextContent2}>Fornecedor 2ªQ</Text>
                    </Col>
                    <Col size={30}>
                      <Text style={styles.NumberContent2}>{Helper.formatNumber(this.state.provider2Q, 0)}</Text>
                    </Col>
                  </Grid>
                </Col>
            </Grid>
          </View>
        </View>
        <View style={[styles.cardBox, {marginTop:0}]}>
          {/* <View style={styles.row}>
            <Grid>
                <Col>
                  <Grid>
                    <Col size={50}>
                      <View style={styles.LabelContent1}>
                        <Text style={styles.TextContent1Small}>Fornecedor</Text>
                      </View>
                    </Col>
                    <Col size={50}>
                      <View style={styles.NumberContent}>
                        <Text style={styles.NumberContentText}>1ªQ  </Text>
                        <Text style={styles.NumberContentText}>2ªQ  </Text>
                      </View>
                    </Col>
                  </Grid>
                </Col>
                <Col>
                  <Grid>
                    <Col size={50}>
                      <View style={styles.LabelContent1}>
                        <Text style={styles.TextContent1Small}>Usina</Text>
                      </View>
                    </Col>
                    <Col size={50}>
                      <View style={styles.NumberContent}>
                        <Text style={styles.NumberContentText}>1ªQ  {Helper.formatNumber(this.state.plant1Q)}</Text>
                        <Text style={styles.NumberContentText}>2ªQ  {Helper.formatNumber(this.state.plant2Q)}</Text>
                      </View>
                    </Col>
                  </Grid>
                </Col>
            </Grid>
          </View> */}
          <View style={[styles.ChartItem, {marginTop:0}]}>
            {dataAtr.length ?
            <DefaultChart data={dataAtr} />
            :
              <View style={styles.chartNoData}>
                <Text style={styles.chartMessage}>
                  {this.state.isLoading ? 'Carregando...' : 'Não há dados disponíveis no momento para o gráfico.'}
                </Text>
              </View>
            }
          </View>
        </View>

        <View style={styles.headerBox}>
        </View>

        <View style={styles.middle}>
          <Text style={styles.middleTitle}>
            Safra
          </Text>
        </View>

        <View style={styles.cardBox}>
          <View style={styles.NameItem}>
            <Text style={styles.boxTitle}>Cana Entregue</Text>
            {data2 && data2.length ?
              <Text style={styles.goalName}>{Helper.formatNumber(this.state.tonDeliveredYear, 0)}<Text style={{fontSize:10, fontWeight:'normal'}}>ton</Text></Text>
            :
              <Text style={styles.goalNameSmall}>Em levantamento</Text>
            }
          </View>
          <View style={styles.ChartItem}>
            {data2 && data2.length ?
              <DefaultChart title={'Cana entregue'} data={data2} months={months} />
            :
              <View style={styles.chartNoData}>
                <Text style={styles.chartMessage}>
                  {this.state.isLoading ? 'Carregando...' : 'Não há dados disponíveis no momento para o gráfico.'}
                </Text>
              </View>
            }
          </View>
        </View>

        <View style={styles.cardBox}>
          <View style={styles.NameItem}>
            <Text style={styles.name2}>ATR Méd. Fornecedor*</Text>
            {data3 && data3.length ?
              <View style={{alignItems:'center'}}>
                <Text style={styles.goalName2}>{Helper.formatNumber(this.state.avgAtr)}<Text style={{fontSize:10, fontWeight:'normal'}}>ton</Text></Text>
                <Text style={styles.goalmemo}>{'*ATR sofrerá reajuste em dezembro conforme apurado.'} </Text>
              </View>
            :
              <Text style={styles.goalName2Small}>Em levantamento</Text>
            }
          </View>
          <View style={styles.ChartItem}>
            {data3 && data3.length ?
              <DefaultChart data={data3} months={months} />
            :
              <View style={styles.chartNoData}>
                <Text style={styles.chartMessage}>
                  {this.state.isLoading ? 'Carregando...' : 'Não há dados disponíveis no momento para o gráfico.'}
                </Text>
              </View>
            }
          </View>
        </View>


        <View style={styles.account}>
          {/*
          <Image style={styles.accountimg}
              source={require('../../../assets/img/account.png')}
          />
          <Text style={styles.Luis}>Gestor Luis</Text>
          */}
        </View>

        </ScrollView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
    scrollViewContainer: {

    },
    container: {
      justifyContent:'center',
      alignItems:'center',
    },
    topContainer: {
        flexDirection: 'row',
        margin:0,
        marginTop:10,
        justifyContent:'space-around',
        alignItems: 'center',
        marginBottom:10,
        backgroundColor: '#F7F7F7'
    },
    tab: {
        padding:0,
        alignItems: 'center',

    },
    activeIconStyle: {

    },
    inactiveIconStyle: {

    },

    activeTextStyle: {

    },
    inactiveTextStyle: {

    },img:{
      borderColor:'#D4D4D4',
      width:190,
      height:52
    },

    title:{
      fontSize:26,
      marginTop:0,
      fontWeight:'bold',
      color:'#005499',
      fontFamily: 'Montserrat',
    },
    rate:{
      fontSize:15,
      fontWeight:'bold',
      color:'black',
      fontFamily: 'Montserrat'
    },
    middle:{
      backgroundColor:'#EBEBEB',
      alignItems:'center',
      justifyContent:'center',
    },
    middleTitle:{
      color:'#005499',
      fontSize:32,
      fontWeight:'bold',
      paddingTop:20,
      paddingBottom:8,
      alignItems:'center',
      justifyContent:'center',
      fontFamily: 'Montserrat'
    },
    accountimg:{
      width:45,
      height:45,
      justifyContent: 'flex-end',
      alignItems:'flex-end',
      marginRight:30,
      fontFamily: 'Montserrat',
    },
    account:{
      justifyContent: 'flex-end',
      alignItems:'flex-end',
      marginTop:10,
      height:60
    },
    Luis:{
      justifyContent: 'flex-end',
      alignItems:'flex-end',
      color:'#005499',
      fontSize:14,
      marginRight:20
    },
    cardBox: {
      marginTop:18,
      marginBottom:18,
    },
    headerBox: {

    },
    NameItem:{
      backgroundColor:'white',
      paddingTop:22,
      paddingBottom:22,
      paddingLeft:20,
      paddingRight:20,
      marginLeft:15,
      marginRight:15,
      alignItems:'center',
      borderRadius:4,
      borderColor:'#F2F6F7',
      borderWidth:2,
      borderBottomWidth:0
    },
    ChartItem:{
      backgroundColor:'white',
      paddingBottom:22,
      paddingLeft:20,
      paddingRight:20,
      marginLeft:17,
      marginRight:17,
    },
    name:{
      fontSize:12,
      color:'black',
      fontFamily: 'Montserrat',
    },
    boxTitle: {
      fontSize:12,
      color:'black',
      fontFamily: 'Montserrat',
      fontWeight:'bold',
      marginBottom:5
    },
    goalName:{
      color:'#00D5F7',
      fontSize:50,
      fontWeight:'bold',
      marginTop:-8,
      fontFamily: 'Montserrat',
    },
    goalNameSmall:{
      color:'#00D5F7',
      fontSize:20,
      fontWeight:'bold',
      marginTop:0,
      fontFamily: 'Montserrat',
    },
    ton:{
      fontWeight:'bold',
      fontSize:20,
      color:'#005499',
      fontFamily: 'Montserrat',
    },
    goalton:{
      color:'#005499',
      fontSize:12,
      fontWeight:'bold',
      fontFamily: 'Montserrat',
    },
    row:{
      flexDirection:'row',
      alignItems:'center',
      justifyContent:'space-between',
      marginTop:-20,
      marginLeft:15,
      marginRight:15,
      flex:1
    },
    column: {
      flexDirection:'row',
      width:width
    },
    gridRowContent: {
      fontSize:8,
      padding:0,
      margin:0
    },
    TextContent1Small: {
      backgroundColor:'white',
      fontSize:width/38,
      marginLeft:1,
      paddingTop:15,
      paddingBottom:15,
      fontWeight:'bold',
      paddingLeft:18,
      color:'black',
      fontFamily: 'Montserrat',
    },
    NumberContent: {
      backgroundColor:'white',
      marginTop:20,
      height:44,
      paddingTop:8,
      paddingBottom:2,
      paddingLeft:25,
    },
    NumberContentText: {
      backgroundColor:'white',
      fontSize:width/40,
      paddingBottom:4,
      color:'black',
      fontFamily: 'Montserrat',
    },
    LabelContent1: {
      backgroundColor:'white',
      height:44,
      marginTop:20,
    },
    TextContent1:{
      backgroundColor:'white',
      fontSize:width/35,
      marginLeft:1,
      marginTop:20,
      paddingTop:15,
      paddingBottom:15,
      fontWeight:'bold',
      paddingLeft:18,
      color:'#005499',
      fontFamily: 'Montserrat',
    },
    NumberContent1:{
      backgroundColor:'white',
      fontSize:width/35,
      marginRight:1,
      marginTop:20,
      paddingTop:15,
      paddingBottom:15,
      fontWeight:'bold',
      justifyContent: 'flex-end',
      color:'#005499',
      fontFamily: 'Montserrat',
    },
    TextContent2:{
      backgroundColor:'white',
      fontSize:width/35,
      paddingTop:15,
      paddingBottom:15,
      paddingLeft:15,
      marginLeft:1,
      marginTop:20,
      fontWeight:'bold',
      color:'#FF892A',
      alignItems:'center',
      justifyContent:'center',
      fontFamily: 'Montserrat',
    },
    NumberContent2:{
      backgroundColor:'white',
      fontSize:width/35,
      marginRight:1,
      marginTop:20,
      paddingTop:15,
      paddingBottom:15,
      fontWeight:'bold',
      justifyContent: 'flex-end',
      color:'#FF892A',
      fontFamily: 'Montserrat',
    },
    goalmemo:{
      marginTop:30,
      fontSize:8,
      justifyContent:'flex-start',
      paddingRight:80,
      alignItems:'flex-start',
      fontFamily: 'Montserrat',
    },
    name2:{
      fontWeight:'bold',
      // fontSize:24,
      fontSize:width/17,
      color:'#005499',
      fontFamily: 'Montserrat',
    },
    goalName2:{
      color:'#00D5F7',
      // fontSize:50,
      fontSize:width/7,
      fontWeight:'bold',
      marginTop:-4,
      fontFamily: 'Montserrat',
    },
    goalName2Small:{
      color:'#00D5F7',
      fontSize:20,
      fontWeight:'bold',
      marginTop:5,
      fontFamily: 'Montserrat',
    },
    chartNoData: {
      height:200,
      alignItems:'center',
      justifyContent:'center'
    },
    chartMessage: {
      fontFamily: 'Montserrat',
      color:'#005499',
      fontSize:11,
      textAlign:'center'
    }
});
