import { AppRegistry } from 'react-native';
import App from './src/components/app/App';

AppRegistry.registerComponent('Tereos', () => App);
