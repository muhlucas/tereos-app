import React, { Component } from 'react';
import { AsyncStorage, StyleSheet, Text, TouchableWithoutFeedback, View } from 'react-native';
import FontAwesome, { Icons } from 'react-native-fontawesome';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { NavigationActions } from 'react-navigation'

import { Col, Row, Grid } from 'react-native-easy-grid';

import Image from 'react-native-remote-svg';


// TODO: Separate component from container in the new containers dir
// components dir should contain only stateless components
// connected should be kept in containers dir.
// Will disable eslint on this file due to required refactoring
/* eslint-disable */
class NavTabBar extends Component {
    componentDidMount() {

    }

    render() {
        const { navigate, state } = this.props.navigation;
        const { index, routes } = state;
        const active = routes[index].key;

        return (
            <View style={styles.container}>
              <Grid>
                <Row style={styles.navBarRow}>
                  <Col>
                    <TouchableWithoutFeedback onPress={() => navigate('REPORTS')}>
                        <View style={styles.tab}>
                            <Image style={styles.iconImg}
                                source={active === 'REPORTS' ? require('../../assets/img/icon_relatorios_on.png') : require('../../assets/img/icon_relatorios_off.png')}
                            />
                            <Text style={[styles.tabBarText, {color: (active === 'REPORTS') ? '#f99439' : '#acafb0' }]}>Relatórios</Text>
                        </View>
                    </TouchableWithoutFeedback>
                  </Col>
                  <Col>
                    <TouchableWithoutFeedback onPress={() => navigate('INDICATORS')}>
                        <View style={styles.tab}>
                            <Image style={styles.iconImg}
                                source={active === 'INDICATORS' ? require('../../assets/img/icon_indicadores_on.png') : require('../../assets/img/icon_indicadores_off.png')}
                            />
                            <Text style={[styles.tabBarText, {color: (active === 'INDICATORS') ? '#f99439' : '#acafb0' }]}>Indicadores</Text>
                        </View>
                    </TouchableWithoutFeedback>
                  </Col>
                  <Col>
                    <TouchableWithoutFeedback onPress={() => navigate('HOME')}>
                        <View style={styles.tab}>
                          <Image style={styles.iconImg}
                              source={active === 'HOME' ? require('../../assets/img/icon_home_on.png') : require('../../assets/img/icon_home_off.png')}
                          />
                          <Text style={[styles.tabBarText, {color: (active === 'HOME') ? '#f99439' : '#acafb0' }]}>Home</Text>
                        </View>
                    </TouchableWithoutFeedback>
                  </Col>
                  <Col>
                    <TouchableWithoutFeedback onPress={() => navigate('EVENTS')}>
                        <View style={styles.tab}>
                          <Image style={styles.iconImg}
                              source={active === 'EVENTS' ? require('../../assets/img/icon_eventos_on.png') : require('../../assets/img/icon_eventos_off.png')}
                          />
                          <Text style={[styles.tabBarText, {color: (active === 'EVENTS') ? '#f99439' : '#acafb0' }]}>Eventos</Text>
                        </View>
                    </TouchableWithoutFeedback>
                  </Col>
                  <Col>
                    <TouchableWithoutFeedback onPress={() => navigate('NEWS')}>
                        <View style={styles.tab}>
                          <Image style={styles.iconImg}
                              source={active === 'NEWS' ? require('../../assets/img/icon_noticias_on.png') : require('../../assets/img/icon_noticias_off.png')}
                          />
                          <Text style={[styles.tabBarText, {color: (active === 'NEWS') ? '#f99439' : '#acafb0' }]}>Notícias</Text>
                        </View>
                    </TouchableWithoutFeedback>
                  </Col>
                </Row>
              </Grid>
            </View>
        );
    }
}

NavTabBar.propTypes = {
    // start react-navigation props
    navigation: PropTypes.object.isRequired
};

function mapStateToProps(state) {
    //const { paymentInfo } = state;
    return {
        paymentInfo: state
    };
}

export default connect(mapStateToProps)(NavTabBar);


const styles = StyleSheet.create({
    container: {
        flexDirection: 'row',
        margin:0,
        justifyContent:'space-around',
        alignItems: 'center',
        height: 75,
        backgroundColor: '#fff'
    },
    navBarRow: {
      marginTop:12
    },
    tab: {
        padding:0,
        alignItems: 'center',

    },
    activeIconStyle: {

    },
    inactiveIconStyle: {

    },

    activeTextStyle: {

    },
    inactiveTextStyle: {

    },
    iconImg:{
      width:40,
      height:30
    },
    tabBarText: {
      fontFamily: 'Montserrat',
      fontSize:9,
      marginTop:4,
      fontWeight:'bold'
    }

});
