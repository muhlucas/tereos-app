import React, { Component } from 'react';
import {
    Image,
    ImageBackground
} from 'react-native';
import FitImage from 'react-native-fit-image';
import {getConnectionInfo} from '../../lib/Api';
export default class ImageBox extends Component {

  constructor(props) {
    super(props);

    this.state = {
      netinfo: null
    }

  }

  componentDidMount() {
    this.getConnectionInfo();
  }

  async getConnectionInfo() {
    const netinfo = await getConnectionInfo();
    this.setState({netinfo});
  }

  render() {
    const {netinfo} = this.state;

    background = (this.props.background) ? this.props.background : false;
    image = (netinfo == null || netinfo.type == 'none') ? require('../../assets/img/pic1.png') : this.props.image;

    return (
         background ?
              <ImageBackground style={this.props.style}
                  source={image}
                  imageStyle={{ borderRadius: 5 }}
              >{this.props.children}
              </ImageBackground>
        :
              <FitImage style={this.props.style}
                resizeMode='cover'
                source={image}
              >{this.props.children}
              </FitImage>
      );
    }
  }
