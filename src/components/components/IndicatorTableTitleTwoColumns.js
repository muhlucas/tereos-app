import React, { Component } from 'react';
import {
    StyleSheet,
    TouchableOpacity,
    View,
    Text
} from 'react-native';
import { Col, Row, Grid } from 'react-native-easy-grid';

export default class IndicatorTable extends Component {

  constructor(props) {
      super(props);

      this.state = {

      }
  }

  render() {
    const { day, month, itemTitle, text, optional } = this.props;

    return (
      <View style={styles.cardBox}>
        <View style={styles.NameItem}>
          <Text style={styles.ton}>ATR</Text>
          <Text style={styles.goalton}>{'(kg/tonelada)'}</Text>
        </View>
        <View style={styles.row}>
          <Grid>
              <Col>
                <Grid>
                  <Col size={75}>
                    <Text style={styles.TextContent1}>Provisório</Text>
                  </Col>
                  <Col size={25}>
                    <Text style={styles.NumberContent1}>137</Text>
                  </Col>
                </Grid>
              </Col>
              <Col>
                <Grid>
                  <Col size={75}>
                    <Text style={styles.TextContent2}>Relativo</Text>
                  </Col>
                  <Col size={25}>
                    <Text style={styles.NumberContent2}>145</Text>
                  </Col>
                </Grid>
              </Col>
          </Grid>
        </View>
      </View>
      );
    }
  }

const styles = StyleSheet.create({
  cardBox: {
    marginTop:18,
    marginBottom:18,
  },
  NameItem:{
    backgroundColor:'white',
    paddingTop:22,
    paddingBottom:22,
    paddingLeft:20,
    paddingRight:20,
    marginLeft:15,
    marginRight:15,
    justifyContent:'center',
    alignItems:'center',
    borderRadius:4,
    borderColor:'#F2F6F7',
    borderWidth:2
  },
  ton:{
    fontWeight:'bold',
    fontSize:20,
    color:'#005499',
    fontFamily: 'Montserrat',
  },
  goalton:{
    color:'#005499',
    fontSize:12,
    fontWeight:'bold',
    fontFamily: 'Montserrat',
  },
  row:{
    flexDirection:'row',
    alignItems:'center',
    justifyContent:'space-between',
    marginTop:-20,
    marginLeft:15,
    marginRight:15,
    flex:1
  },
  TextContent1:{
    backgroundColor:'white',
    fontSize:12,
    marginLeft:1,
    marginTop:20,
    paddingTop:15,
    paddingBottom:15,
    fontWeight:'bold',
    paddingLeft:18,
    color:'#005499',
    fontFamily: 'Montserrat',
  },
  NumberContent1:{
    backgroundColor:'white',
    fontSize:12,
    marginRight:1,
    marginTop:20,
    paddingTop:15,
    paddingBottom:15,
    fontWeight:'bold',
    justifyContent: 'flex-end',
    color:'#005499',
    fontFamily: 'Montserrat',
  },
  TextContent2:{
    backgroundColor:'white',
    fontSize:12,
    paddingTop:15,
    paddingBottom:15,
    paddingLeft:30,
    marginLeft:1,
    marginTop:20,
    fontWeight:'bold',
    color:'#FF892A',
    alignItems:'center',
    justifyContent:'center',
    fontFamily: 'Montserrat',
  },
  NumberContent2:{
    backgroundColor:'white',
    fontSize:12,
    marginRight:1,
    marginTop:20,
    paddingTop:15,
    paddingBottom:15,
    fontWeight:'bold',
    justifyContent: 'flex-end',
    color:'#FF892A',
    fontFamily: 'Montserrat',
  },
});
