import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { StyleSheet, View, TextInput, Text, ScrollView, TouchableWithoutFeedback, TouchableOpacity, Dimensions } from 'react-native';
import { Col, Row, Grid } from 'react-native-easy-grid';
import Image from 'react-native-remote-svg';
import Spinner from 'react-native-loading-spinner-overlay';
import ReportTableThreeColumns from '../../components/ReportTableThreeColumns';
import ReportTableTitleToExpand from '../../components/ReportTableTitleToExpand';
import ReportTableFarm from '../../components/ReportTableFarm';

import User from '../../../lib/User';

var width = Dimensions.get('window').width;
var height = Dimensions.get('window').height;

export default class ReportsByFarm extends Component {

  constructor(props) {
    super(props);

    this.state = {
      tabActive: 'Safra',
      farms: [],
      labels: [],
      values: [],
      isLoading: false,
      emptyData: false
    }

    this.setTabActive = this.setTabActive.bind(this);
  }

  setTabActive(tab) {
    let _this = this;
    this.setState({ tabActive: tab, isLoading: true }, function () {
      if (tab == 'Safra') {
        _this.loadReportSummaryByYear(function () {
          _this.setState({ isLoading: false });
        });
      } else {
        _this.loadReportSummaryByMonth(function () {
          _this.setState({ isLoading: false });
        });
      }
    });
  }

  componentDidMount() {
    this.loadFarms();
    this.loadReportSummaryByYear();
  }

  async loadFarms() {
    let _this = this;
    let id = await User.getId();
    let year = Helper.getCurrentYear();

    this.setState({ isLoading: true });
    Api.getFarms(id, year)
      .then(function (data) {

        if (typeof data == 'undefined') {
          setTimeout(() => {
            alert("Usuário sem permissão");
          }, 100);
          throw 'Usuário sem permissão';
        }

        farms = [];

        if (data) {
          for (idx in data) {
            item = data[idx];
            farms.push(item.Nome);
          }
          emptyData = false;
        } else {
          emptyData = true;
        }


        _this.setState({ farms: farms, isLoading: false, emptyData: emptyData });
        // console.log('data farms', data);
      })
      .catch(function (err) {
        _this.setState({ isLoading: false, emptyData: true });
      });
  }

  async loadReportSummaryByYear(callback) {
    let _this = this;
    let id = await User.getId();
    let year = Helper.getCurrentYear();

    Api.getFarmSummaryReportsByYear(id, year)
      .then(function (data) {

        if (typeof data == 'undefined') {
          setTimeout(() => {
            alert("Usuário sem permissão");
          }, 100);
          throw 'Usuário sem permissão';
        }

        console.log('farm data', data)
        var labels = ['Índice'];


        if (data) {
          values1 = ['Tonelada'];
          values2 = ['ATR'];
          values3 = ['Impurezas'];
          values4 = ['TCH'];
          values5 = ['Área'];

          for (idx in data) {
            item = data[idx];

            harvest = item.Safra.substring(2).replace('/', '');
            labels.push(harvest);

            values1.push(Helper.formatNumber(item.Ton, 0));
            values2.push(item.Atr);
            values3.push(item.Impureza);
            values4.push(item.Tch);
            values5.push(Helper.formatNumber(item.Area, 0));
          }

          var values = [
            { values: values1 },
            { values: values2 },
            { values: values3 },
            { values: values4 },
            { values: values5 },
          ];

          console.log('farm labels', labels);
          console.log('farm values', values);
          emptyData = false;
        } else {
          emptyData = true;
        }

        _this.setState({ values: values, labels: labels, emptyData: emptyData }, function () {
          if (typeof callback != 'undefined') {
            callback();
          }
        });
      })
      .catch(function (err) {
        _this.setState({ isLoading: false, emptyData: true });
      });
  }

  async loadReportSummaryByMonth(callback) {
    let _this = this;
    let id = await User.getId();
    let year = Helper.getCurrentYear();
    let month = Helper.getCurrentMonth();

    Api.getFarmSummaryReportsByMonth(id, year, month)
      .then(function (data) {

        if (typeof data == 'undefined') {
          setTimeout(() => {
            alert("Usuário sem permissão");
          }, 100);
          throw 'Usuário sem permissão';
        }

        if (data) {
          console.log('farm data', data)
          var labels = ['Índice'];

          values1 = ['Tonelada'];
          values2 = ['ATR'];
          values3 = ['Impurezas'];
          values4 = ['TCH'];
          values5 = ['Área'];

          for (idx in data) {
            item = data[idx];

            month = Helper.getMonth(item.Mes)
            labels.push(month);

            values1.push(Helper.formatNumber(item.Ton, 0));
            values2.push(item.Atr);
            values3.push(item.Impureza);
            values4.push(item.Tch);
            values5.push(Helper.formatNumber(item.Area, 0));
          }

          var values = [
            { values: values1 },
            { values: values2 },
            { values: values3 },
            { values: values4 },
            { values: values5 },
          ];

          console.log('farm labels', labels);
          console.log('farm values', values);

          emptyData = false;
        } else {
          emptyData = true;
        }


        _this.setState({ values: values, labels: labels, emptyData: emptyData }, function () {
          if (typeof callback != 'undefined') {
            callback();
          }
        });
      })
      .catch(function (err) {
        _this.setState({ isLoading: false, emptyData: true });
      });
  }

  render() {
    const { navigate, state } = this.props.navigation;

    let { tabActive, labels, values, emptyData } = this.state;

    /* if(tabActive == 'Safra') {
      var labels = ['Índice', '1819', '1718', '1617'];
      var values = [
        {values: ['Tonelada', '50.312', '748.639', '757.659']},
        {values: ['ATR', '145', '139', '149']},
        {values: ['Impurezas', '4.2', '4.2', '4.3']},
        {values: ['TCH', '81', '83', '84']},
        {values: ['Área', '621,14', '9.019,75', '9.019,75']},
      ];
    } else {
      var labels = ['Índice', 'Maio', 'Abril', 'Março'];
      var values = [
        {values: ['Tonelada', '10.824', '11.091', '11.225']},
        {values: ['ATR', '145', '139', '149']},
        {values: ['Impurezas', '4.2', '4.2', '4.3']},
        {values: ['TCH', '81', '83', '84']},
        {values: ['Área', '133,63', '133,63', '133,63']},
      ];
    } */

    if (this.state.farms) {
      var contentFarmsYear = this.state.farms.map(function (item, idx) {
        color = (idx % 2) ? '#005596' : '#3ca9dd';
        return (<ReportTableFarm key={idx} name={item} iconBackgroundColor={color} content={<View />} type={'Safra'} />);
      });

      var contentFarmsMonth = this.state.farms.map(function (item, idx) {
        color = (idx % 2) ? '#005596' : '#3ca9dd';
        return (<ReportTableFarm key={idx} name={item} iconBackgroundColor={color} content={<View />} type={'Mês'} />);
      })

    } else {
      var contentFarmsYear = null;
      var contentFarmsMonth = null;
    }

    console.log('labels farm', labels);
    console.log('values farm', values);

    return (
      <View style={{ backgroundColor: '#F7F7F7' }}>
        <ScrollView style={styles.scrollViewContainer}>
          <Grid>
            <Row>
              <Col>
                {tabActive == 'Safra' ?
                  <TouchableOpacity style={styles.buttonActive} onPress={() => this.setTabActive('Safra')}>
                    <Text style={styles.buttonText}>Safra</Text>
                  </TouchableOpacity>
                  :
                  <TouchableOpacity style={styles.button} onPress={() => this.setTabActive('Safra')}>
                    <Text style={styles.buttonText}>Safra</Text>
                  </TouchableOpacity>
                }
              </Col>
              <Col>
                {tabActive == 'Mês' ?
                  <TouchableOpacity style={styles.buttonActive} onPress={() => this.setTabActive('Mês')}>
                    <Text style={styles.buttonText}>Mês</Text>
                  </TouchableOpacity>
                  :
                  <TouchableOpacity style={styles.button} onPress={() => this.setTabActive('Mês')}>
                    <Text style={styles.buttonText}>Mês</Text>
                  </TouchableOpacity>
                }
              </Col>
            </Row>
          </Grid>

          {this.state.isLoading && !emptyData ?
            <View style={{ flex: 1 }}>
              <Spinner visible={true} textStyle={{ color: '#FFF' }} />
            </View>
            : <View>
              <ReportTableThreeColumns type={tabActive} labels={labels} values={values} />

              {tabActive == 'Safra' ?
                contentFarmsYear
                :
                contentFarmsMonth
              }
            </View>
          }
        </ScrollView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  scrollViewContainer: {

  },
  title: {
    fontSize: 23,
    marginTop: 10,
    marginBottom: 15,
    marginLeft: 15,
    fontWeight: 'bold',
    color: '#005499',
    fontFamily: 'Montserrat',
  },
  button: {
    backgroundColor: '#b2b2b2',
    height: 40,
    margin: 10,
    borderRadius: 5,
    textAlign: 'center',
    alignItems: 'center',
    justifyContent: 'center'
  },
  buttonActive: {
    backgroundColor: '#3ca9dd',
    height: 40,
    margin: 10,
    borderRadius: 5,
    textAlign: 'center',
    alignItems: 'center',
    justifyContent: 'center'
  },
  buttonText: {
    color: '#FFF',
    fontFamily: 'Montserrat',
    fontWeight: 'bold'
  }
});
