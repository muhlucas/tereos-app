import React, { Component } from 'react';
import {
    StyleSheet,
    TouchableOpacity,
    View,
    Text
} from 'react-native';

import ReportTableTitleToExpand from './ReportTableTitleToExpand';
import ReportTableThreeColumns from './ReportTableThreeColumns';
import NoDataButton from './NoDataButton';
import Api from '../../lib/Api';
import User from '../../lib/User';

export default class ReportTableFarm extends Component {

  constructor(props) {
      super(props);

      this.state = {
        values: [],
        labels: [],
        content: null,
        emptyData: false
      }
  }

  async loadFarmData() {
    // alert('loadFarmData');
    let _this = this;

    let id = await User.getId();
    let farm = this.props.name;
    let year = Helper.getCurrentYear();
    let month = Helper.getCurrentMonth();
    let pastMonth = Helper.getPastMonth();

    let type = this.props.type;

    let loadingContent = (<View>
      <Text style={styles.loadingMessage}>Carregando...</Text>
    </View>);

    this.setState({values: [], labels: [], content: loadingContent, emptyData: false}, function() {
      if(type == 'Safra') {
        Api.getFarmReportsByYear(id, farm, year)
        .then(function(data) {

          console.log('response', data);

          if(data) {
            let labels = [''];

            let values1 = ['Tonelada'];
            let values2 = ['ATR'];
            let values3 = ['Impurezas'];
            let values4 = ['TCH'];
            let values5 = ['Área'];

            for(idx in data) {
              item = data[idx];

              harvest = item.Safra.substring(2).replace('/', '');
              labels.push(harvest);

              values1.push(Helper.formatNumber(item.Ton, 0));
              values2.push(item.Atr);
              values3.push(item.Impureza);
              values4.push(item.Tch);
              values5.push(Helper.formatNumber(item.Area, 0));
            }

            values = [
              {values: values1},
              {values: values2},
              {values: values3},
              {values: values4},
              {values: values5},
            ];

            _this.setState({values: values, labels: labels, content: null, emptyData: false});
          } else {
            _this.setState({values: [], labels: [], content: null, emptyData: true});
          }
        }).catch(function() {
          _this.setState({values: [], labels: [], content: null, emptyData: true});
        })
      } else {
        Api.getFarmReportsByMonth(id, farm, year, month)
        .then(function(data) {

          if(data) {
            var labels = [''];

            values1 = ['Tonelada'];
            values2 = ['ATR'];
            values3 = ['Impurezas'];
            values4 = ['TCH'];
            values5 = ['Área'];

            for(idx in data) {
              item = data[idx];

              month = Helper.getMonth(item.Mes)
              labels.push(month);

              values1.push(Helper.formatNumber(item.Ton, 0));
              values2.push(item.Atr);
              values3.push(item.Impureza);
              values4.push(item.Tch);
              values5.push(Helper.formatNumber(item.Area, 0));
            }

            var values = [
              {values: values1},
              {values: values2},
              {values: values3},
              {values: values4},
              {values: values5},
            ];

            _this.setState({values: values, labels: labels, content: null, emptyData: false});
          } else {
            _this.setState({values: [], labels: [], content: null, emptyData: true});
          }
        }).catch(function() {
          _this.setState({values: [], labels: [], content: null, emptyData: true});
        })
      }

    });

  }

  render() {
    let {values, labels, emptyData} = this.state;

    if(emptyData) {
      content = (<NoDataButton onPress={() => {this.loadFarmData();}} />)
    } else if(values.length && labels.length) {
      content = (<ReportTableThreeColumns labels={labels} values={values} />)
    } else {
      content = this.state.content;
    }

    return (<ReportTableTitleToExpand
      name={this.props.name}
      iconBackgroundColor={this.props.iconBackgroundColor}
      content={content}
      onExpand={() => {this.loadFarmData();}}
    />);
  }
}

const styles = StyleSheet.create({
    loadingMessage: {
      fontFamily: 'Montserrat',
      color:'#005499',
      fontSize:11,
      textAlign:'center'
    }
});
