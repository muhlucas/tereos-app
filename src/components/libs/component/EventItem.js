import React, { Component } from 'react';
import {
    StyleSheet,
    TouchableOpacity,
    View,
    Text,
    Alert
} from 'react-native';
import Image from 'react-native-remote-svg';
import {getConnectionInfo} from '../../../lib/Api';

export default class EventItem extends Component {
  static propTypes = {

  }

  static defaultProps = {

  }
  constructor(props) {
      super(props);

      this.state = {
        interested: null,
        confirmed: null,
        netinfo: null
      };

      this.getConnectionInfo = this.getConnectionInfo.bind(this);
  }

  componentDidMount() {
    this.getConnectionInfo();
  }

  async getConnectionInfo() {
    const netinfo = await getConnectionInfo();
    this.setState({netinfo});
    return netinfo;
  }

  async onConfirm() {
    const netinfo = await this.getConnectionInfo();

    let isConnected = (netinfo && (netinfo != null && netinfo.type != 'none'));

    if(!isConnected) {
      alert('Você precisa estar online para executar esta ação.')
      return true;
    }

    this.setState({confirmed: true, interested: false},
      function() {
        this.props.onConfirm()
      }
    );
  }

  async onInterest() {
    const netinfo = await this.getConnectionInfo();

    let isConnected = (netinfo && (netinfo != null && netinfo.type != 'none'));

    if(!isConnected) {
      alert('Você precisa estar online para executar esta ação.')
      return true;
    }

    this.setState({confirmed: false, interested: true},
      function() {
        this.props.onInterest()
      }
    );
  }

  async onUncheck() {
    const netinfo = await this.getConnectionInfo();

    let isConnected = (netinfo && (netinfo != null && netinfo.type != 'none'));

    if(!isConnected) {
      alert('Você precisa estar online para executar esta ação.')
      return true;
    }

    this.setState({confirmed: false, interested: false},
      function() {
        this.props.onUncheck()
      }
    );
  }

  /* eslint-disable */

  renderElement(){

    let _this = this;

    interested = (this.state.interested) == null ? this.props.interested : this.state.interested;
    confirmed = (this.state.confirmed) == null ? this.props.confirmed : this.state.confirmed;

    return (
     <View style={styles.Item2}>
          {interested ?
            <TouchableOpacity
            onPress={() => {_this.onUncheck();}}
            style={styles.buttonFirst1}>
             <Text style={styles.TextFirst1}>Interessado</Text>
           </TouchableOpacity>
           :
           <TouchableOpacity
           onPress={() => { _this.onInterest(); }}
           style={styles.buttonFirst2}>
              <Text style={styles.TextFirst2}>Confirmar interesse</Text>
            </TouchableOpacity>
         }
           {confirmed ?
             <TouchableOpacity
             onPress={() => {_this.onUncheck(); }}
             style={styles.buttonLast2}>
               <Text style={styles.TextLast2}>Presença confirmada</Text>
             </TouchableOpacity>
            :
            <TouchableOpacity
            onPress={() => { _this.onConfirm(); }}
            style={styles.buttonLast1}>
              <Text style={styles.TextLast1}>Confirmar presença</Text>
              </TouchableOpacity>
            }
      </View>
   );
  }


  render() {

    const { day, month, itemTitle, firstText, text, optional } = this.props;
    return (
      <View style={styles.mainItem}>
          <View style={styles.Item}>

                  <View style={styles.imgItem}>
                    <Text style={styles.day}>{day}</Text>
                    <Text style={styles.month}>{month}</Text>
                  </View>

                  <View style={styles.nameItem}>
                    <Text style={styles.greenText}>{itemTitle}</Text>
                    <Text style={styles.titleText}>{firstText}</Text>
                    <Text style={styles.titleText}>{text}</Text>
                    <TouchableOpacity onPress={() => {
                      this.props.onDetail();
                    }}>
                      <Text style={styles.optionalText}>{optional}</Text>
                    </TouchableOpacity>
                  </View>
          </View>
          <View>
              { this.renderElement() }
          </View>
    </View>
      );
    }
  }

  const styles = StyleSheet.create({
    mainItem: {
      marginBottom:20,
    },
    Item:{
      marginLeft:15,
      marginRight:15,
      marginTop:1,
      flexDirection:'row',
      backgroundColor:'white',
      borderWidth: 1,
      borderColor: '#EFF4F6',
      paddingLeft:15,
      paddingTop:15,
      paddingRight:15,
      paddingBottom:25,
      borderRadius:7
    },
    greenText:{
      color:'green',
      fontSize:16,
      marginTop:5,
      color:'#005499',
      fontWeight:'bold',
      fontFamily: 'Montserrat',

    },
    titleText:{
      color:'black',
      fontSize:12,
      marginTop:12,
      fontFamily: 'Montserrat',
    },
    optionalText:{
      color:'#555555',
      fontSize:11,
      marginTop:14,
      marginLeft:2,
      fontWeight:'bold',
      fontFamily: 'Montserrat',
    },

    nameItem:{
      marginLeft:10,
      paddingRight:20,
      width:'58%',
      fontFamily: 'Montserrat',
    }
    ,imgItem:{
      width:130,
      height:130,
      borderRadius:550,
      backgroundColor:'#F7F7F7',
      justifyContent: 'center',
      alignItems: 'center',
      fontFamily: 'Montserrat',
    },
    day:{
      fontSize:56,
      color:'#0c4F77',
      marginTop:-10,
      fontFamily: 'Montserrat',
    },
    month:{
      fontSize:20,
      color:'#00D1F2',
      marginTop:-14,
      fontWeight:'bold',
      fontFamily: 'Montserrat',
    },
    buttonFirst1:{
      backgroundColor:'#FFBA00',
      width:'50%',
      paddingLeft:10,
        paddingTop:28,
        paddingRight:10,
        paddingBottom:28,
        justifyContent: 'center',
        alignItems: 'center',
        fontFamily: 'Montserrat',
    },
    buttonLast1:{
      width:'50%',
      paddingLeft:10,
      paddingTop:28,
      paddingRight:10,
      paddingBottom:28,
      justifyContent: 'center',
      alignItems: 'center',
      fontFamily: 'Montserrat',
    },
    TextFirst1:{
      fontWeight:'bold',
      color:'white',
      fontSize:13,
      fontFamily: 'Montserrat',
    },
    TextLast1:{
      fontWeight:'bold',
      color:'#0c4F77',
      fontSize:13,
      fontFamily: 'Montserrat',
    },
    Item2:{
      marginLeft:15,
      marginRight:15,
      flexDirection:'row',
      backgroundColor:'white',
      borderWidth: 1,
      borderColor: '#EFF4F6',
      borderRadius:7,
      fontFamily: 'Montserrat',
    },
      buttonFirst2:{
        width:'50%',
        paddingLeft:10,
        paddingTop:28,
        paddingRight:10,
        paddingBottom:28,
        justifyContent: 'center',
        alignItems: 'center',
        fontFamily: 'Montserrat',
      },
      buttonLast2:{
        backgroundColor:'#005499',
        width:'50%',
        paddingLeft:10,
        paddingTop:28,
        paddingRight:10,
        paddingBottom:28,
        justifyContent: 'center',
        alignItems: 'center',
        fontFamily: 'Montserrat',
      },
      TextFirst2:{
        fontWeight:'bold',
        color:'#005499',
        fontSize:13,
        fontFamily: 'Montserrat',
      },
      TextLast2:{
        fontWeight:'bold',
        color:'white',
        fontSize:13,
        fontFamily: 'Montserrat',
      },
      buttonFirst3:{
        width:'50%',
        paddingLeft:10,
        paddingTop:28,
        paddingRight:10,
        paddingBottom:28,
        justifyContent: 'center',
        alignItems: 'center',
        fontFamily: 'Montserrat',
      },
      buttonLast3:{
        width:'50%',
        paddingLeft:10,
        paddingTop:28,
        paddingRight:10,
        paddingBottom:28,
        borderWidth: 1,
        borderColor: '#EFF4F6',
        justifyContent: 'center',
        alignItems: 'center',
        fontFamily: 'Montserrat',
      },
      TextFirst3:{
        fontWeight:'bold',
        color:'#005499',
        fontSize:13,
        fontFamily: 'Montserrat',
      },
      TextLast3:{
        fontWeight:'bold',
        color:'#005499',
        fontSize:13,
        fontFamily: 'Montserrat',
      },


  });
