import React, { Component } from 'react';
import {
    StyleSheet,
    TouchableOpacity,
    View,
    ScrollView,
    Text,
    Image,
    Dimensions
} from 'react-native';
import { Col, Row, Grid } from 'react-native-easy-grid';

var width = Dimensions.get('window').width;
var height = Dimensions.get('window').height;

export default class ReportTableCustomInfiniteColumns extends Component {

  constructor(props) {
      super(props);

      this.state = {

      }

      this.renderHeaderRow = this.renderHeaderRow.bind(this);
      this.renderRow = this.renderRow.bind(this);
  }

  renderFirstColumnHeaderRow(label) {
    return (
      <View style={styles.cardRow}>
        <Col>
          <Grid>
            <Col
              style={[styles.column, {borderColor:'#eef3f5', borderBottomWidth:1}]}
            >
              <View>
                <Text style={styles.LabelRow1}>{label}</Text>
              </View>
            </Col>
          </Grid>
        </Col>
      </View>
    );
  }

  renderHeaderRow(values) {
    var valuesContent = values.map(function(item) {
      return (
        <Col
          style={[styles.column, styles.centeredCol, {borderColor:'#eef3f5', borderBottomWidth:1}]}
          >
          <View>
            <Text style={styles.LabelRow2}>{item}</Text>
          </View>
        </Col>
      );
    });

    return (
      <View style={styles.cardRow}>
        <Col>
          <Grid>
            {valuesContent}
          </Grid>
        </Col>
      </View>
    );
  }

  renderFirstColumnRow(label, type) {
    if(type == 'first') {
      bulletImg = require('../../assets/img/bullet_line_first.png');
    } else if(type == 'last') {
      bulletImg = require('../../assets/img/bullet_line_last.png');
    } else {
      bulletImg = require('../../assets/img/bullet_line.png');
    }

    return (<View style={[styles.cardRow, {marginTop:10}]}>
      <Col>
        <Grid>
          <Col
            style={[styles.column, styles.firstColumn, {borderColor:'#eef3f5', borderRightWidth:1}]}
          >
            <Image style={styles.bullet} source={bulletImg} />
            <View>
              <Text style={styles.LabelRow3}>{label}</Text>
            </View>
          </Col>
        </Grid>
      </Col>
    </View>);
  }

  renderRow(values, type) {
    var valuesContent = values.map(function(item) {
      return (
        <Col
          style={[styles.column, styles.centeredCol]}
          >
          <View>
            <Text style={styles.LabelRow4}>{item}</Text>
          </View>
        </Col>
      );
    });

    return (<View style={[styles.cardRow, {marginTop:10}]}>
      <Col>
        <Grid>
          {valuesContent}
        </Grid>
      </Col>
    </View>);
  }

  render() {
    let _this = this;
    const { labels, values } = this.props;

    var headerRowFirstColumn = this.renderFirstColumnHeaderRow(labels[0]);
    var headerRow = this.renderHeaderRow(labels.slice(1));

    var firstColumnRows = [];
    var rows = values.map(function(item, idx) {
      if(idx == 0) {
        firstColumnRow = _this.renderFirstColumnRow(item[0], 'first');
      } else if(idx == (values.length-1)) {
        firstColumnRow = _this.renderFirstColumnRow(item[0], 'last');
      } else {
        firstColumnRow = _this.renderFirstColumnRow(item[0]);
      }

      firstColumnRows.push(firstColumnRow);

      row = _this.renderRow(item.slice(1));
      return row;
    });

    return (
      <View style={styles.container}>
        <View>
          <View style={[styles.cardBox, {width:120}]}>
             {headerRowFirstColumn}
          </View>
          <View style={[styles.cardBox, {marginTop:-10, width:120}]}>
             {firstColumnRows}
          </View>
        </View>

          <ScrollView horizontal style={styles.scrollViewContainer} showsHorizontalScrollIndicator={true} overScrollMode={'always'} showsVerticalScrollIndicator={true}>
            <View >
              {headerRow}
              {rows}
            </View>

            {/*
              {this.renderHeaderRow('Maio', ['Ton.', 'ATR (kg)', 'Impurezas (kg)', 'TCH', 'Área (ha)'])}
              {this.renderRow('Dia 03', ['241', '140', '4.5', '80', '3.01'], 'first')}
              {this.renderRow('Dia 04', ['330', '140', '4.3', '80', '4.13'])}
              {this.renderRow('Dia 05', ['450', '138', '4.4', '80', '5.63'])}
              {this.renderRow('Dia 06', ['220', '138', '3.8', '80', '2.75'])}
              {this.renderRow('Dia 07', ['230', '139', '4.3', '80', '2.88'])}
              {this.renderRow('Dia 10', ['520', '139', '4', '80', '6.50'])}
              {this.renderRow('Dia 11', ['400', '139', '4', '80', '5.00'])}
              {this.renderRow('Dia 12', ['380', '138', '3.9', '80', '4.75'])}
              {this.renderRow('Dia 13', ['270', '140', '3.8', '80', '3.38'])}
              {this.renderRow('Dia 17', ['280', '140', '3.7', '80', '3.50'])}
              {this.renderRow('Dia 18', ['220', '141', '2.2', '80', '2.75'])}
              {this.renderRow('Dia 19', ['230', '139', '2', '80', '2.88'])}
              {this.renderRow('Dia 20', ['870', '141', '5', '80', '10.88'])}
              {this.renderRow('Dia 22', ['697', '141', '2.9', '80', '8.71'])}
              {this.renderRow('Dia 25', ['402', '139', '1.9', '80', '5.03'])}
              {this.renderRow('Dia 26', ['1000', '138', '2.2', '80', '12.50'])}
              {this.renderRow('Dia 27', ['1300', '137', '3.4', '80', '16.25'])}
              {this.renderRow('Dia 28', ['756', '140', '3.8', '80', '9.45'])}
              {this.renderRow('Dia 29', ['1368', '141', '1.9', '80', '17.10'])}
              {this.renderRow('Dia 30', ['660', '139', '4.6', '80', '8.25'], 'last')}
            */}
          </ScrollView>
      </View>
      );
    }
  }

const styles = StyleSheet.create({
  container: {
    backgroundColor:'white',
    flexDirection:'row',
    marginTop:0
  },
  cardBox: {
    marginBottom:10,
    marginLeft:10,
    marginRight:10,

  },
  scrollViewContainer: {
    flexDirection:'row',
    width:width
  },
  cardRow: {
    height:40
  },
  column:{
    width:90,
    height:40
  },
  firstColumn: {
    flexDirection:'row',
  },
  centeredCol: {
    alignItems:'center',
    justifyContent:'center'
  },
  alignLeftCol: {
    alignItems:'flex-start',
    justifyContent:'center',
  },
  bullet: {
    width:20,
    height:50,
    alignItems:'flex-start',
    marginLeft:15
  },
  colorBg: {
    backgroundColor:'#005596',
    borderRadius:2,
  },
  colorBgSmallTh: {
    backgroundColor:'#005596',
    borderRadius:20,
    padding:5,
    width:50,
  },
  TextContent1:{
    fontSize:11,
    marginLeft:1,
    paddingTop:8,
    paddingBottom:8,
    textAlign:'center',
    color:'#000',
    fontFamily: 'Montserrat',
  },
  LabelRow1:{
    fontSize:15,
    paddingTop:10,
    paddingBottom:15,
    fontWeight:'bold',
    textAlign:'center',
    color:'#005596',
    fontFamily: 'Montserrat',
  },
  LabelRow2:{
    fontSize:13,
    fontWeight:'bold',
    textAlign:'center',
    color:'#6d6d6d',
    borderRadius:10,
    fontFamily: 'Montserrat',
  },
  LabelRow3:{
    fontSize:13,
    paddingTop:17,
    fontWeight:'bold',
    textAlign:'center',
    color:'#005596',
    fontFamily: 'Montserrat',
  },
  LabelRow4: {
    fontSize:15,
    marginTop:8,
    textAlign:'center',
    color:'#000',
    fontFamily: 'Montserrat',
  },
  ValueContent1:{
    fontSize:13,
    paddingTop:10,
    paddingBottom:15,
    fontWeight:'bold',
    textAlign:'center',
    color:'#000',
    fontFamily: 'Montserrat',
  },
  NumberContent1:{
    fontSize:13,
    marginRight:1,
    marginTop:20,
    paddingTop:15,
    paddingBottom:15,
    fontWeight:'bold',
    justifyContent: 'flex-end',
    color:'#005499',
    fontFamily: 'Montserrat',
  },
  TextContent2:{
    fontSize:13,
    paddingTop:15,
    paddingBottom:15,
    paddingLeft:30,
    marginLeft:1,
    marginTop:20,
    fontWeight:'bold',
    color:'#FF892A',
    alignItems:'center',
    justifyContent:'center',
    fontFamily: 'Montserrat',
  },
  NumberContent2:{
    fontSize:13,
    marginRight:1,
    marginTop:20,
    paddingTop:15,
    paddingBottom:15,
    fontWeight:'bold',
    justifyContent: 'flex-end',
    color:'#FF892A',
    fontFamily: 'Montserrat',
  },
});
