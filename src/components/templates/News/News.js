import React, { Component } from 'react';
import PropTypes from 'prop-types';
import ImageComponent from '../../libs/component/ImageComponent';
import DetailView from '../../libs/component/DetailView';
import NewsItem from '../../components/NewsItem';
import ImageBox from '../../components/ImageBox';
// import Image from 'react-native-remote-svg';
import { StyleSheet, View, ScrollView, Text, TouchableOpacity, Dimensions, Modal, FlatList, ActivityIndicator, Platform, Image } from 'react-native';
import InfiniteScrollView from 'react-native-infinite-scroll-view';
import NoDataButton from '../../components/NoDataButton';
import HTMLView from 'react-native-htmlview';
import Spinner from 'react-native-loading-spinner-overlay';

import AutoHeightImage from 'react-native-auto-height-image';
import Settings from '../../app/settings';

var screenWidth = Dimensions.get('window').width;
var screenHeight = Dimensions.get('window').height;

function renderNode(node, index, siblings, parent, defaultRenderer) {
  if (node.name == 'img') {
    src = node.attribs.src;

    if(typeof node.attribs.width == 'undefined' || typeof node.attribs.height == 'undefined') {
      newWidth = screenWidth * 0.8;
      newHeight = null;
    } else {
      width = node.attribs.width;
      height = node.attribs.height;
      ratio =  height / width;

      newWidth = screenWidth * 0.8;
      newHeight = parseInt((newWidth*ratio).toFixed(0));
    }

    return (
      src.length ?
      <View style={{alignItems:'center', justifyContent:'center', width:screenWidth-40}}>
        {newHeight ?
        <Image
          style={{width: newWidth, height: newHeight}}
          resizeMode="cover"
          source={{uri: src}}
        />
        :
        <AutoHeightImage
            width={newWidth}
            source={{uri: src}}
        />
        }
      </View>
      : null
    );
  }
}

export default class NoticiasScreen extends Component {

  constructor(props){
      super(props);

      this.state = {
        modalVisible: false,
        isLoading: false,
        news: [],
        pageIndex: 0,
        noMoreItems: false,
        emptyData: false
      }

      this.onClose = this.onClose.bind(this);
      this.onNext = this.onNext.bind(this);
      this.loadNews = this.loadNews.bind(this);
      this.showNewsDetail = this.showNewsDetail.bind(this);
  }

  componentDidMount() {
    this.loadNews();
  }

  loadNews() {
    let _this = this;

    _this.setState({isLoading: true}, function() {

      setTimeout(function() {
        startDate = Helper.get6MonthsAgoDate();
        endDate = Helper.getCurrentDate();

        pageIndex = _this.state.pageIndex;

        Api.getNews(startDate, endDate, pageIndex)
        .then(function(data) {
          news = _this.state.news;

          if(data) {
            for(idx in data) {
              item = data[idx];

              // isValidNews = (item.Id > 0 && item.Cadastro != '0001-01-01T00:00:00');
              // if(isValidNews) {
                obj = {
                  id: item.Id,
                  title: item.Titulo,
                  date: item.DataPublicacao,
                  image: (item.Imagemnoticia) ? Settings.urlApi + item.Imagemnoticia : null,
                }
                news.push(obj);
              // }

            }

            console.log('news data 1', data);
            // console.log('news data 2', news);

            _this.setState({news: news, isLoading: false, pageIndex: pageIndex+1, noMoreItems: (data.length == 0), emptyData: false});
          } else {
            _this.setState({news: [], isLoading: false, pageIndex: pageIndex+1, noMoreItems: true});
          }

        })
        .catch(function(err) {
          _this.setState({isLoading: false, emptyData: true});
        });
      }, 1500);
    });
  }

  onClose() {
      // this.props.navigation.goBack();
      this.setState({modalVisible: false});
  }
  onNext() {
      // this.props.navigation.navigate('DetailViewScreen');
      this.setState({modalVisible: true});
  }

  showNewsDetail(item) {
    let _this = this;

    _this.setState({modalContent: null, modalVisible: true});

    Api.getNewsDetail(item.id)
    .then(function(data) {

      console.log('news data', data);
      console.log('news data conteudo', data.Conteudo);

      body = Helper.clearNewLine(Helper.removeImgFromPTags(data.Conteudo));
      imagem = (data.Imagemnoticia) ? (Settings.urlApi + data.Imagemnoticia) : null;

      console.log('news data conteudo formatted', body);

      content = (
        <View>
          <View>
            {imagem ?
              <View style={styles.modalContainerImg}>
                <ImageBox style={styles.modalImg}
                  image={{uri: imagem}}
                />
              </View>
            :
              <Image style={styles.modalImg}
                  source={require('../../../assets/img/pic1.png')}
              />
            }
            <Text style={styles.textModalTitle}>{item.date}</Text>
            <Text style={styles.textModalContent}>{item.title.toUpperCase()}</Text>
          </View>
          <View>
            <HTMLView
              value={body}
              lineBreak={"\r\n"}
              addLineBreaks={false}
              stylesheet={htmlStyles}
              renderNode={renderNode}
            />
          </View>
        </View>
      );

      _this.setState({modalContent: content});
    }).catch(function(err) {
      _this.setState({modalVisible: false});
    })
  }

  render() {

    let _this = this;

    let { emptyData } = this.state;

    /* var newsContent = this.state.news.map(function(item) {
      return (
        <NewsItem
         title={item.date + ' - Conteúdo Exclusivo'}
         content={item.title}
         image={item.image ? {uri: item.image} : require('../../../assets/img/pic1.png')}
         onNext={() => { _this.showNewsDetail(item); }}
        />
      )
    }) */

    return (
      <View style={styles.container}>

      <Modal
      animationType="slide"
      transparent={false}
      visible={this.state.modalVisible}
      onRequestClose={() => {
        this.setState({modalVisible: false});
      }}
      >
        <View style={styles.modalContainer}>
            <View style={styles.modalTopBar}>
              <TouchableOpacity onPress={() => {
                this.setState({modalVisible: false});
              }}>
                <Image style={styles.img1}
                    source={require('../../../assets/img/close.png')}
                />
              </TouchableOpacity>
            </View>
            <ScrollView style={styles.modalContent}>
              <View>
                <View style={{marginBottom:150}}>
                  {this.state.modalContent}
                </View>
              </View>
            </ScrollView>
        </View>
      </Modal>

      <View style={styles.newsContainer}>
         <View>
           <TouchableOpacity onPress={this.onNext} >
           <Text style={styles.title}>Notícias</Text>
           </TouchableOpacity>
         </View>

         {(_this.state.isLoading && _this.state.pageIndex == 0) ?
           <View style={{ flex: 1 }}>
             <Spinner visible={true} textStyle={{color: '#FFF'}} />
           </View>
         : null }

         {emptyData ?
           <NoDataButton onPress={_this.loadNews} />
         :
           <FlatList
             data={_this.state.news}
             renderItem={({item, separators}) => (
               <NewsItem key={item.title}
                  title={item.date}
                  content={item.title}
                  image={item.image ? {uri: item.image} : require('../../../assets/img/pic1.png')}
                  onNext={() => { _this.showNewsDetail(item); }}
               />
             )}
             onEndReached={() => { _this.loadNews(); }}
             onEndThreshold={100}
             refreshing={_this.state.isLoading}
           />
         }

         {_this.state.isLoading && _this.state.news.length > 0 ?
           <ActivityIndicator size="small" color="#005499" />
         : null }

      </View>
    </View>
    );
  }
}

const htmlStyles = StyleSheet.create({
  img: {

  },
  strong: {
    fontWeight:'bold'
  },
  p: {
    fontSize:12,
    paddingLeft:20,
    paddingRight:20,
    fontFamily: 'Montserrat',
    marginTop:10,
    lineHeight:16
  },
  li: {
    fontSize:12,
    paddingLeft:20,
    paddingRight:20,
    fontFamily: 'Montserrat',
    marginTop:10,
    lineHeight:16
  },
  blockquote: {
    fontSize:12,
    paddingLeft:20,
    paddingRight:20,
    fontFamily: 'Montserrat',
    marginTop:10,
    lineHeight:16,
    fontStyle:'italic'
  },
});

const styles = StyleSheet.create({
    container: {
      backgroundColor: '#F7F7F7',
      color:'#cc00ff'
    },
    newsContainer: {
      ...Platform.select({
        android: {
          marginBottom:120
        },
        ios: {
          marginBottom:120
        }
      }),
    },
    title:{
      fontSize:23,
      marginTop:10,
      marginBottom:15,
      marginLeft:15,
      fontWeight:'bold',
      color:'#005499',
      fontFamily: 'Montserrat',
    },
    img:{
      width:'90%',
      height:270,
      borderRadius: 5,
      marginRight: 20,
      marginLeft: 20,
      position:'absolute',
      flex:1,
    },
    textTitle:{
      fontSize:9,
      color:'white',
      marginTop:180,
      marginLeft:30,
      fontFamily: 'Montserrat',
    },
    textTitle2:{
      fontSize:9,
      color:'white',
      marginTop:170,
      marginLeft:30,
      fontFamily: 'Montserrat',
    },
    textContent:{
      fontSize:15,
      color:'white',
      marginTop:1,
      marginLeft:30,
      fontWeight:'bold',
      width:'70%',
      lineHeight:24,
      marginBottom:20,
      fontFamily: 'Montserrat',
    },
    modalContainer: {
      backgroundColor:'#EAEAEA',
      marginTop: 22,
      height:screenHeight
    },
    modalTopBar: {
      backgroundColor:'white',
      height:50,
      padding:20,
      width:screenWidth
    },
    modalContent: {
      padding:20,
      height:screenHeight*1.5
    },
    modalTitle: {
      fontFamily: 'Montserrat',
      fontSize:14,
      color:'#005499',
      fontWeight:'bold',
      marginBottom:10,
      marginTop:10
    },
    img1:{
      width:84,
      height:21,
      marginBottom:10
    },
    modalContainerImg:{
      width:'100%',
      height:282,
      backgroundColor:'white',
    },
    modalImg:{
      width:'100%',
      height:282,
    },
    textModalTitle:{
      fontSize:9,
      color:'white',
      marginTop:32,
      marginLeft:20,
      marginBottom:10,
      color:'#0c4F77',
    },

    textModalContent:{
      fontSize:18,
      color:'white',
      marginTop:-10,
      marginLeft:20,
      color:'#0c4F77',
      fontWeight:'bold',
      fontFamily: 'Montserrat',
      lineHeight:30
    },
    textProfile:{
      fontSize:12,
      fontFamily: 'Montserrat',
      marginTop:10,
      lineHeight:16
    },
    textBoldProfile: {
      fontSize:12,
      paddingLeft:20,
      paddingRight:20,
      fontFamily: 'Montserrat',
      marginTop:10,
      lineHeight:16,
      fontWeight:'bold'
    }
    ,close:{
      height:53,
      padding:17
    },img1:{
      width:84,
      height:21
    }
});
