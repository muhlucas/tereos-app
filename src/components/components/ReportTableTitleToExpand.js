import React, { Component } from 'react';
import {
    StyleSheet,
    TouchableOpacity,
    TouchableWithoutFeedback,
    View,
    Image,
    Text
} from 'react-native';
import { Col, Row, Grid } from 'react-native-easy-grid';

export default class ReportTableTitleToExpand extends Component {

  constructor(props) {
      super(props);

      this.state = {
        blockExpanded: false
      }

      this.toggleBlock = this.toggleBlock.bind(this);
  }

  toggleBlock() {
    this.setState({blockExpanded: !this.state.blockExpanded});
  }

  render() {

    const { name, iconBackgroundColor, content } = this.props;

    if(!name || !iconBackgroundColor) {
      return null;
    }

    const { blockExpanded } = this.state;

    iconImage = blockExpanded ? require('../../assets/img/icon_arrow_up.png') : require('../../assets/img/icon_arrow_down.png');

    return (
      <View style={styles.container}>
        <View style={styles.containerBox}>
          <View style={styles.buttonBox}>
            <View style={styles.iconAndNameBox}>
              <View
                style={[styles.icon, {backgroundColor:iconBackgroundColor}]}
                >
                <Image
                source={require('../../assets/img/icon_planta_branco.png')}
                style={styles.iconImage}
                />
              </View>
              <TouchableOpacity
                style={styles.name}
                onPress={() => { if(!blockExpanded) { this.props.onExpand(); } this.toggleBlock() }}
                >
                <Text style={styles.textName}>{name}</Text>
              </TouchableOpacity>
            </View>
            <TouchableOpacity
              style={styles.iconArrowDown}
              onPress={() => { if(!blockExpanded) { this.props.onExpand(); } this.toggleBlock() }}
              >
              <Image
              source={iconImage}
              style={styles.iconArrowDownImage}
              />
            </TouchableOpacity>
          </View>
          {blockExpanded ?
            <View style={styles.blockExpanded}>
              {content ?
                content
                :
                <Text style={styles.textBlockExpanded}>Não existem dados disponíveis no momento.</Text>
              }
            </View>
            : null
          }
        </View>
      </View>
      );
    }
  }

const styles = StyleSheet.create({
  container: {
    marginTop:5
  },
  containerBox: {
    backgroundColor:'white',
    marginLeft:10,
    marginRight:10,
    borderRadius:5,
    borderColor:'#EAEAEA',
    borderWidth:1,
  },
  buttonBox: {
    flexDirection:'row',
    justifyContent:'space-between'
  },
  iconAndNameBox: {
    justifyContent:'center',
    flexDirection:'row',
  },
  icon: {
    width:40,
    height:40,
    alignItems:'center',
    justifyContent:'center',
    borderRadius:4
  },
  iconImage: {
    width:30,
    height:30,
  },
  name: {
    alignItems:'flex-start',
    justifyContent:'center'
  },
  textName: {
    fontFamily: 'Montserrat',
    marginLeft:10,
    color:'#005596',
    fontSize:12,
    fontWeight:'bold'
  },
  iconArrowDown: {
    alignItems:'flex-end',
    justifyContent:'center',
  },
  iconArrowDownImage: {
    width:20,
    height:15,
    marginRight:5
  },
  blockExpanded: {
    padding:10
  },
  textBlockExpanded: {
    fontSize:10,
    fontFamily: 'Montserrat',
  }

});
