import { combineReducers } from 'redux';
import { paymentInfo } from '../common/actionTypes';

const rootInitialState = {
};

const rooter = (state = rootInitialState, action) => {
    switch (action) {
      default:
          return state
    }
};

const rootReducer = combineReducers({
    rooter
});

export default rootReducer;
