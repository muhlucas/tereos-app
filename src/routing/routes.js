import { StackNavigator, TabNavigator, SwitchNavigator } from 'react-navigation';

import AppLoading from '../components/app/AppLoading';

import Login from '../components/templates/Login/Login';
import ForgotPassword from '../components/templates/Login/ForgotPassword';
import Home1 from '../components/templates/Home/Home1';
import Home2 from '../components/templates/Home/Home2';
import News from '../components/templates/News/News';
import Indicators from '../components/templates/Indicators/Indicators';
import IndicatorsReport from '../components/templates/Indicators/IndicatorsReport';
import ReportsHome from '../components/templates/Reports/ReportsHome';
import ReportsByFarm from '../components/templates/Reports/ReportsByFarm';
import FinancialReports from '../components/templates/Reports/FinancialReports';
import DailyReport from '../components/templates/Reports/DailyReport';
import Events from '../components/templates/Events/Events';
import Profile from '../components/templates/Profile/Profile';
import EditProfile from '../components/templates/Profile/EditProfile';
import DetailView from '../components/libs/component/DetailView';

import NavTabBar from './tabs/NavTabBar';
import NavBar from './tabs/NavBar';

console.disableYellowBox = true;

export const LoginNavigator = StackNavigator(
    {
        Login: { screen: Login },
        ForgotPassword: { screen: ForgotPassword },
    },
    {
        initialRouteName: 'Login',
        headerMode: 'none',
    }
);


export const HomeNavigator = TabNavigator(
    {
        HomeScreen1: { screen: Home1 },
        HomeScreen2: { screen: Home2 },
    },
    {
        initialRouteName: 'HomeScreen1',
        tabBarComponent: NavBar,
        tabBarPosition: 'top',
        headerTitleStyle: {alignSelf: 'center'},
    }
);

export const NewsDetailNavigator = StackNavigator(
    {
        Main: { screen: DetailView },
    },
    {
      mode: 'modal',
      headerMode: 'none',
    }
);

export const NewsNavigator = TabNavigator(
    {
          NewsScreen: { screen: News },
          DetailViewScreen: { screen: NewsDetailNavigator },
    },
    {
        initialRouteName: 'NewsScreen',
        tabBarComponent: NavBar,
        swipeEnabled: false,
        tabBarPosition: 'top',
    }
);

export const ProfileNavigator = TabNavigator(
    {
          EditProfileScreen: { screen: EditProfile },
          ProfileScreen: { screen: Profile },
    },
    {
        initialRouteName: 'ProfileScreen',
        tabBarComponent: NavBar,
        tabBarPosition: 'top',
        swipeEnabled: false,
    }
);

export const EventNavigator = TabNavigator(
    {
          EventScreen: { screen: Events },
    },
    {
        initialRouteName: 'EventScreen',
        tabBarComponent: NavBar,
        tabBarPosition: 'top',
        swipeEnabled: false,
    }
);

export const IndicatorsNavigator = TabNavigator(
    {
          IndicatorsScreen: { screen: Indicators },
          IndicatorsReportScreen: { screen: IndicatorsReport }
    },
    {
        initialRouteName: 'IndicatorsScreen',
        tabBarComponent: NavBar,
        tabBarPosition: 'top',
        swipeEnabled: false,
    }
);

export const ReportsNavigator = TabNavigator(
    {
          ReportsHomeScreen: { screen: ReportsHome },
          ReportsByFarm: { screen: ReportsByFarm },
          FinancialReports: { screen: FinancialReports },
          DailyReport: { screen: DailyReport }
    },
    {
        initialRouteName: 'ReportsHomeScreen',
        tabBarComponent: NavBar,
        tabBarPosition: 'top',
        swipeEnabled: false,
    }
);

export const MainNavigator = TabNavigator(
    {
        REPORTS: { screen: ReportsNavigator },
        PROFILE: { screen: ProfileNavigator },
        INDICATORS: { screen: IndicatorsNavigator },
        HOME: { screen: HomeNavigator },
        EVENTS: { screen: EventNavigator },
        NEWS: { screen: NewsNavigator },
    },
    {
        initialRouteName: 'HOME',
        tabBarComponent: NavTabBar,
        tabBarPosition: 'bottom',
        swipeEnabled: false,
    }
);


export const FullNavigator = StackNavigator(
    {
        MainScreen: { screen: MainNavigator },
        DetailViewScreen: { screen: DetailView }

    },
    {
        initialRouteName: 'MainScreen',
        headerMode: 'none',
        cardStack: {
          gesturesEnabled: false,
        },
    }
);


export const AppNavigator = SwitchNavigator(
    {
        AppLoading,
        Login: LoginNavigator,
        App: FullNavigator,
        MainScreen: { screen: MainNavigator },
    },
    {
        initialRouteName: 'Login',
        cardStack: {
          gesturesEnabled: false,
        },
    }
);
