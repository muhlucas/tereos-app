import React, { Component } from 'react';
import { AsyncStorage,
  StyleSheet,
  Text,
  TouchableOpacity,
  TouchableWithoutFeedback,
  ScrollView,
  View,
  Platform,
  Dimensions,
  Linking,
  Modal } from 'react-native';
import FontAwesome, { Icons } from 'react-native-fontawesome';
import { Container, Header, Left, Body, Right, Button, Icon, Title } from 'native-base';
import { Col, Row, Grid } from 'react-native-easy-grid';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';

import { setLocRate } from '../../redux/common/actions';
import { getLocRateInUserSelectedCurrency, getCurrencyRates } from '../../utils/requester';
import Image from 'react-native-remote-svg';

var width = Dimensions.get('window').width;
var height = Dimensions.get('window').height;

class NavBar extends Component {
    constructor(props) {
      super(props);

      this.state = {
        modalVisible: false
      }
    }

    openLink(url) {
      console.log('openLink');
      Linking.openURL(url);
    }

    render() {
        const { navigate, state, goBack } = this.props.navigation;

        const { routes, index } = state;

        // console.log('this.props index', index);
        // console.log('this.props routes', routes);

        let title = null;
        let back = false;
        let routeKeyName = null;
        if(routes[index]) {
          // console.log('routes[index]', routes[index]);
          title = (routes[index].params && routes[index].params.title) ? routes[index].params.title : null;
          back = (routes[index].params && routes[index].params.back) ? routes[index].params.back : null
          routeKeyName = (routes[index].routeName) ? routes[index].routeName : null
        }

        let routeName = state.routeName;

        return (
          <View >

            <Modal
            animationType="slide"
            transparent={false}
            visible={this.state.modalVisible}
            onRequestClose={() => {
              this.setState({modalVisible: false});
            }}
            >
              <View style={styles.modalContainer}>
                <View style={styles.modalTopBar}>
                  <TouchableOpacity onPress={() => {
                    this.setState({modalVisible: false});
                  }}>
                    <Image style={styles.img1}
                        source={require('../../assets/img/close.png')}
                    />
                  </TouchableOpacity>
                </View>
                <ScrollView style={styles.modalContent}>
                  <Image style={styles.logo_big}
                      source={require('../../assets/img/logo_tereos.png')}
                  />

                  <Text style={styles.text}>O Grupo Tereos está presente em <Text style={styles.textFeatured}>16 países</Text>, <Text style={styles.textFeatured}>23 mil colaboradores</Text>, mantém <Text style={styles.textFeatured}>49 unidades industriais</Text>, atende <Text style={styles.textFeatured}>5.800 clientes</Text>, comercializa produtos em mais de <Text style={styles.textFeatured}>100 países</Text> e possui faturamento de <Text style={styles.textFeatured}>4.8 bilhões de euros.</Text></Text>
                  <Text style={styles.text}>Somos o <Text style={styles.textFeatured}>terceiro maior produtor mundial de açucar</Text> e o <Text style={styles.textFeatured}>primeiro da produção de álcool e etanol no mercado europeu</Text>.</Text>

                  <Text style={styles.headerTitle}>Nosso DNA Agrícola</Text>

                  <Text style={styles.text}>Onde quer que operemos, mantemos uma estreira relação com o mundo agrícola, levando em conta os interesses de todos os elos da cadeia do setor. Oferecemos uma visão de longo prazo a nossos parceiros agricultores e os apoiamos em agregar valor à sua produção e no desenvolvimento de práticas agrícolas que conciliem performance com sustentabilidade.</Text>

                  <Image style={styles.img}
                      source={require('../../assets/img/img_tereos1.png')}
                  />

                  <Text style={styles.headerTitle}>Nossos números</Text>
                  <Text style={styles.text}><Text style={styles.textFeatured}>18 mil</Text> agricultores parceiros.</Text>
                  <Text style={styles.text}>6 centros de P&D.</Text>
                  <Text style={styles.text}><Text style={styles.textFeatured}>12 mil</Text> agricultores associados cooperados.</Text>

                  <Text style={styles.headerTitle}>Nossa produção no mundo</Text>

                  <Text style={styles.text}><Text style={styles.textFeatured}>1 milhão de hectares</Text> agrícolas</Text>
                  <Text style={styles.text}><Text style={styles.textFeatured}>45 milhões de toneladas</Text> de matérias-primas processadas</Text>

                  <Text style={styles.headerTitle}>Nossa produção no Brasil</Text>
                  <Text style={styles.text}><Text style={styles.textFeatured}>20,1 milhões de toneladas</Text> de cana moída</Text>
                  <Text style={styles.text}><Text style={styles.textFeatured}>1 milhão de MW/h</Text> de bioeletricidade gerada</Text>
                  <Text style={styles.text}><Text style={styles.textFeatured}>1,6 milhões de toneladas</Text> de açucar produzida</Text>

                  <Image style={styles.img}
                      source={require('../../assets/img/img_tereos2.png')}
                  />

                  <Text style={styles.headerTitle}>A Tereos no Brasil</Text>
                  <Text style={styles.text}>O Brasil é o maior produtor e exportador mundial de açucar, e a Tereos Açucar & Energia Brasil tem papel de destaque nesse setor, ocupando a terceira posição na produção de açucar no Brasil. Possuímos escritório na cidade de São Paulo, um centro de distribuição no Rio de Janeiro e 7 unidades industriais distribuídas pelo estado de São Paulo</Text>

                  <Text style={styles.bulletSmall}>Andrade - localizada em Pitangueiras</Text>
                  <Text style={styles.bulletSmall}>Cruz Alta - localizada em Olímpia</Text>
                  <Text style={styles.bulletSmall}>São José - localizada em Colina</Text>
                  <Text style={styles.bulletSmall}>Mandu - localizada em Guaíra</Text>
                  <Text style={styles.bulletSmall}>Severínia - localizada em Severínia</Text>
                  <Text style={styles.bulletSmall}>Tanabi - localizada em Tanabi</Text>
                  <Text style={styles.bulletSmall}>Usina Vertente - localizada em Guaraci</Text>

                  <Text style={styles.headerTitle}>Nossa história</Text>

                  <Text style={styles.bullet}><Text style={styles.bold}>1967</Text> - A Guarani é fundada em Severínia/SP como uma pequena destilaria de álcool e aguardente;</Text>
                  <Text style={styles.bullet}><Text style={styles.bold}>1976</Text> - Unidade de Severínia da Guarani é adquirida pelo Grupo Gafisa;</Text>
                  <Text style={styles.bullet}><Text style={styles.bold}>1987</Text> - É inaugurada em Olímpia a segunda unidade do grupo, a Cruz Alta, pioneira com o que havia de mais moderno no setor de açucar;</Text>
                  <Text style={styles.bullet}><Text style={styles.bold}>1990</Text> - Inauguração da refinaria na Unidade Industrial Cruz Alta, para a produção de açucar amorfo e açucar líquido para a indústria de alimentos e bebidas, e também açucar refinado granulado para exportação;</Text>
                  <Text style={styles.bullet}><Text style={styles.bold}>2000</Text> - O Grupo Tereos começa a investir no setor sucroenergético brasileiro por meio da criação da FBA (Franco Brasileira S/A), uma joint venture. É a primeira empresa estrangeira a entrar no setor no Brasil;</Text>
                  <Text style={styles.bullet}><Text style={styles.bold}>2001</Text> - O Grupo Tereos se estabelece fisicamente no Brasil, assumindo o controle acionário da Guarani;</Text>
                  <Text style={styles.bullet}><Text style={styles.bold}>2003</Text> - A Tereos duplica sua capacidade de moagem na Unidade Cruz Alta, em Olímpia;</Text>
                  <Text style={styles.bullet}><Text style={styles.bold}>2005</Text> - Modernização da Unidade Severínia;</Text>
                  <Text style={styles.bullet}><Text style={styles.bold}>2006</Text> - Aquisição da unidade São José, em Colina/SP, e do projeto da Unidade Tanabi;</Text>
                  <Text style={styles.bullet}><Text style={styles.bold}>2007</Text> - Inauguração da unidade Tanabi e incorporação da Unidade Andrade, em Pitangueiras/SP;</Text>
                  <Text style={styles.bullet}><Text style={styles.bold}>2008</Text> - Tereos e a Tractebel, maior empresa privada de geração de energia do Brasil, participam do primeiro Leilão de Energia de Reserva promovido pela Agência Nacional de Energia Elétrica - ANEEL;</Text>
                  <Text style={styles.bullet}><Text style={styles.bold}>2009</Text> - Em agosto de 2009, dá início ao projeto de construção de uma usina de biomassa da Unidade Andrade;</Text>
                  <Text style={styles.bullet}><Text style={styles.bold}>2010</Text> - Aquisição de 50% da Usina Vertente, em Guaraci, associando-se ao Grupo Humus nessa usina. Criação e listagem da Tereos Internacional na BM&FBovespa. Criou-se uma empresa internacional que reúne os ativos de açucares, amidos e outros produtos provenientes do processamento de cana-de-açucar, cereais, batata e mandioca do Grupo Tereos no mundo. Tereos Internacional forma uma parceira com a Petrobras Biocombustível para investir no crescimento da Tereos. O primeiro fruto dessa parceria foi a aquisição da atual Unidade Mandu (Guaíra/SP);</Text>
                  <Text style={styles.bullet}><Text style={styles.bold}>2011</Text> - Filia-se ao Bonsucro e inicia a produção de etanol anidro na Unidade Tanabi e inaugura a destilaria na Unidade São José;</Text>
                  <Text style={styles.bullet}><Text style={styles.bold}>2012</Text> - Amplia sua capacidade de cogeração com novas caldeiras em suas unidades Cruz Alta e São José. Adquire participação (35%) no Terminal Portuário Teapar;</Text>
                  <Text style={styles.bullet}><Text style={styles.bold}>2013</Text> - Usina Vertente conquista a certificação FSSC 22000 e Unidades Cruz Alta e Severínia recebem a certificação Bonsucro. Amplia a capacidade de cogeração na Usina Vertente e unidade Tanabi;</Text>
                  <Text style={styles.bullet}><Text style={styles.bold}>2014</Text> - Busca competitividade e excelência de seus processos agroindustrial e comercial, lançando o Programa Guarani 2016;</Text>
                  <Text style={styles.bullet}><Text style={styles.bold}>2016</Text> - Em dezembro a Tereos adquire as participações da Petrobras e assume o controle de 100% da Guarani S/A;</Text>

                  <View style={{height:100}}>
                    <Text style={styles.readMoreTitle}>Saiba mais em:</Text>
                    <TouchableOpacity onPress={() => {
                      this.openLink('http://www.tereos.com.br');
                    }}>
                      <Text style={styles.readMoreLink}>www.tereos.com.br</Text>
                    </TouchableOpacity>
                  </View>
                </ScrollView>
              </View>
            </Modal>

            <Header style={styles.headerBar}>

            <Grid style={{alignItems: 'center'}}>

            <Col style={{alignItems: 'flex-start', alignSelf:'flex-start'}}>
              {back ?
                <View style={{alignItems:'flex-start'}}>
                  <Button transparent onPress={() => navigate(back, (routeKeyName == 'EditProfileScreen') ? {refresh: true} : null)}>
                    <Image style={styles.backIcon}
                      source={require('../../assets/img/icon_back.png')}
                    />
                  </Button>
                </View>
              :
                <TouchableOpacity onPress={() => {this.setState({modalVisible: true})}}>
                  <Image style={styles.icon}
                    source={require('../../assets/img/icon_tereos.png')}
                  />
                </TouchableOpacity>
              }
            </Col>

            <Col style={styles.centeredCol}>
              {title ?
                <Text style={styles.textTopBar} numberOfLines={1}>{title}</Text>
              :
                <Image style={styles.logo}
                  source={require('../../assets/img/logo.png')}
                />
              }
            </Col>


            <Col style={{alignItems: 'flex-end', alignSelf:'flex-start'}}>
              {routeName == 'PROFILE' ?
                routeKeyName == 'EditProfileScreen' ?
                  <View /> // we had a "save" button before
                :
                <TouchableOpacity style={styles.editProfile} onPress={() => navigate('EditProfileScreen', {back: 'ProfileScreen'})}>
                  <Text>Editar perfil</Text>
                </TouchableOpacity>
              :
                <TouchableOpacity onPress={() => navigate('PROFILE')}>
                  <Image style={styles.icon}
                  source={require('../../assets/img/icon_user.png')}
                  />
                </TouchableOpacity>
              }
            </Col>

            </Grid>

            </Header>
          </View>
        );
    }
}

NavBar.propTypes = {
    // start react-navigation props
    navigation: PropTypes.object.isRequired
};

export default NavBar;

const styles = StyleSheet.create({
    container: {
        flexDirection: 'row',
        margin:0,
        justifyContent:'space-around',
        alignItems: 'center',
        backgroundColor: '#FFFFFF'
    },
    centeredCol: {
      alignItems:'center',
    },
    headerBar: {
      ...Platform.select({
        android: {
          marginTop:20,
        }
      }),
      backgroundColor:'#FFFFFF'
    },
    tab: {
        padding:0,
        alignItems: 'center',

    },
    activeIconStyle: {

    },
    inactiveIconStyle: {

    },

    activeTextStyle: {

    },
    inactiveTextStyle: {

    },
    logo: {
      height:40,
      width:90,

      alignItems: 'center',
      justifyContent:'center',

    },
    icon: {
      height:30,
      width:30,
      ...Platform.select({
        android: {
          marginTop:15
        },
        ios: {
          marginTop:8
        }
      }),
    },
    backIcon: {
      height:25.5,
      width:15,
      ...Platform.select({
        android: {
          marginTop:15
        },
        ios: {
          marginTop:8
        }
      }),
    },
    editProfile: {
      marginTop:20
    },
    img:{
      borderColor:'#D4D4D4',
      width:190,
      height:52
    },
    textTopBar: {
      fontFamily: 'Montserrat',
      fontSize: 12,
      fontWeight:'bold',
      color:'#30618b',
      width:200,
      alignSelf:'center',
      textAlign:'center'
    },
    modalContainer: {
      backgroundColor:'#EAEAEA',
      marginTop: 22,
      ...Platform.select({
        android: {
          height:height - 50
        },
        ios: {
          height:height
        }
      }),
    },
    modalTopBar: {
      backgroundColor:'white',
      height:50,
      padding:20,
      width:width
    },
    modalContent: {
      height:height,
    },
    modalTitle: {
      fontFamily: 'Montserrat',
      fontSize:14,
      color:'#005499',
      fontWeight:'bold',
      marginBottom:10,
      marginTop:10
    },
    img1:{
      width:84,
      height:21,
      marginBottom:10
    },
    headerTitle: {
      fontFamily: 'Montserrat',
      fontSize:18,
      fontWeight:'bold',
      color:'#226aa2',
      paddingTop:10,
      paddingBottom:10,
      paddingLeft:20,
      paddingRight:20,
    },
    text: {
      fontFamily: 'Montserrat',
      fontSize:13,
      paddingTop:5,
      paddingBottom:5,
      paddingLeft:20,
      paddingRight:20,
    },
    textFeatured: {
      fontFamily: 'Montserrat',
      fontSize:13,
      fontWeight:'bold',
    },
    bulletSmall: {
      fontFamily: 'Montserrat',
      fontSize:13,
      fontWeight:'bold',
      color:'#226aa2',
      paddingTop:5,
      paddingBottom:10,
      paddingLeft:20,
      paddingRight:20,
    },
    bullet: {
      fontFamily: 'Montserrat',
      fontSize:14,
      color:'black',
      paddingTop:9,
      paddingBottom:10,
      paddingLeft:20,
      paddingRight:20,
    },
    bold: {
      fontWeight:'bold'
    },
    img: {
      width:width,
      height:width/2,
      marginTop:10,
      marginBottom:10
    },
    logo_big: {
      width:width*0.8,
      height:(width*0.8/3.63),
      marginTop:10,
      marginBottom:20,
      alignSelf:'center',
      textAlign:'center',
    },
    readMoreTitle: {
      fontFamily: 'Montserrat',
      fontSize:18,
      color:'#226aa2',
      alignSelf:'center',
      marginTop:10
    },
    readMoreLink: {
      fontFamily: 'Montserrat',
      fontSize:26,
      fontWeight:'bold',
      color:'#226aa2',
      alignSelf:'center'
    }
});
