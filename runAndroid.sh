#!/bin/bash

kill $(lsof -n -i4TCP:8081 | grep -v 'PID' | awk '{print $2}')
/Users/gocampos/Library/Android/sdk/tools/emulator -avd Galaxy_Nexus_API_23 -netdelay none -netspeed full > /dev/null 2>&1 & 
sleep 10
react-native run-android
