import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { StyleSheet, View, TextInput, Text, ScrollView, AsyncStorage} from 'react-native';
import Image from 'react-native-remote-svg';
import Spinner from 'react-native-loading-spinner-overlay';
import HomeTab from '../../../routing/tabs/HomeTab';
import Home2Component from '../../libs/component/Home2Component';
import Helper from '../../../lib/Helper';

export default class Home2 extends Component {

  constructor(props) {
    super(props);

    this.state = {
      values: {
        grossValue: 0,
        CTT: 0,
        association: 0,
        otherServices: 0,
        retention: 0,
        netValue: 0
      },
      values2: {
        grossValue: 0,
        CTT: 0,
        association: 0,
        otherServices: 0,
        retention: 0,
        netValue: 0
      },
      monthlyAccumulatedConsecana: 0,
      yearlyAccumulatedConsecana: 0,
      netAccumulatedMonth: 0,
      netAccumulated: 0,
      atrRelative: 0,
      updatedAt: null,
      monthUpdatedAt: null,
      isLoading: false
    };

    this.loadAllData = this.loadAllData.bind(this);
  }

  componentWillReceiveProps(props) {
    this.loadAllData();
  }

  componentDidMount() {
    this.loadAllData();
  }

  async loadAllData() {
    let _this = this;

    var id = await User.getId();
    var year = Helper.getCurrentYear();
    var month = Helper.getPastMonth();

    this.setState({isLoading: true});
    Api.getMonthlySummaryData(id, year, month)
    .then(function(data) {
      // console.log('data 2 - 1', data);

      if (typeof data == 'undefined') {
        setTimeout(() => {
          alert("Usuário sem permissão");
        }, 100);
        throw 'Usuário sem permissão';
      }

      if(data && data[0]) {
        obj = {
          values: {
            grossValue: data[0].VB,
            CTT: data[0].CTT,
            association: data[0].Associacao,
            otherServices: data[0].Outros_Servicos,
            retention: data[0].Retencao,
            netValue: data[0].VL_Acumulado
          },
          monthlyAccumulatedConsecana: data[0].Consecana_Acumulado,
          netAccumulatedMonth: data[0].VL_Acumulado,
          updatedAt: (data[0].Dtatualizacao) ? Helper.formatDateShort(data[0].Dtatualizacao) : null,
          monthUpdatedAt: (data[0].Dtatualizacao) ? Helper.getMonthNameByDDMMYYY(data[0].Dtatualizacao) : null,
          // atrRelative: data[0].ATR_Relativo_Acumulado
        };
        _this.setState(obj);

      }else{
        alert("Usuário sem permissão");
      }

      Api.getAnualSummaryData(id, year)
      .then(function(data) {
        console.log('data 2', data);

        if(data && data[0]) {
          harvest = data[0].Safra.substring(2).replace('/', '');

          _this.setState({
            values2: {
              grossValue: data[0].VB,
              CTT: data[0].CTT,
              association: data[0].Associacao,
              otherServices: data[0].Outros_Servicos,
              retention: data[0].Retencao,
              netValue: data[0].VL_Acumulado
            },
            yearlyAccumulatedConsecana: data[0].Consecana_Acumulado,
            atrRelative: data[0].ATR_Relativo_Acumulado,
            netAccumulated: data[0].VL_Acumulado,
            harvest: harvest
          }, function() {
            AsyncStorage.multiGet(['@dashboardUpdateAt', '@dashboardMonthUpdateAt'], function(err, data) {
              for(idx in data) {
                key = data[idx][0];
                value = data[idx][1];
                if(key == '@dashboardUpdateAt') {
                  updatedAt = value
                } else if(key == '@dashboardMonthUpdateAt') {
                  monthUpdatedAt = value;
                }
              }

              _this.setState({updatedAt, monthUpdatedAt, isLoading: false});
            }).catch(function(err) {
              _this.setState({isLoading: false});
            });
          });
        } else {
          throw "No data";
        }
      })
      .catch(function(err) {
        _this.setState({isLoading: false});
      });
    })
    .catch(function(err) {
      _this.setState({isLoading: false});
    });
  }


  render() {
    var {updatedAt, monthUpdatedAt, netAccumulatedMonth, netAccumulated, harvest} = this.state;

    values1 = [
      {label: 'Valor Bruto', value: Helper.formatNumber(this.state.values.grossValue)},
      {label: 'CTT', value: Helper.formatNumber(this.state.values.CTT), negative: true},
      {label: 'Associação', value: Helper.formatNumber(this.state.values.association), negative: true},
      {label: 'Outros Serviços', value: Helper.formatNumber(this.state.values.otherServices), negative: true},
      // {label: 'Retenção', value: '-'},
      {label: '-', value: '-'},
      {label: 'Valor Líquido', value: Helper.formatNumber(this.state.values.netValue)},
    ];

    values2 = [
      {label: 'Valor Bruto', value: Helper.formatNumber(this.state.values2.grossValue)},
      {label: 'CTT', value: Helper.formatNumber(this.state.values2.CTT), negative: true},
      {label: 'Associação', value: Helper.formatNumber(this.state.values2.association), negative: true},
      {label: 'Outros Serviços', value: Helper.formatNumber(this.state.values2.otherServices), negative: true},
      // {label: 'Retenção', value: '-'},
      {label: '-', value: '-'},
      {label: 'Valor Líquido', value: Helper.formatNumber(this.state.values2.netValue)},
    ];

    return (
      <View style={{backgroundColor:'#F7F7F7'}}>
        {this.state.isLoading ?
          <Spinner visible={true} textStyle={{color: '#FFF'}} />
        : null }
        <HomeTab navigation={this.props.navigation}/>
        <ScrollView>

        <View style={styles.Container}>
            <View style={styles.Item}>
              <View style={styles.TitleItem}>
                <Text style={styles.titleBox}>Consecana Acumulado</Text>
                <Text style={styles.titleBox}>(R$/kg ATR) </Text>
                <Text style={styles.goalTitle}>{Helper.formatNumber(this.state.yearlyAccumulatedConsecana, 4)}</Text>
              </View>
            </View>
        </View>

        {updatedAt && monthUpdatedAt ?
          <View style={styles.container}>
            <Text style={styles.title}>{this.state.monthUpdatedAt}</Text>
            <Text style={styles.name}>Atualizado até</Text>
            <Text style={styles.rate}>{this.state.updatedAt}</Text>
          </View>
        :
          <View style={styles.container}>
            <Text style={styles.title}>{this.state.isLoading ? ' ' : Helper.getMonth(Helper.getCurrentMonth())}</Text>
            <Text style={styles.name}>Atualizado até</Text>
            <Text style={styles.rate}>{this.state.isLoading ? '-' : Helper.getCurrentDateShort()}</Text>
          </View>
        }

        <Home2Component
          number={Helper.formatNumber(this.state.netAccumulatedMonth)}
          mainNumber={Helper.formatNumber(this.state.monthlyAccumulatedConsecana)}
          title={'Valor líquido Mês'}
          values={values1}
        />

        <View style={styles.middle}>
          <Text style={styles.middleTitle}>
            Safra
          </Text>
        </View>

        <Home2Component
          number={Helper.formatNumber(this.state.netAccumulated)}

          title={'Valor líquido Safra'}
          note={'*Sem Retenção'}
          values={values2}
        />

        {/* <View style={styles.Container}>
            <View style={styles.Item}>
              <View style={styles.TitleItem}>
                <Text style={styles.titleBox}>Consecana Mensal acum.</Text>
                <Text style={styles.titleBox}>(R$/kg ATR) </Text>
                <Text style={styles.goalTitle}>{this.state.yearlyAccumulatedConsecana}</Text>
              </View>
            </View>
        </View> */}



        <View style={styles.account}>
          {/*
          <Image style={styles.accountimg}
              source={require('../../../assets/img/account.png')}
          />
          <Text style={styles.floatingIcon}>Gestor Luís</Text>
          */}
        </View>
        </ScrollView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
    container: {
      color:'#cc00ff',
      justifyContent:'center',
      alignItems:'center'
    },
    title:{
      fontSize:26,
      marginTop:0,
      fontWeight:'bold',
      color:'#005499',
      fontFamily: 'Montserrat',
    },
    name:{
      fontSize:12,
      color:'black',
      fontFamily: 'Montserrat',
    },
    rate:{
      fontSize:15,
      fontWeight:'bold',
      color:'black',
      fontFamily: 'Montserrat'
    },
    middle:{

      alignItems:'center',
      justifyContent:'center',
      fontFamily: 'Montserrat',
    },
    middleTitle:{
      color:'#005499',
      fontSize:32,
      fontWeight:'bold',
      paddingTop:20,
      paddingBottom:8,
      alignItems:'center',
      justifyContent:'center',
      fontFamily: 'Montserrat'
    },
    accountimg:{
      width:45,
      height:45,
      justifyContent: 'flex-end',
      alignItems:'flex-end',
      marginRight:30,
      fontFamily: 'Montserrat',
    },
    account:{
      justifyContent: 'flex-end',
      alignItems:'flex-end',
      marginTop:10,
      height:60
    },
    floatingIcon:{
      justifyContent: 'flex-end',
      alignItems:'flex-end',
      color:'#005499',
      fontSize:14,
      marginRight:20,
      fontFamily: 'Montserrat',
    },
    Item:{
        marginBottom:20,
        fontFamily: 'Montserrat',
    },
    TitleItem:{
      backgroundColor:'#00D5F7',
      width:'93%',
      marginLeft:15,
      marginLeft:15,
      justifyContent:'center',
      alignItems:'center',
      marginTop:20,
      borderRadius:4,
      paddingTop:26,
      paddingBottom:36,
      paddingLeft:20,
      paddingRight:20,
      fontFamily: 'Montserrat',
    },
    titleBox:{
      color:'white',
      fontSize:13,
      fontWeight:'bold',
      fontFamily: 'Montserrat',
    },
    goalTitle:{
      color:'#0068A9',
      fontSize:48,
      fontWeight:'bold',
      fontFamily: 'Montserrat',
    },
    NameItem:{
      backgroundColor:'white',
      width:'93%',
      paddingTop:29,
      paddingBottom:24,
      paddingLeft:20,
      paddingRight:20,
      marginLeft:15,
      marginRight:15,
      fontWeight:'bold',
      justifyContent:'center',
      alignItems:'center',
      marginTop:14,
      borderRadius:4,
      borderColor:'#F2F6F7',
      borderWidth:2,
      fontFamily: 'Montserrat',
    },
    note: {
      fontSize:10,
      alignItems:'flex-start',
      justifyContent:'flex-start',
      textAlign:'left',
      color:'black',
      fontFamily: 'Montserrat',
      width:'93%',
    },
    goalName:{
      color:'#00D5F7',
      fontSize:50,
      fontWeight:'bold',
      marginTop:-8,
      fontFamily: 'Montserrat',
    },
});
