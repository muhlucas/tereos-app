import React, { Component } from 'react';
import {
    StyleSheet,
    TouchableOpacity,
    View,
    ScrollView,
    Text,
    Dimensions
} from 'react-native';
import { Col, Row, Grid } from 'react-native-easy-grid';

var width = Dimensions.get('window').width;
var height = Dimensions.get('window').height;

export default class ReportTableCustomInfiniteColumns extends Component {

  constructor(props) {
      super(props);

      this.state = {

      }
  }

  render() {
    const { labels, values, type, headerBgAllFilled, firstColDoubleSize } = this.props;

    if(labels && labels.length > 0) {
      var isHeaderBgAllFilled = (typeof headerBgAllFilled != 'undefined' && headerBgAllFilled == true);

      var labelsContent = labels.map(function(item, idx) {

        var isFirst = (idx == 0);

        if((isFirst || isHeaderBgAllFilled) && item) {
          return (
            <Col style={styles.column} size={isFirst && firstColDoubleSize ? 2 : 1}>
              <View style={[styles.colorBg, styles.headerColumn]}>
                <Text style={styles.LabelRow1}>{item}</Text>
              </View>
            </Col>
          );
        }

        return (
          <Col style={[styles.column, styles.centeredCol]}>
            {item ?
            <View style={styles.colorBgSmallTh}>
              <Text style={styles.LabelRow2}>{item}</Text>
            </View>
            :
            <View style={styles.smallTh} />
            }
          </Col>
        );
      });
    } else {
      var labelsContent = null;
    }


    if(!values || values.length == 0) {
      return (<View />);
    }

    var valuesContent = values.map(function(item, idx) {

      var valuesContent2 = item.values.map(function(item2, idx2) {

        var isFirst = (idx2 == 0);

        if(isFirst) {

          if(item.colTextStyle) {
            return (
              <Col size={firstColDoubleSize ? 2 : 1} style={[styles.column, styles.alignLeftCol, { borderRightWidth:1, borderColor:'#EAEAEA'}]}>
                <Text style={[styles.LabelRow3, item.colTextStyle]}>{item2}</Text>
              </Col>
            );
          }

          return (
            <Col size={firstColDoubleSize ? 2 : 1} style={[styles.column, styles.alignLeftCol, {borderRightWidth:1, borderColor:'#EAEAEA'}]}>
              <Text style={styles.LabelRow3}>{item2}</Text>
            </Col>
          );
        }

        if(item.colTextStyle) {
          return (<Col style={[styles.column, styles.centeredCol]}>
            <Text style={[styles.TextContent1, item.colTextStyle]}>{item2}</Text>
          </Col>);
        }

        return (<Col style={[styles.column, styles.centeredCol]}>
          <Text style={styles.TextContent1}>{item2}</Text>
        </Col>);

      });

      if(item.style) {
        return (
          <Row style={item.style}>
            <Col>
              <Grid>
                {valuesContent2}
              </Grid>
            </Col>
          </Row>
        )
      }

      return (
        <Row>
          <Col>
            <Grid>
              {valuesContent2}
            </Grid>
          </Col>
        </Row>
      );
    });

    var containerStyle = (labelsContent) ? styles.container : [styles.container, {marginTop: 0} ];

    return (
      <ScrollView style={styles.scrollViewContainer} horizontal showsHorizontalScrollIndicator={true} overScrollMode={'always'} showsVerticalScrollIndicator={true}>

          <View>
            <View style={containerStyle}>
              {labelsContent ?
               <View style={styles.cardBox}>
                  <Grid>
                    <Row>
                      <Col>
                        <Grid>
                          {labelsContent}
                        </Grid>
                      </Col>
                    </Row>
                  </Grid>
              </View>
              : null }
<ScrollView>
              <View style={styles.cardBox}>
                <Grid>
                  {valuesContent}
                </Grid>
              </View>
              </ScrollView>
            </View>
          </View>

      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    marginTop:20,
  },
  cardBox: {
    marginBottom:10,
    marginLeft:10,
    marginRight:10,
    borderRadius:5,
    borderColor:'#EAEAEA',
    borderWidth:1,
    backgroundColor:'white',
  },
  goalton:{
    color:'#005499',
    fontSize:12,
    fontWeight:'bold',
    fontFamily: 'Montserrat',
  },
  column:{
    width:120,
    height:40
  },
  centeredCol: {
    alignItems:'center',
    justifyContent:'center',
    borderRightWidth:1,
    borderColor:'#EAEAEA'
  },
  alignLeftCol: {
    alignItems:'flex-start',
    justifyContent:'center',
  },
  headerColumn: {
    marginRight:1
  },
  colorBg: {
    backgroundColor:'#005596',
    borderRadius:2
  },
  colorBgSmallTh: {
    backgroundColor:'#005596',
    borderRadius:20,
    padding:5,
    width:50,
  },
  smallTh: {
    borderRadius:20,
    padding:20,
    width:50,
  },
  TextContent1:{
    fontSize:10,
    marginLeft:1,
    paddingTop:8,
    paddingBottom:8,
    textAlign:'center',
    color:'#000',
    fontFamily: 'Montserrat',
  },
  LabelRow1:{
    fontSize:12,
    paddingTop:15,
    paddingBottom:15,
    fontWeight:'bold',
    textAlign:'center',
    color:'#FFF',
    fontFamily: 'Montserrat',
  },
  LabelRow2:{
    fontSize:10,
    fontWeight:'bold',
    textAlign:'center',
    color:'#FFF',
    borderRadius:10,
    backgroundColor:'#005596',
    fontFamily: 'Montserrat',
  },
  LabelRow3: {
    fontSize:12,
    fontWeight:'bold',
    textAlign:'center',
    color:'#005596',
    fontFamily: 'Montserrat',
    marginLeft:10,
  },
  ValueContent1:{
    fontSize:12,
    paddingTop:10,
    paddingBottom:15,
    fontWeight:'bold',
    textAlign:'center',
    color:'#000',
    fontFamily: 'Montserrat',
  },
  NumberContent1:{
    fontSize:12,
    marginRight:1,
    marginTop:20,
    paddingTop:15,
    paddingBottom:15,
    fontWeight:'bold',
    justifyContent: 'flex-end',
    color:'#005499',
    fontFamily: 'Montserrat',
  },
  TextContent2:{
    fontSize:12,
    paddingTop:15,
    paddingBottom:15,
    paddingLeft:30,
    marginLeft:1,
    marginTop:20,
    fontWeight:'bold',
    color:'#FF892A',
    alignItems:'center',
    justifyContent:'center',
    fontFamily: 'Montserrat',
  },
  NumberContent2:{
    fontSize:12,
    marginRight:1,
    marginTop:20,
    paddingTop:15,
    paddingBottom:15,
    fontWeight:'bold',
    justifyContent: 'flex-end',
    color:'#FF892A',
    fontFamily: 'Montserrat',
  },
});
