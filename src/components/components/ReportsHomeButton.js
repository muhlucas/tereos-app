import React, { Component } from 'react';
import {
    StyleSheet,
    TouchableOpacity,
    View,
    Text,
    Image,
    Dimensions
} from 'react-native';

var width = Dimensions.get('window').width;
var height = Dimensions.get('window').height;

export default class ReportsHomeButton extends Component {

  constructor(props) {
      super(props);

      this.state = {

      }

      this.onClick = this.onClick.bind(this);
  }

  onClick() {
    if(typeof this.props.onClick != 'undefined') {
      this.props.onClick();
    }
  }

  render() {
    const { title, image, color } = this.props;

    console.log('image', image );

    return (
      <TouchableOpacity style={styles.cardBox} onPress={this.onClick}>
        <View style={styles.NameItem}>
          <View style={[styles.icon, {backgroundColor:color}]}>
            <Image style={styles.img}
              source={image}
            />
          </View>
          <Text style={styles.cardTitle}>{title}</Text>
        </View>
      </TouchableOpacity>
      );
    }
  }

const styles = StyleSheet.create({
  cardBox: {
    marginTop:0,
    marginBottom:20,
    marginLeft:15,
    marginRight:15,
    borderRadius:5,
    borderColor:'#EAEAEA',
    borderWidth:1,
    backgroundColor:'white',
  },
  NameItem:{
    paddingTop:22,
    paddingBottom:22,
    paddingLeft:20,
    paddingRight:20,
    alignItems:'center'
  },
  cardTitle:{
    fontWeight:'bold',
    fontSize:14,
    color:'#005499',
    fontFamily: 'Montserrat',
    textAlign:'center',
    alignItems:'flex-start',
    justifyContent:'flex-start',
    width: width*0.75
  },
  goalton:{
    color:'#005499',
    fontSize:12,
    fontWeight:'bold',
    fontFamily: 'Montserrat',
  },
  row:{
    flexDirection:'row',
    alignItems:'center',
    justifyContent:'space-between',
    marginTop:-20,
    flex:1
  },
  TextContent1:{
    fontSize:10,
    marginLeft:1,
    marginTop:20,
    paddingTop:15,
    paddingBottom:15,
    fontWeight:'bold',
    textAlign:'center',
    color:'#005499',
    fontFamily: 'Montserrat',
  },
  LabelRow1:{
    fontSize:10,
    marginTop:20,
    paddingTop:15,
    paddingBottom:15,
    fontWeight:'bold',
    textAlign:'center',
    color:'#005499',
    fontFamily: 'Montserrat',
  },
  ValueContent1:{
    fontSize:12,
    paddingTop:10,
    paddingBottom:15,
    fontWeight:'bold',
    textAlign:'center',
    color:'#000',
    fontFamily: 'Montserrat',
  },
  NumberContent1:{
    fontSize:12,
    marginRight:1,
    marginTop:20,
    paddingTop:15,
    paddingBottom:15,
    fontWeight:'bold',
    justifyContent: 'flex-end',
    color:'#005499',
    fontFamily: 'Montserrat',
  },
  TextContent2:{
    fontSize:12,
    paddingTop:15,
    paddingBottom:15,
    paddingLeft:30,
    marginLeft:1,
    marginTop:20,
    fontWeight:'bold',
    color:'#FF892A',
    alignItems:'center',
    justifyContent:'center',
    fontFamily: 'Montserrat',
  },
  NumberContent2:{
    fontSize:12,
    marginRight:1,
    marginTop:20,
    paddingTop:15,
    paddingBottom:15,
    fontWeight:'bold',
    justifyContent: 'flex-end',
    color:'#FF892A',
    fontFamily: 'Montserrat',
  },
  img: {
    width:70,
    height:50,
  },
  icon: {
    borderRadius: 80,
    width:70,
    height:70,
    alignItems:'center',
    justifyContent:'center',
    marginBottom:5
  },
});
