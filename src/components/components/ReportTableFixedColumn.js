import React, { Component } from 'react';
import {
    StyleSheet,
    TouchableOpacity,
    View,
    ScrollView,
    Text,
    Dimensions
} from 'react-native';
import { Col, Row, Grid } from 'react-native-easy-grid';

var width = Dimensions.get('window').width;
var height = Dimensions.get('window').height;

export default class ReportTableFixedColumn extends Component {

  constructor(props) {
    super(props);

    this.state = {

    }
  }

  render() {
    const { labels, values, type, headerBgAllFilled, firstColDoubleSize } = this.props;

    if(labels && labels.length > 0) {
      var isHeaderBgAllFilled = (typeof headerBgAllFilled != 'undefined' && headerBgAllFilled == true);

      var labelsContent = labels.map(function(item, idx) {

        var isFirst = (idx == 0);

        if((isHeaderBgAllFilled) && item) {
          return (
            <Col style={styles.column} size={isFirst && firstColDoubleSize ? 2 : 1}>
              <View style={[styles.colorBg, styles.headerColumn]}>
                <Text style={styles.LabelRow1}>{item}</Text>
              </View>
            </Col>
          );
        }

        return (
          <Col style={[styles.column, styles.centeredCol]}>
            {item ?
            <View style={styles.colorBgSmallTh}>
              <Text style={styles.LabelRow2}>{item}</Text>
            </View>
            :
            <View style={styles.smallTh} />
            }
          </Col>
        );
      });
    } else {
      var labelsContent = null;
    }

    if(!values || values.length == 0) {
      return (<View />);
    }

    var firstColumnContent = values.map(function(item, idx) {

      var valuesContent2 = item.values.map(function(item2, idx2) {
        var isFirst = (idx2 == 0);

        if(isFirst) {

          if(item.header) {
            return (
              <Col style={styles.column} size={1}>
                <View style={[styles.colorBg, styles.headerColumn]}>
                  <Text style={styles.LabelRow1}>{item2}</Text>
                </View>
              </Col>
            );
          }

          if(item.colTextStyle) {
            return (
              <Col size={firstColDoubleSize ? 2 : 1} style={[styles.column, styles.alignLeftCol, { borderRightWidth:1, borderColor:'#EAEAEA'}]}>
                <Text style={[styles.LabelRow3, item.colTextStyle]}>{item2}</Text>
              </Col>
            );
          }

          return (
            <Col size={firstColDoubleSize ? 2 : 1} style={[styles.column, styles.alignLeftCol, {borderRightWidth:1, borderColor:'#EAEAEA'}]}>
              <Text style={styles.LabelRow3}>{item2}</Text>
            </Col>
          );
        }
      });

      var isFirst = (idx == 0);

      if(isFirst && !labels) {
        return null;
      }

      if(item.header) {
        return (
          <Row>
            <Col>
              <Grid>
                {valuesContent2}
              </Grid>
            </Col>
          </Row>
        );
      }

      return (
        <Row>
          <Col>
            <Grid>
              {valuesContent2}
            </Grid>
          </Col>
        </Row>
      );
    });

    var valuesContent = values.map(function(item, idx) {

      var valuesContent2 = item.values.map(function(item2, idx2) {

        // console.log('item2', item2);

        var isFirst = (idx2 == 0);

        if(isFirst) {
          return null;
        }

        if(typeof item2 == 'undefined' || item2 == '') {
          item2 = 0;
        }


        // console.log('item2', item2);

        if(item.colTextStyle) {
          return (<Col style={[styles.column, styles.centeredCol]}>
            <Text style={[styles.TextContent1, item.colTextStyle]}>{item2}</Text>
          </Col>);
        }

        return (<Col style={[styles.column, styles.centeredCol]}>
          <Text style={styles.TextContent1}>{item2}</Text>
        </Col>);
      });

      if(valuesContent2.length <= 1) { // there is no content
        return null;
      }

      // console.log('valuesContent2', valuesContent2);

      if(item.style) {
        return (
          <Row style={item.style}>
            <Col>
              <Grid>
                {valuesContent2}
              </Grid>
            </Col>
          </Row>
        )
      }

      return (
        <Row>
          <Col>
            <Grid>
              {valuesContent2}
            </Grid>
          </Col>
        </Row>
      );

      return (<View></View>);
    });

    var containerStyle = (labelsContent) ? styles.container : [styles.container, {marginTop: 0} ];

    return (
      <View style={{flexDirection:'row', marginTop:0}}>
        <View style={containerStyle}>
          {labels ?
          <View style={[styles.cardBox, {width:120}]}>
             <Grid>
               <Row>
                 <Col>
                   <Grid>
                    {firstColumnContent[0]}
                   </Grid>
                 </Col>
                </Row>
              </Grid>
          </View>
          : null}
          <View>
            <View style={[styles.cardBox, {width:120}]}>
               <Grid>
                 <Row>
                   <Col>
                     <Grid>
                      {(firstColumnContent.length < 10) ?
                        firstColumnContent.slice(1, 3)
                        :
                        firstColumnContent.slice(1, 5)
                      }
                     </Grid>
                   </Col>
                  </Row>
                </Grid>
            </View>
            <View style={[styles.cardBox, {width:120}]}>
               <Grid>
                 <Row>
                   <Col>
                     <Grid>
                      {(firstColumnContent.length < 10) ?
                        firstColumnContent.slice(3)
                        :
                        firstColumnContent.slice(5)
                      }
                     </Grid>
                   </Col>
                  </Row>
                </Grid>
            </View>
          </View>
        </View>

      <ScrollView style={styles.scrollViewContainer} horizontal showsHorizontalScrollIndicator={true} overScrollMode={'always'} showsVerticalScrollIndicator={true}>
          <View>
            <View style={containerStyle}>
              {labelsContent ?
               <View style={styles.cardBox}>
                  <Grid>
                    <Row>
                      <Col>
                        <Grid>
                          {labelsContent}
                        </Grid>
                      </Col>
                    </Row>
                  </Grid>
              </View>
              : null }
              <ScrollView alwaysBounceVertical={false}>
                <View>
                  <View style={[styles.cardBox]}>
                    <Grid>
                      {(valuesContent.length < 10) ?
                        valuesContent.slice(0, 3)
                        :
                        valuesContent.slice(0, 5)
                      }
                    </Grid>
                  </View>
                  <View style={[styles.cardBox]}>
                    <Grid>
                      {(valuesContent.length < 10) ?
                        valuesContent.slice(3)
                        :
                        valuesContent.slice(5)
                      }
                    </Grid>
                  </View>
                </View>
              </ScrollView>
            </View>
          </View>

      </ScrollView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    marginTop:20,
  },
  cardBox: {
    marginBottom:10,
    marginLeft:10,
    marginRight:10,
    borderRadius:5,
    borderColor:'#EAEAEA',
    borderWidth:1,
    backgroundColor:'white',
  },
  goalton:{
    color:'#005499',
    fontSize:12,
    fontWeight:'bold',
    fontFamily: 'Montserrat',
  },
  column:{
    width:120,
    height:40
  },
  centeredCol: {
    alignItems:'center',
    justifyContent:'center',
    borderRightWidth:1,
    borderColor:'#EAEAEA'
  },
  alignLeftCol: {
    alignItems:'flex-start',
    justifyContent:'center',
  },
  headerColumn: {
    marginRight:1
  },
  colorBg: {
    backgroundColor:'#005596',
    borderRadius:2
  },
  colorBgSmallTh: {
    backgroundColor:'#005596',
    borderRadius:20,
    padding:5,
    width:50,
  },
  smallTh: {
    borderRadius:20,
    padding:20,
    width:50,
  },
  TextContent1:{
    fontSize:10,
    marginLeft:1,
    paddingTop:8,
    paddingBottom:8,
    textAlign:'center',
    color:'#000',
    fontFamily: 'Montserrat',
  },
  LabelRow1:{
    fontSize:12,
    paddingTop:15,
    paddingBottom:15,
    fontWeight:'bold',
    textAlign:'center',
    color:'#FFF',
    fontFamily: 'Montserrat',
  },
  LabelRow2:{
    fontSize:10,
    fontWeight:'bold',
    textAlign:'center',
    color:'#FFF',
    borderRadius:10,
    backgroundColor:'#005596',
    fontFamily: 'Montserrat',
  },
  LabelRow3: {
    fontSize:12,
    fontWeight:'bold',
    textAlign:'left',
    color:'#005596',
    fontFamily: 'Montserrat',
    marginLeft:10,
  },
  ValueContent1:{
    fontSize:12,
    paddingTop:10,
    paddingBottom:15,
    fontWeight:'bold',
    textAlign:'center',
    color:'#000',
    fontFamily: 'Montserrat',
  },
  NumberContent1:{
    fontSize:12,
    marginRight:1,
    marginTop:20,
    paddingTop:15,
    paddingBottom:15,
    fontWeight:'bold',
    justifyContent: 'flex-end',
    color:'#005499',
    fontFamily: 'Montserrat',
  },
  TextContent2:{
    fontSize:12,
    paddingTop:15,
    paddingBottom:15,
    paddingLeft:30,
    marginLeft:1,
    marginTop:20,
    fontWeight:'bold',
    color:'#FF892A',
    alignItems:'center',
    justifyContent:'center',
    fontFamily: 'Montserrat',
  },
  NumberContent2:{
    fontSize:12,
    marginRight:1,
    marginTop:20,
    paddingTop:15,
    paddingBottom:15,
    fontWeight:'bold',
    justifyContent: 'flex-end',
    color:'#FF892A',
    fontFamily: 'Montserrat',
  },
});
