import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { StyleSheet, View, TextInput, Text, ScrollView, TouchableWithoutFeedback, TouchableOpacity, Dimensions } from 'react-native';
import { Col, Row, Grid } from 'react-native-easy-grid';
import Image from 'react-native-remote-svg';
import ReportTableInfiniteColumns from '../../components/ReportTableInfiniteColumns';
import Spinner from 'react-native-loading-spinner-overlay';

import User from '../../../lib/User';
// import ReportTableFixedColumnBullet from '../../components/ReportTableFixedColumn';

var width = Dimensions.get('window').width;
var height = Dimensions.get('window').height;

export default class DailyReport extends Component {

  constructor(props) {
    super(props);

    this.state = {
      tabActive: 'Safra',
      values: [],
      monthName: null,
      isLoading: false
    }

    this.setTabActive = this.setTabActive.bind(this);
  }

  componentWillMount() {
    this.loadReport();
  }

  async loadReport() {
    let _this = this;

    var id = await User.getId();
    var month = Helper.getCurrentMonth();
    var year = Helper.getCurrentYear();

    var monthName = Helper.getMonth(month);
    this.setState({ monthName: monthName, isLoading: true }, function () {
      Api.getDailyProductionReport(id, year, month)
        .then(function (data) {

          if (typeof data == 'undefined') {
            setTimeout(() => {
              alert("Usuário sem permissão");
            }, 100);
            throw 'Usuário sem permissão';
          }

          values = [];
          for (idx in data) {
            item = data[idx];

            day = item.Dia.substr(8, 2);
            values.push([
              'Dia ' + day,
              Helper.formatNumber(item.Ton, 0),
              Helper.formatNumber(item.Atr, 2),
              Helper.formatNumber(item.Impureza),
              Helper.formatNumber(item.Tch, 0),
              Helper.formatNumber(item.AreaEstimada, 2)
            ]);
            console.log('value item', item);
            console.log('value values item', values);
          }

          console.log('value values', values);
          _this.setState({ values: values, isLoading: false });
          // console.log('value values', values);
        })
        .catch(function (err) {
          _this.setState({ isLoading: false });
        });
    });
  }

  setTabActive(tab) {
    this.setState({ tabActive: tab });
  }

  render() {
    const { navigate, state } = this.props.navigation;

    const { tabActive, monthName } = this.state;

    const labels = [monthName, 'Ton.', 'ATR (kg)', 'Impurezas (kg)', 'TCH', 'Área (ha)'];
    /* const values = [
      ['Dia 03', '241', '140', '4.5', '80', '3.01'],
      ['Dia 04', '241', '140', '4.5', '80', '3.01']
    ]; */

    values = this.state.values;

    return (
      <View style={{ backgroundColor: '#F7F7F7' }}>
        <ScrollView style={styles.viewContainer}>
          <View>
            {this.state.isLoading ?
              <View style={{ flex: 1 }}>
                <Spinner visible={true} textStyle={{ color: '#FFF' }} />
              </View>
              :
              <ReportTableInfiniteColumns labels={labels} values={values} />
            }
          </View>
        </ScrollView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  viewContainer: {

  },
  title: {
    fontSize: 23,
    marginTop: 10,
    marginBottom: 15,
    marginLeft: 15,
    fontWeight: 'bold',
    color: '#005499',
    fontFamily: 'Montserrat',
  },
  button: {
    backgroundColor: '#b2b2b2',
    height: 40,
    margin: 10,
    borderRadius: 5,
    textAlign: 'center',
    alignItems: 'center',
    justifyContent: 'center'
  },
  buttonActive: {
    backgroundColor: '#3ca9dd',
    height: 40,
    margin: 10,
    borderRadius: 5,
    textAlign: 'center',
    alignItems: 'center',
    justifyContent: 'center'
  },
  buttonText: {
    color: '#FFF',
    fontFamily: 'Montserrat',
    fontWeight: 'bold'
  }
});
