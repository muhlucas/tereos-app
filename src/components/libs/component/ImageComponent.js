// import React, { Component } from 'react';
// import {View,Text,ScrollView} from 'react-native';
// import Image from 'react-native-remote-svg';
// import { StyleSheet,TouchableOpacity} from 'react-native';
// import PropTypes from 'prop-types';
//
// const styles = StyleSheet.create({
//   img:{
//     width:'90%',
//     height:270,
//     borderRadius: 5,
//     marginRight: 20,
//     marginLeft: 20,
//     position:'absolute',
//     flex:1,
//   },
//   textTitle:{
//     fontSize:9,
//     color:'white',
//     marginTop:190,
//     marginLeft:30,
//
//   },textTitle2:{
//     fontSize:9,
//     color:'white',
//     marginTop:170,
//     marginLeft:30,
//
//   },
//   textContent:{
//     fontSize:17,
//     color:'white',
//     marginTop:1,
//     marginLeft:30,
//     fontWeight:'bold',
//     width:'70%',
//     lineHeight:24
//
//   }
// });
//
//
// export default class ImageComponent extends Component {
//   static propTypes = {
//       navigation: PropTypes.shape({
//           navigate: PropTypes.func
//       })
//   }
//
//   static defaultProps = {
//       navigation: {
//           navigate: () => {}
//       }
//   }
//
//   constructor(){
//       super();
//       this.onClose = this.onClose.bind(this);
//       this.onNext = this.onNext.bind(this);
//   }
//
//   onClose() {
//       this.props.navigation.goBack();
//   }
//   onNext() {
//       this.props.navigation.navigate('DetailViewScreen');
//   }
//
//   render() {
//     return (
//       <View>
//
//
//           <TouchableOpacity onPress={this.onNext} >
//             <Image style={styles.img}
//                 source={require('../../../assets/img/pic1.png')}
//             />
//             <Text style={styles.textTitle}>10/10/2017 - Conteúdo Exclusivo</Text>
//             <Text style={styles.textContent}>SEM ROTA 2030, ANFAVEA PREVE MENOS INVESTIMENTO</Text>
//           </TouchableOpacity>
//
//           <TouchableOpacity onPress={this.onNext} >
//              <View style={{marginTop:33}}>
//                 <Image style={styles.img}
//                     source={require('../../../assets/img/pic2.png')}
//                 />
//                 <Text style={styles.textTitle2}>10/10/2017 - Conteúdo Exclusivo</Text>
//                 <Text style={styles.textContent}>Biosev vê centro-sul com produção máxima de etanol em 18/19 e queda em açúcar</Text>
//               </View>
//           </TouchableOpacity>
//
//           <View style={{marginTop:33}}>
//             <TouchableOpacity onPress={this.onNext} >
//               <Image style={styles.img}
//                   source={require('../../../assets/img/pic3.png')}
//               />
//               <Text style={styles.textTitle}>10/10/2017 - Conteúdo Exclusivo</Text>
//               <Text style={styles.textContent}>Cosan acredita em cenário favorável para etanol brasileiro</Text>
//             </TouchableOpacity>
//
//           </View>
//       </View>
//     );
//   }
// }
