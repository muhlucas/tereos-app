import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { StyleSheet, View, TextInput, Text, ScrollView, TouchableWithoutFeedback, TouchableOpacity, Dimensions, Platform } from 'react-native';
import { Container, Header, Title, Content, Button, Icon, Right, Body, Left, Picker, Form } from "native-base";
import { Col, Row, Grid } from 'react-native-easy-grid';
import Image from 'react-native-remote-svg';
import SimplePicker from 'react-native-simple-picker';
import Spinner from 'react-native-loading-spinner-overlay';
import DialogAndroid from 'react-native-dialogs';
import NoDataButton from '../../components/NoDataButton';
import ReportTableCustomInfiniteColumns from '../../components/ReportTableCustomInfiniteColumns';
import ReportTableThreeColumns from '../../components/ReportTableThreeColumns';
import ReportTableTitleToExpand from '../../components/ReportTableTitleToExpand';
import ReportTableFixedColumn from '../../components/ReportTableFixedColumn';

import User from '../../../lib/User';

var width = Dimensions.get('window').width;
var height = Dimensions.get('window').height;

export default class FinancialReports extends Component {

  constructor(props) {
    super(props);

    this.state = {
      tabActive: 'Safra',
      plants: [],
      optionSelected: "TODAS",
      valuesByYear: [],
      valuesFortnight1: [],
      valuesFortnight2: [],
      labelsAll: [],
      labelsMonth: [],
      isLoading: false,
      emptyData: false
    }

    this.setTabActive = this.setTabActive.bind(this);
    this.loadData = this.loadData.bind(this);
  }

  componentWillMount() {
    this.loadData();
  }

  loadData() {
    let _this = this;
    _this.loadPlants();
    _this.loadReports();
  }

  loadReports() {
    let _this = this;
    let tab = this.state.tabActive;

    let year = Helper.getCurrentYear();
    let pastMonth = Helper.getPastMonth();
    let month = Helper.getCurrentMonth();

    // alert('pastMonth: ' + pastMonth + ' - month: ' + month);

    if (tab == 'Mês') {
      _this.setState({ isLoading: true }, function () {
        _this.loadReportByMonth(year, pastMonth, function () {
          _this.setState({ isLoading: false });
        });
      });
    } else {
      _this.setState({ isLoading: true }, function () {
        _this.loadReportByYear(year, function () {
          _this.setState({ isLoading: false });
        });
      })
    }
  }

  async loadPlants() {
    let _this = this;
    let year = Helper.getCurrentYear();
    let id = await User.getId();

    Api.getPlants(id, year)
      .then(function (data) {

        if (typeof data == 'undefined') {
          setTimeout(() => {
            alert("Usuário sem permissão");
          }, 100);
          throw 'Usuário sem permissão';
        }

        plants = ['TODAS'];
        for (idx in data) {
          item = data[idx];
          plants.push(item.Nome);
        }

        _this.setState({ plants: plants });
        // console.log('plants', data);
      })
  }

  async loadReportByYear(year, callback) {
    let _this = this;

    var id = await User.getId();

    var valuesByYear = [];
    var labelsAll = [];

    var plant = _this.state.optionSelected;

    // console.log('plant by year', plant);

    Api.getFinancialReportsByYear(id, plant, year)
      .then(function (data) {

        if (typeof data == 'undefined') {
          setTimeout(() => {
            alert("Usuário sem permissão");
          }, 100);
          throw 'Usuário sem permissão';
        }

        if (data) {
          console.log('data by year', data);

          for (idx in data) {
            item = data[idx];
            year = item.Safra;
            harvest = year.substring(2).replace('/', '');

            valuesObj = {
              year: year,
              VB: item.VB,
              ConsecanaAcumulado: item.Consecana_Acumulado,
              ATRRelativoAcumulado: item.ATR_Relativo_Acumulado,
              CTT: item.CTT,
              Associacao: item.Associacao,
              OutrosServicos: item.Outros_Servicos,
              // Retencao: Helper.calcRetention(item.VL_Acumulado), // item.Retencao,
              Retencao: '-', // item.Retencao,
              VL: item.VL_Acumulado,
            }

            labelsAll.push(harvest);
            valuesByYear.push(valuesObj);
          }

          emptyData = false;
        } else {
          emptyData = true;
        }

        _this.setState({ valuesByYear: valuesByYear, labelsAll: labelsAll, emptyData: emptyData }, function () {
          this.prepareDataForRender(function () {
            if (typeof callback != 'undefined') {
              callback();
            }
          });
        });

      }).catch(function (err) {
        _this.setState({ isLoading: false });
      });
  }

  async loadReportByMonth(year, month, callback) {
    let _this = this;

    var id = await User.getId();

    var valuesByMonth = [];

    var plant = this.state.optionSelected;

    var valuesFortnight1 = [];
    var valuesFortnight2 = [];

    var labelsMonth = [];

    Api.getFinancialReportsByMonth(id, plant, year, month)
      .then(function (data) {

        if (typeof data == 'undefined') {
          setTimeout(() => {
            alert("Usuário sem permissão");
          }, 100);
          throw 'Usuário sem permissão';
        }

        console.log('value data by month', data);

        if (data) {
          for (idx in data) {
            item = data[idx];

            console.log('item fortnight', item);
            itemMonth = item.data.substring(0, 2);

            fortnight = item.data.substr(3, 1);
            index = fortnight - 1;

            valueObj = {
              VB: item.VB,
              ConsecanaAcumulado: item.Consecana_Acumulado,
              ATRRelativoAcumulado: item.ATR_Relativo_Acumulado,
              CTT: item.CTT,
              Associacao: item.Associacao,
              OutrosServicos: item.Outros_Servicos,
              // Retencao: Helper.calcRetention(item.VL), // item.Retencao,
              Retencao: '-', // item.Retencao,
              VL: item.VL,
              ATRUsina: item.atrmdforn,
              ATRProvisorio: item.VL_ATR_MDUsina,
              ATRRelativo: item.atrrelativo,
            };

            if (fortnight == '1') {
              monthName = Helper.getMonth(itemMonth);
              labelsMonth.push(monthName);
              valuesFortnight1.push(valueObj);
            } else {
              valuesFortnight2.push(valueObj);
            }
          }

          console.log('valuesFortnight1', valuesFortnight1);
          console.log('valuesFortnight2', valuesFortnight2);
          emptyData = false;
        } else {
          emptyData = true;
        }

        _this.setState({ valuesFortnight1: valuesFortnight1, valuesFortnight2: valuesFortnight2, labelsMonth: labelsMonth, emptyData: emptyData }, function () {
          this.prepareDataForRender(function () {
            if (typeof callback != 'undefined') {
              callback();
            }
          });
        });

        /* if(typeof data != 'undefined') {
          fortnight = month.substr(3, 1);
  
          if(typeof valuesByMonth[index] == 'undefined') {
            valuesByMonth[index] = [];
          }
  
          if(typeof valuesByMonth[index][fortnight] == 'undefined') {
             valuesByMonth[index][fortnight] = {};
          }
  
          var item = data[0];
  
          valuesByMonth[index][fortnight].VB = item.VB;
  
          valuesByMonth[index][fortnight].VB = item.VB;
          valuesByMonth[index][fortnight].ConsecanaAcumulado = item.Consecana_Acumulado;
          valuesByMonth[index][fortnight].ATRRelativoAcumulado = item.ATR_Relativo_Acumulado;
          valuesByMonth[index][fortnight].CTT = item.CTT;
          valuesByMonth[index][fortnight].Associacao = item.Associacao;
          valuesByMonth[index][fortnight].OutrosServicos = item.Outros_Servicos;
          valuesByMonth[index][fortnight].Retencao = item.Retencao;
          valuesByMonth[index][fortnight].VL = item.VL;
  
          console.log('value valuesByMonth', valuesByMonth);
        } else {
          valuesByMonth[index][fortnight].VB = 0;
          valuesByMonth[index][fortnight].ConsecanaAcumulado = 0;
          valuesByMonth[index][fortnight].ATRRelativoAcumulado = 0;
          valuesByMonth[index][fortnight].CTT = 0;
          valuesByMonth[index][fortnight].Associacao = 0;
          valuesByMonth[index][fortnight].OutrosServicos = 0;
          valuesByMonth[index][fortnight].Retencao = 0;
          valuesByMonth[index][fortnight].VL = 0;
        }
  
        _this.setState({valuesByMonth: valuesByMonth}, function() {
          this.prepareDataForRender();
        });
  
        */

      }).catch(function (err) {
        _this.setState({ isLoading: false, emptyData: true });
      });
  }

  prepareDataForRender(callback) {

    const { tabActive, labelsAll, labelsMonth, optionSelected } = this.state;

    values1 = ['Consecana (acum.)'];
    values2 = ['ATR relativo (acum.)'];
    values3 = ['VB (R$)'];
    values4 = ['CTT (R$)'];
    values5 = ['Associação (R$)'];
    values6 = ['Outros serviços (R$)'];
    // values7 = ['Retenção (R$)'];
    values7 = ['-'];
    values8 = ['VL (R$)'];

    // var labels = ['Índice', '1819', '1718', '1617', '1516'];
    // var labelsAll = ['1819', '1718', '1617', '1516'];

    var valuesByYear = this.state.valuesByYear;

    for (idx in valuesByYear) {
      item = valuesByYear[idx];

      values1.push(item.ConsecanaAcumulado == null || item.ConsecanaAcumulado == '0' ? '0,00' : item.ConsecanaAcumulado);

      if (isNaN(parseInt(item.ATRRelativoAcumulado)) || parseInt(item.ATRRelativoAcumulado) == 0) {
        values2.push(" - ");
      } else
        values2.push(Helper.formatNumber(item.ATRRelativoAcumulado));

      values3.push(Helper.formatNumber(item.VB));
      values4.push(Helper.formatNumber(item.CTT));
      values5.push(Helper.formatNumber(item.Associacao));
      values6.push(Helper.formatNumber(item.OutrosServicos));
      // values7.push(Helper.formatNumber(item.Retencao));
      values7.push(item.Retencao);
      values8.push(Helper.formatNumber(item.VL));
      // alert('values2 length');
    }

    var valuesAll = [
      { values: ['Índice'], header: true, style: { height: 40 } },
      { values: values1, style: { height: 40 } },
      { values: values2, style: { height: 40 } },
      { values: values3, style: { height: 40 } },
      { values: values4, style: { height: 40 }, colTextStyle: { color: 'red' } },
      { values: values5, style: { height: 40 }, colTextStyle: { color: 'red' } },
      { values: values6, style: { height: 40 }, colTextStyle: { color: 'red' } },
      { values: values7, style: { height: 40 }, colTextStyle: {} },
      { values: values8, style: { height: 40 } },
    ];

    let isMonthAndPlant = (tabActive != 'Safra' && optionSelected != 'TODAS');

    // 1st fortnight

    values1 = ['Consecana (acum.)'];
    if (isMonthAndPlant) {
      values1_1 = ['ATR Usina'];
      values1_2 = ['ATR Provisório'];
      values1_3 = ['ATR Relativo'];
    } else {
      values2 = ['ATR Relativo (acum.)'];
    }
    values3 = ['VB (R$)'];
    values4 = ['CTT (R$)'];
    values5 = ['Associação (R$)'];
    values6 = ['Outros serviços (R$)'];
    // values7 = ['Retenção (R$)'];
    values7 = ['-'];
    values8 = ['VL (R$)'];

    var valuesFortnight1 = this.state.valuesFortnight1;
    for (idx in valuesFortnight1) {
      item = valuesFortnight1[idx];

      values1.push(item.ConsecanaAcumulado == null || item.ConsecanaAcumulado == '0' ? '0,00' : item.ConsecanaAcumulado);
      if (isMonthAndPlant) {
        values1_1.push(Helper.formatNumber(item.ATRUsina));
        values1_2.push(Helper.formatNumber(item.ATRProvisorio));

        if (isNaN(parseInt(item.ATRRelativo)) || parseInt(item.ATRRelativo) == 0) {
          values1_3.push(" - ");
        } else
          values1_3.push(Helper.formatNumber(item.ATRRelativo));
      } else {
        if (isNaN(parseInt(item.ATRRelativoAcumulado)) || parseInt(item.ATRRelativoAcumulado) == 0) {
          values2.push(" - ");
        } else
          values2.push(Helper.formatNumber(item.ATRRelativoAcumulado));
      }
      values3.push(Helper.formatNumber(item.VB));
      values4.push(Helper.formatNumber(item.CTT));
      values5.push(Helper.formatNumber(item.Associacao));
      values6.push(Helper.formatNumber(item.OutrosServicos));
      // values7.push(Helper.formatNumber(item.Retencao));
      values7.push(item.Retencao);
      values8.push(Helper.formatNumber(item.VL));
    }

    if (isMonthAndPlant) {
      var valuesMesQuinzena1_1 = [
        { values: ['Índice'], header: true, style: { height: 40 } },
        { values: values1, style: { height: 40 } },
        { values: values1_1, style: { height: 40 } },
        { values: values1_2, style: { height: 40 } },
        { values: values1_3, style: { height: 40 } },
        { values: values3, style: { height: 40 } },
        { values: values4, style: { height: 40 }, colTextStyle: { color: 'red' } },
        { values: values5, style: { height: 40 }, colTextStyle: { color: 'red' } },
        { values: values6, style: { height: 40 }, colTextStyle: { color: 'red' } },
        { values: values7, style: { height: 40 }, colTextStyle: {} },
        { values: values8, style: { height: 40 } },
      ];
    } else {
      var valuesMesQuinzena1_1 = [
        { values: ['Índice'], header: true, style: { height: 40 } },
        { values: values1, style: { height: 40 } },
        { values: values2, style: { height: 40 } },
        { values: values3, style: { height: 40 } },
        { values: values4, style: { height: 40 }, colTextStyle: { color: 'red' } },
        { values: values5, style: { height: 40 }, colTextStyle: { color: 'red' } },
        { values: values6, style: { height: 40 }, colTextStyle: { color: 'red' } },
        { values: values7, style: { height: 40 }, colTextStyle: {} },
        { values: values8, style: { height: 40 } },
      ];
    }


    // 2nd fortnight

    values1 = ['Consecana (acum.)'];
    if (isMonthAndPlant) {
      values1_1 = ['ATR Usina'];
      values1_2 = ['ATR Provisório'];
      values1_3 = ['ATR Relativo'];
    } else {
      values2 = ['ATR Relativo (acum.)'];
    }
    values3 = ['VB (R$)'];
    values4 = ['CTT (R$)'];
    values5 = ['Associação (R$)'];
    values6 = ['Outros serviços (R$)'];
    // values7 = ['Retenção (R$)'];
    values7 = ['-'];
    values8 = ['VL (R$)'];

    var valuesFortnight2 = this.state.valuesFortnight2;

    for (idx in valuesFortnight2) {
      item = valuesFortnight2[idx];

      values1.push(item.ConsecanaAcumulado == null || item.ConsecanaAcumulado == '0' ? '0,00' : item.ConsecanaAcumulado);
      if (isMonthAndPlant) {
        values1_1.push(Helper.formatNumber(item.ATRUsina));
        values1_2.push(Helper.formatNumber(item.ATRProvisorio));
        if (item.ATRRelativo == 0 || item.ATRRelativo == "0") {
          values1_3.push(" - ");
        }
        else
          values1_3.push(Helper.formatNumber(item.ATRRelativo));
      } else {
        if (isNaN(parseInt(item.ATRRelativoAcumulado)) || parseInt(item.ATRRelativoAcumulado) == 0) {
          values2.push(" - ");
        } else
        values2.push(Helper.formatNumber(item.ATRRelativoAcumulado));
      }
      values3.push(Helper.formatNumber(item.VB));
      values4.push(Helper.formatNumber(item.CTT));
      values5.push(Helper.formatNumber(item.Associacao));
      values6.push(Helper.formatNumber(item.OutrosServicos));
      // values7.push(Helper.formatNumber(item.Retencao));
      values7.push(item.Retencao);
      values8.push(Helper.formatNumber(item.VL));
    }

    if (isMonthAndPlant) {
      var valuesMesQuinzena2_1 = [
        { values: ['Índice'], header: true, style: { height: 40 } },
        { values: values1, style: { height: 40 } },
        { values: values1_1, style: { height: 40 } },
        { values: values1_2, style: { height: 40 } },
        { values: values1_3, style: { height: 40 } },
        { values: values3, style: { height: 40 } },
        { values: values4, style: { height: 40 }, colTextStyle: { color: 'red' } },
        { values: values5, style: { height: 40 }, colTextStyle: { color: 'red' } },
        { values: values6, style: { height: 40 }, colTextStyle: { color: 'red' } },
        { values: values7, style: { height: 40 }, colTextStyle: {} },
        { values: values8, style: { height: 40 } },
      ];
    } else {
      var valuesMesQuinzena2_1 = [
        { values: ['Índice'], header: true, style: { height: 40 } },
        { values: values1, style: { height: 40 } },
        { values: values2, style: { height: 40 } },
        { values: values3, style: { height: 40 } },
        { values: values4, style: { height: 40 }, colTextStyle: { color: 'red' } },
        { values: values5, style: { height: 40 }, colTextStyle: { color: 'red' } },
        { values: values6, style: { height: 40 }, colTextStyle: { color: 'red' } },
        { values: values7, style: { height: 40 }, colTextStyle: {} },
        { values: values8, style: { height: 40 } },
      ];
    }

    // var labelsMonth = ['Julho', 'Junho', 'Maio', 'Abril'];

    /* var valuesMesQuinzena1_1 = [
      {values: ['Índice'], header: true, style: {height: 40}},
      {values: ['Consecana (acum.)'], style: {height: 40}},
      {values: ['ATR relativo (acum.)'], style: {height: 40}},
      {values: ['VB (R$)'], style: {height: 40}},
      {values: ['CTT (R$)'], style: {height: 40}, colTextStyle: {color:'red'}},
      {values: ['Associação (R$)'], style: {height: 40}, colTextStyle: {color:'red'}},
      {values: ['Outros serviços (R$)'], style: {height: 40}, colTextStyle: {color:'red'}},
      {values: ['Retenção (R$)'], style: {height: 40}, colTextStyle: {}},
      {values: ['VL (R$)'], style: {height: 40}},
    ];

    var valuesMesQuinzena2_1 = [
      {values: ['Índice'], header: true, style: {height: 40}},
      {values: ['Consecana (acum.)'], style: {height: 40}},
      {values: ['ATR relativo (acum.)'], style: {height: 40}},
      {values: ['VB (R$)'], style: {height: 40}},
      {values: ['CTT (R$)'], style: {height: 40}, colTextStyle: {color:'red'}},
      {values: ['Associação (R$)'], style: {height: 40}, colTextStyle: {color:'red'}},
      {values: ['Outros serviços (R$)'], style: {height: 40}, colTextStyle: {color:'red'}},
      {values: ['Retenção (R$)'], style: {height: 40}, colTextStyle: {}},
      {values: ['VL (R$)'], style: {height: 40}},
    ];

    var valuesByMonth = this.state.valuesByMonth;

    for(idx in valuesByMonth) {
      for(fortnight in valuesByMonth[idx]) {
        item = valuesByMonth[idx][fortnight];

        if(fortnight == '1') { // first fortnight of month
          valuesMesQuinzena1_1[0].values.push(item.ConsecanaAcumulado);
          valuesMesQuinzena1_1[1].values.push(item.ATRRelativoAcumulado);
          valuesMesQuinzena1_1[2].values.push(item.VB);
          valuesMesQuinzena1_1[3].values.push(item.CTT);
          valuesMesQuinzena1_1[4].values.push(item.Associacao);
          valuesMesQuinzena1_1[5].values.push(item.OutrosServicos);
          valuesMesQuinzena1_1[6].values.push(item.Retencao);
          valuesMesQuinzena1_1[7].values.push(item.VL);

        } else { // second fortnight/half of month
          valuesMesQuinzena2_1[0].values.push(item.ConsecanaAcumulado);
          valuesMesQuinzena2_1[1].values.push(item.ATRRelativoAcumulado);

          valuesMesQuinzena2_1[2].values.push(item.VB);
          valuesMesQuinzena2_1[3].values.push(item.CTT);
          valuesMesQuinzena2_1[4].values.push(item.Associacao);
          valuesMesQuinzena2_1[5].values.push(item.OutrosServicos);
          valuesMesQuinzena2_1[6].values.push(item.Retencao);
          valuesMesQuinzena2_1[7].values.push(item.VL);
        }
      }
    }

    // alert('prepareDataForRender callback1');
    this.setState({valuesMesQuinzena1_1, valuesMesQuinzena1_1, valuesAll, labelsAll, labelsMonth}, function() {
      // alert('prepareDataForRender callback2');
      if(typeof callback != 'undefined') {
        callback();
      }
    });
    */

    this.setState({ valuesMesQuinzena1_1, valuesMesQuinzena2_1, valuesAll, labelsAll, labelsMonth }, function () {
      // alert('prepareDataForRender callback2');
      console.log('valuesAll', valuesAll);
      if (typeof callback != 'undefined') {
        callback();
      }
    });
  }

  setTabActive(tab) {
    let _this = this;
    console.log('setTabActive');
    this.setState({ tabActive: tab }, function () {
      _this.loadReports();
    });
  }

  async showPicker() {
    let _this = this;

    if (Platform.OS == 'ios') {
      this.refs.picker2.show();
    } else {
      let options = [];
      let plants = this.state.plants;
      for (idx in plants) {
        plant = plants[idx];
        options.push({ label: plant, id: plant });
      }

      // DialogAndroid.alert('Title', 'Message');
      const { selectedItem } = await DialogAndroid.showPicker('Escolha uma Usina', null, {
        items: options
      });
      if (selectedItem) {
        // console.log('selectedItem', selectedItem);
        // alert('selectedItem: ' + selectedItem.id)
        _this.setState({
          optionSelected: selectedItem.id,
          valuesByYear: []
        }, function () {
          _this.loadReports();
        });
      }
    }
  }

  render() {
    let _this = this;

    const { navigate, state } = this.props.navigation;

    const { tabActive, valuesMesQuinzena1_1, valuesMesQuinzena2_1, valuesAll, labelsAll, labelsMonth, emptyData } = this.state;

    return (
      <View style={{ backgroundColor: '#F7F7F7' }}>
        <View>
          <View style={styles.titleTop}>
            <Text style={styles.titleTopText}>Usina</Text>

            <TouchableOpacity style={styles.pickerSelect}
              onPress={() => {
                this.showPicker();
              }}
            >
              <Text style={styles.pickerSelectText}>{this.state.optionSelected}</Text>
            </TouchableOpacity>

            <SimplePicker
              ref={'picker2'}
              options={this.state.plants}
              confirmText={'Confirmar'}
              cancelText={'Cancelar'}
              itemStyle={{
                fontSize: 25,
                marginLeft: 10,
                textAlign: 'left',
                fontWeight: 'bold',
              }}
              onSubmit={(option) => {
                let _this = this;
                this.setState({
                  optionSelected: option,
                  valuesByYear: []
                }, function () {
                  _this.loadReports();
                });
              }}
            />

            {/*<Picker
              iosHeader="Selecione"
              headerBackButtonText="Voltar"
              mode="dropdown"
              selectedValue={this.state.optionSelected}
              onValueChange={this.onValueChange.bind(this)}
              placeholderStyle={styles.placeholderPicker}
            >
              <Picker.Item label="Andrade" value="andrade" />
              <Picker.Item label="Cruz Alta" value="cruz_alta" />
              <Picker.Item label="Santo André" value="santo_andre" />
            </Picker>*/}

          </View>
          <View style={styles.buttons}>
            <Grid>
              <Row>
                <Col>
                  {tabActive == 'Safra' ?
                    <TouchableOpacity style={styles.buttonActive} onPress={() => this.setTabActive('Safra')}>
                      <Text style={styles.buttonText}>Safra</Text>
                    </TouchableOpacity>
                    :
                    <TouchableOpacity style={styles.button} onPress={() => this.setTabActive('Safra')}>
                      <Text style={styles.buttonText}>Safra</Text>
                    </TouchableOpacity>
                  }
                </Col>
                <Col>
                  {tabActive == 'Mês' ?
                    <TouchableOpacity style={styles.buttonActive} onPress={() => this.setTabActive('Mês')}>
                      <Text style={styles.buttonText}>Mês</Text>
                    </TouchableOpacity>
                    :
                    <TouchableOpacity style={styles.button} onPress={() => this.setTabActive('Mês')}>
                      <Text style={styles.buttonText}>Mês</Text>
                    </TouchableOpacity>
                  }
                </Col>
              </Row>
            </Grid>
          </View>


          {this.state.isLoading ?
            <View style={{ flex: 1 }}>
              <Spinner visible={true} textStyle={{ color: '#FFF' }} />
            </View>
            :
            tabActive == 'Safra' ?
              emptyData ?
                <NoDataButton onPress={_this.loadData} /> :
                <ScrollView style={styles.scrollViewContainer}>
                  <ReportTableFixedColumn type={tabActive} headerBgAllFilled={true} labels={labelsAll} values={valuesAll} firstColDoubleSize={true} />
                </ScrollView>
              :
              emptyData ?
                <NoDataButton onPress={_this.loadData} /> :
                <ScrollView style={styles.scrollViewContainer}>
                  <Text style={styles.subtitle}>1ª Quinzena</Text>

                  <ReportTableFixedColumn type={tabActive} labels={labelsMonth} values={valuesMesQuinzena1_1} firstColDoubleSize={true} />

                  <Text style={styles.subtitle}>2ª Quinzena</Text>

                  <ReportTableFixedColumn type={tabActive} labels={labelsMonth} values={valuesMesQuinzena2_1} firstColDoubleSize={true} />
                  {/*

                     <ReportTableCustomInfiniteColumns type={tabActive} headerBgAllFilled={true} labels={labels} values={valuesMesQuinzena2_1} firstColDoubleSize={true} />
                    <ScrollView style={styles.scrollViewContainer}>

                    </ScrollView>
                    */}

                </ScrollView>
          }

        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  scrollViewContainer: {
    ...Platform.select({
      android: {
        marginBottom: 235,
      },
      ios: {
        marginBottom: 235,
      }
    }),
  },
  titleTop: {
    margin: 10,
  },
  titleTopText: {
    fontFamily: 'Montserrat',
  },
  placeholderPicker: {
    fontFamily: 'Montserrat',
  },
  buttons: {
    zIndex: 999,
    height: 50,
  },
  title: {
    fontSize: 23,
    marginTop: 10,
    marginBottom: 15,
    marginLeft: 15,
    fontWeight: 'bold',
    color: '#005499',
    fontFamily: 'Montserrat',
  },
  subtitle: {
    fontSize: 12,
    marginTop: 20,
    marginLeft: 15,
    fontWeight: 'bold',
    color: '#005499',
    fontFamily: 'Montserrat',
  },
  button: {
    backgroundColor: '#b2b2b2',
    height: 40,
    margin: 10,
    borderRadius: 5,
    textAlign: 'center',
    alignItems: 'center',
    justifyContent: 'center'
  },
  buttonActive: {
    backgroundColor: '#3ca9dd',
    height: 40,
    margin: 10,
    borderRadius: 5,
    textAlign: 'center',
    alignItems: 'center',
    justifyContent: 'center'
  },
  buttonText: {
    color: '#FFF',
    fontFamily: 'Montserrat',
    fontWeight: 'bold'
  },
  pickerSelect: {
    marginTop: 5,
    borderBottomWidth: 1,
    borderColor: '#0c5a97'
  },
  pickerSelectText: {
    fontFamily: 'Montserrat',
    fontSize: 20,
    fontWeight: 'bold',
    color: '#0c5a97'
  }
});
