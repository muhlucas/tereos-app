import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { StyleSheet, View, ScrollView, Text, TouchableOpacity, AsyncStorage, Dimensions, Image, Platform } from 'react-native';
// import Image from 'react-native-remote-svg';
import Spinner from 'react-native-loading-spinner-overlay';
import ImagePicker from 'react-native-image-crop-picker';

import Api from '../../../lib/Api';
import User from '../../../lib/User';

var width = Dimensions.get('window').width;
var height = Dimensions.get('window').height;

export default class Profile extends Component {

  constructor(props) {
    super();

    this.state = {
      profileData: null,
      id: null,
      name: null,
      email: null,
      address: null,
      mobilePhone: null,
      cpf: null,
      image: null,
      managerName: null,
      managerPhone: null,
      managerMobilePhone: null,
      managerImage: null,
      isLoading: false
    }
  }

  componentWillReceiveProps() {
    this.loadProfile();
  }

  componentDidMount() {
    this.loadProfile();
  }

  openImagePicker() {
    let _this = this;

    ImagePicker.openPicker({
      width: 300,
      height: 400,
      cropping: true,
      includeBase64: true,
      compressImageQuality: 0.5
    }).then(result => {
      let profileImage = result.data;

      if(profileImage != null) {
        _this.setState({image: 'data:image/png;base64,' + profileImage}, function() {
          values = _this.state.profileData;
          values.Gestor = null;
          values.Foto = _this.state.image;

          let id = _this.state.id;

          console.log('data values', values);

          this.setState({isLoading: true}, function() {
            Api.updateProfile(id, values)
            .then(function(data) {
              console.log('data ', data);
              _this.setState({isLoading: false});
            });
          })
        });
      }

      console.log('image picker', result);
    });
  }

  logout() {
    let _this = this;
    User.removeNotificationPreference();
    User.removePlayerId();
    AsyncStorage.removeItem('@api_token');
    AsyncStorage.removeItem('@login', function() {
      _this.props.navigation.navigate('Login');
    })
  }

  async loadProfile() {
    let _this = this;

    let id = await User.getId();

    this.setState({isLoading: true, id: id}, function() {
      Api.getProfile(id, false)
      .then(function(data) {
        console.log('data', data);

        if(data) {
          address = (data.Endereco) ? data.Endereco.trim() : '';
          if(data.Numero) {
            address += ', ' + data.Numero.trim();
          }

          if(data.Complemento) {
            address += ' - ' + data.Complemento.trim();
          }

          address = address.toUpperCase();

          cpf = Helper.formatCPF(data.CPF);

          phone = (data.Telefone) ? Helper.formatPhoneNumber(data.Telefone) : '';

          mobilePhone = (data.Celular1) ? Helper.formatPhoneNumber(data.Celular1) : '';

          city = (data.CIDADE) ? data.CIDADE.trim() : '';
          state = (data.UF) ? data.UF.trim() : '';
          zipcode = (data.CEP) ? data.CEP.trim() : '';

          // image = 'data:image/png;base64,' + data.Foto;
          image = data.Foto;
          // image = data.Gestor.Foto;
          managerImage = (data.Gestor) ? data.Gestor.Foto : null;

          _this.setState({
            profileData: data,
            name: data.Nome,
            email: data.Email,
            address: address,
            city: city,
            state: state,
            zipcode: zipcode,
            phone: phone,
            mobilePhone: mobilePhone,
            cpf: cpf,
            image: image,
            managerName: (data.Gestor) ? data.Gestor.Nome.trim() : null,
            managerPhone: (data.Gestor) ? data.Gestor.TelefoneFixo.trim() : null,
            managerMobilePhone: (data.Gestor) ? data.Gestor.Celular.trim() : null,
            managerImage: managerImage,
            isLoading: false
          });
        } else {
          _this.setState({isLoading: false});
        }

        // console.log('data profile', data);
      })
      .catch(function(err) {
        alert('err: ' + err)
        _this.setState({isLoading: false});
      });
    })
  }

  render() {

    let nameLength = (this.state.name) ? this.state.name.length : 1;
    let nameFontSize = (width)/(nameLength/1.5);
    if(nameFontSize > 26) {
      nameFontSize = 26;
    }

    let managerNameLength = (this.state.managerName) ? this.state.managerName.length : 1;
    let managerFontSize = (width)/(managerNameLength/1.0);
    if(managerFontSize > 15 ) {
      managerFontSize = 15;
    }

    return (
      <View style={styles.container}>
       <ScrollView>
         {this.state.isLoading ?
           <Spinner visible={true} textStyle={{color: '#FFF'}} />
         : null }
            <View style={styles.CircleImg}>
              <View style={styles.PhotoBar}>
                {/*
                <Image style={styles.imgLarge}
                    source={require('../../../assets/img/person_2.png')}
                />
                */}
                {this.state.image ?
                  <Image style={styles.imgLarge}
                    source={{uri: this.state.image}}
                    />
                :
                  <View style={styles.imgLarge} />
                }
                <TouchableOpacity style={styles.imgView}
                onPress={() => {this.openImagePicker(); }}>
                  <Image style={styles.imgPhoto}
                      source={require('../../../assets/img/camera.png')}
                  />
                </TouchableOpacity>
              </View>
              <View style={styles.listItem1}>
                {Platform.OS == 'ios' ?
                  <Text style={styles.name} adjustsFontSizeToFit={true} numberOfLines={1}>{this.state.name}</Text>
                :
                  <Text style={[styles.name, {fontSize:nameFontSize}]}>{this.state.name}</Text>
                }
                {/* <Text style={styles.membership}> Membro Gold </Text> */}
              </View>
              <View style={styles.listItem2}>
                <Text style={styles.normalText}><Text style={{fontWeight:'bold'}}>CPF / Nome de usuário:</Text> {this.state.cpf}</Text>
              </View>
              <View style={styles.listItem3}>
                <Text style={styles.normalText}><Text style={{fontWeight:'bold'}}>Telefone Fixo:</Text> (17) 3280-1000</Text>
                <Text style={styles.normalSpaceText}><Text style={{fontWeight:'bold'}}>Celular:</Text>             {this.state.mobilePhone}</Text>
              </View>

              <View style={styles.listItem2}>
                <Text style={styles.normalText}><Text style={{fontWeight:'bold'}}>Email:</Text>               {this.state.email} </Text>
              </View>

              <View style={styles.listItem4}>
                <Text style={styles.normalText}><Text style={{fontWeight:'bold'}}>Endereço:</Text></Text>
                <Text style={[styles.normalText, {marginTop:5}]}>{this.state.address}</Text>
                {this.state.city && this.state.state && this.state.zipcode ?
                  <Text style={styles.normalText}>{this.state.city} - {this.state.state} CEP: {Helper.formatZipCode(this.state.zipcode)}</Text>
                  :
                  <Text style={styles.normalText}>-</Text>
                }
              </View>

              {this.state.managerName ?
                <View>
                  <View style={styles.listItem5}>

                    <View style={styles.share}>

                        <Text style={styles.normalText}><Text style={{fontWeight:'bold'}}>Seu Gestor</Text></Text>

                        {/*
                        <Image style={styles.shareimg}
                            source={require('../../../assets/share.png')}
                        />
                        */}

                    </View>

                    <View style={styles.sharePic}>
                      {this.state.managerImage ?
                      <Image style={styles.smallimg}
                          source={{uri: this.state.managerImage}}
                      />
                      : null }
                      <View>
                        {Platform.OS == 'ios' ?
                          <Text style={[styles.middleText, {width:width-120}]} adjustsFontSizeToFit={true} numberOfLines={1}>{this.state.managerName}</Text>
                        :
                          <Text style={[styles.middleText, {width:width-120, fontSize:managerFontSize}]}>{this.state.managerName}</Text>
                        }
                        {/* <Text style={styles.normalText5}>Seu gestor desde<Text style={{color:'#00D5F7'}}> 10/10/2013</Text></Text> */}
                      </View>
                    </View>

                  </View>

                  <View style={styles.listItem6}>
                    <Text style={styles.normalText}><Text style={{fontWeight:'bold'}}>Telefone Fixo:</Text> {this.state.managerPhone}</Text>
                    <Text style={styles.normalSpaceText}><Text style={{fontWeight:'bold'}}>Celular:</Text>             {this.state.managerMobilePhone}</Text>
                  </View>
                </View>
                :
                null
              }

              <TouchableOpacity
                style={styles.boxButton}
                onPress={() => {
                  this.logout();
                }}>
               <Text style={styles.button}>Sair</Text>
              </TouchableOpacity>
         </View>
        </ScrollView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
    container: {
      backgroundColor: '#F7F7F7',
      color:'#cc00ff'
    },
    listItem:{
      backgroundColor:'white',
      marginTop:30,
      marginLeft:10,
      marginRight:10,
      height:100,
      borderRadius:7,
      borderWidth: 1,
      borderColor: '#EFF4F6',
      fontFamily: 'Montserrat',
    },
    listItem1:{
      backgroundColor:'white',
      marginTop:45,
      marginLeft:10,
      marginRight:10,
      height:70,
      borderRadius:7,
      borderWidth: 1,
      borderColor: '#EFF4F6',
      paddingTop:20,
      paddingLeft:20,
      paddingRight:20,
      fontFamily: 'Montserrat',
    },
    name:{
      color:'#0c4F77',
      fontFamily: 'Montserrat',
      textAlign:'left',
      fontSize:25
    },
    membership:{
      fontSize:13,
      color:'#00D5F7',
      fontWeight:'bold',
      paddingLeft:5,
      paddingBottom:15,
      fontFamily: 'Montserrat',
    },
    listItem2:{
      backgroundColor:'white',
      marginTop:-2,
      marginLeft:10,
      marginRight:10,
      height:54,
      borderRadius:7,
      borderWidth: 1,
      borderColor: '#EFF4F6',
      paddingTop:15,
      paddingLeft:20,
      fontFamily: 'Montserrat',
    },
    normalText:{
      fontSize:15,
      color:'#0c4F77',
      paddingLeft:5,
      fontFamily: 'Montserrat',
    },
    normalSpaceText:{
      fontSize:15,
      color:'#0c4F77',
      paddingLeft:5,
      marginTop:3,
      fontFamily: 'Montserrat',
    },
    listItem3:{
      backgroundColor:'white',
      marginTop:7,
      marginLeft:10,
      marginRight:10,
      height:93,
      borderRadius:7,
      borderWidth: 1,
      borderColor: '#EFF4F6',
      paddingTop:30,
      paddingLeft:20,
      fontFamily: 'Montserrat',
    },
    listItem4:{
      backgroundColor:'white',
      marginTop:0,
      marginLeft:10,
      marginRight:10,
      height:115,
      borderRadius:7,
      borderWidth: 1,
      borderColor: '#EFF4F6',
      paddingTop:15,
      paddingLeft:20,
      fontFamily: 'Montserrat',
    },
    imgLarge:{
      marginTop:30,
      borderRadius: 80,
      width:165,
      height:165,
      borderWidth:1,
      borderColor:'#00D5F7',
      justifyContent:'center',
      alignItems:'center',
    },
    imgView:{
      position:'absolute',
      borderRadius: 150,
      width:52,
      height:52,
      right:125,
      alignItems:'center',
      padding:12,
      backgroundColor:'white',
      bottom:-10,
    },
    imgPhoto:{
      width:15,
      height:8,
      padding:12
    },
    listItem5:{
      backgroundColor:'white',
      marginTop:8,
      marginLeft:10,
      marginRight:10,
      height:160,
      borderRadius:7,
      borderWidth: 1,
      borderColor: '#EFF4F6',
      paddingTop:20,
      paddingLeft:20,
      fontFamily: 'Montserrat',
    },
    PhotoBar:{
      justifyContent:'center',
      alignItems:'center'
    },
    header:{
      flexDirection:'row',
      justifyContent:'flex-end',
      paddingRight:10,
      borderBottomWidth:2,
      borderBottomColor:'#F3F6F8',
      height:30,
      alignItems:'center',
      backgroundColor:'white',
      fontFamily: 'Montserrat',
    },
    back:{
      width:80,
      height:30,
      marginTop:20
    },
    edit:{
      fontWeight:'bold',
      fontFamily: 'Montserrat',
    },
    share:{
      flexDirection:'row',
      justifyContent:'space-between',
      paddingTop:5,
      fontFamily: 'Montserrat',
    },
    smallimg:{
      borderRadius:30,
      width:60,
      height:60,
      borderWidth:1,
      borderColor:'#0c4F77'
    },
    shareimg:{
      width:22,
      height:22,
      marginRight:15
    },
    sharePic:{
      flexDirection:'row',
      marginTop:30
    },
    middleText:{
      fontSize:15,
      color:'#0c4F77',
      marginLeft:16,
      marginTop:18,
      fontFamily: 'Montserrat',
    },
    listItem6:{
      backgroundColor:'white',
      marginTop:0,
      marginLeft:10,
      marginRight:10,
      height:93,
      borderRadius:7,
      borderWidth: 1,
      borderColor: '#EFF4F6',
      paddingTop:30,
      paddingLeft:20,
      fontFamily: 'Montserrat',
    },
    normalText5:{
      fontSize:12,
      color:'#0c4F77',
      paddingLeft:5,
      marginLeft:18,
      fontFamily: 'Montserrat',
    },
    boxButton: {
      alignItems:'center',
      justifyContent:'center',
      marginBottom:20
    },
    button:{
      backgroundColor:'#00ABE0',
      fontSize:15,
      paddingTop:15,
      paddingBottom:15,
      width:'93%',
      marginTop:10,
      textAlign:'center',
      borderColor:'#A9B8D6',
      borderWidth:1,
      alignItems:'center',
      justifyContent:'center',
      color:'white',
      fontFamily: 'Montserrat',
    },
});
