import { StyleSheet, Dimensions, Platform } from 'react-native';

var width = Dimensions.get('window').width;
var height = Dimensions.get('window').height;

const styles = StyleSheet.create({
    container: {
        display: 'flex',
        flex: 1,
        flexDirection: 'column',
        backgroundColor: '#FFFFFF',
    },
    logo:{
      borderColor:'#D4D4D4',
      width:width*0.6,
      height:(width*0.6)/2.1,
      marginTop:height*0.10,
      alignSelf:'center',
    },
    backgroundImage: {
      flex: 1,
      resizeMode: 'cover', // or 'stretch'
      position: 'absolute',
      top: 0,
      bottom: 0,
      left: 0,
      right: 0,
      width:width,
      height:height,
    },
    loginForm: {
      alignItems:'flex-start',
      width:width*0.8,
      marginTop:50
    },
    TextInput:{
      backgroundColor:'white',
      fontSize:15,
      padding:10,
      width:(width*0.8),
      marginTop:20,
      borderColor:'#005596',
      borderWidth:0.5,
      fontFamily: 'Montserrat',
    },
    headerAccountAccess: {
      fontFamily: 'Montserrat',
      color:'#005596',
      fontSize:14,
      fontWeight:'bold'
    },
    boxForgotPassword: {
        marginTop:20,
        marginBottom:20,
    },
    textForgotPassword: {
      fontFamily: 'Montserrat',
      color:'#005596',
      fontSize:11,
      textDecorationLine:'underline'
    },
    boxKeepLogged: {
      alignItems:'flex-start',
      flex:1,
      flexDirection:'row'
    },
    textKeepLogged: {
      fontFamily: 'Montserrat',
      color:'#5f5f5f',
      fontSize:11,
    },
    labelKeepLogged: {
      height:30,
      marginRight:10,
      justifyContent:'center'
    },
    button: {
        backgroundColor: '#3ca9dd',
        width:width*0.4,
        height:(width*0.1),
        borderRadius:0,
        marginTop:50,
        alignItems:'center',
        alignSelf:'center',
        justifyContent:'center',
    },
    textButton: {
      fontFamily: 'Montserrat',
      fontSize:12,
      textAlign:'center',
      color:'#FFFFFF',
    },
    text: {
      fontFamily: 'Montserrat',
      fontSize:13,
      marginTop:10
    },
    boxBackButton: {
      alignItems:'flex-start',
      alignSelf:'flex-start',
      marginLeft:10,
      height:25.5,
    },
    backIcon: {
      height:25.5,
      width:65,
      ...Platform.select({
        android: {
          marginTop:15
        },
        ios: {
          marginTop:8
        }
      }),
    },
    version: {
      position:'absolute',
      top:height-15,
      right:5,
    },
    versionText: {
      fontSize:9
    }
});

export default styles;
