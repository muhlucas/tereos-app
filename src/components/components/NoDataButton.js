import React, { Component } from 'react';
import {
    View,
    Dimensions
} from 'react-native';
import { StyleSheet, ScrollView} from 'react-native';
import {Button, Text} from 'native-base';
import {getConnectionInfo} from '../../lib/Api';

var width = Dimensions.get('window').width;
var height = Dimensions.get('window').height;

export default class NoDataButton extends Component {

  constructor(props) {
      super(props);

      this.state = {
        netinfo: null
      }
  }

  componentDidMount() {
    this.getConnectionInfo();
  }

  async getConnectionInfo() {
    const netinfo = await getConnectionInfo();
    this.setState({netinfo});
  }

  render() {
    const {netinfo} = this.state;

    let isConnected = (netinfo && (netinfo != null && netinfo.type != 'none'));
    // alert('isConnected: ' + isConnected);
    // isConnected = false;

    return (
      <View style={styles.container}>
        <Text style={styles.noDataFound}>{isConnected ? 'Sem resultados' : 'Você precisa estar online para ver este conteúdo.'}</Text>
        <Button small primary onPress={this.props.onPress} style={styles.button}>
          <Text>Clique para recarregar</Text>
        </Button>
      </View>
      );
    }
}

  const styles = StyleSheet.create({
    container: {
      alignItems: 'center',
      justifyContent: 'center',
      padding:10
    },
    noDataFound: {
      fontFamily: 'Montserrat',
      marginBottom: 10,
      fontSize: 15,
      marginTop:10,
      color:'#000000',
      textAlign:'center'
    },
    button: {
      alignSelf:'center',
      padding:5,
      color: '#FFFFFF',
      fontFamily: 'Montserrat',
    }
  });
