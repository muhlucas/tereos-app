import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { StyleSheet, View, TextInput, Text, ScrollView, TouchableWithoutFeedback, Dimensions } from 'react-native';
import { Col, Row, Grid } from 'react-native-easy-grid';
import Image from 'react-native-remote-svg';
import IndicatorTableReport from '../../components/IndicatorTableReport';
import AnimateNumber from 'react-native-animate-number';
import Chart from '../../libs/component/Chart';
import Spinner from 'react-native-loading-spinner-overlay';
import HomeTab from '../../../routing/tabs/HomeTab';

var width = Dimensions.get('window').width;
var height = Dimensions.get('window').height;

export default class IndicatorReport extends Component {

  constructor(props) {
    super(props);

    this.state = {
      data: [],
      isLoading: false
    }
  }

  componentDidMount() {
    this.loadIndexes();
  }

  loadIndexes() {
    let _this = this;
    indexes = [];

    _this.setState({ isLoading: true }, function () {
      Api.getFullConsecanaIndexes()
        .then(function (data) {

          if (typeof data == 'undefined') {
            setTimeout(() => {
              alert("Usuário sem permissão");
            }, 100);
            throw 'Usuário sem permissão';
          }

          if (typeof data != 'undefined' && data.length) {
            for (idx in data) {
              item = data[idx];

              itemDate = Helper.getMonth(item.Mes) + '/' + item.Ano;

              indexes.push({
                values: [
                  itemDate,
                  item.ValorMensal,
                  item.ValorAcumulado
                ]
              });
            }
          }

          _this.setState({ data: indexes, isLoading: false })
        })
        .catch(function (err) {
          _this.setState({ isLoading: false })
        });
    });
  }

  openReport() {
    const { navigate, state } = this.props.navigation;

    navigate('IndicatorDetailReport');
  }

  render() {
    const { navigate, state } = this.props.navigation;

    const labels = ['Mês', 'Mensal', 'Acumulada'];

    values = this.state.data;

    /* const values = [
      {values: ['Abril/2018', '0,5671', '0,5671']},
      {values: ['Março/2018', '0,6161', '0,5901']},
      {values: ['Fevereiro/2018', '0,6140', '0,5844']},
      {values: ['Janeiro/2018', '0,6176', '0,5815']},
      {values: ['Dezembro/2017', '0,6111', '0,5769']},
      {values: ['Novembro/2017', '0,5820', '0,5719']},
      {values: ['Outubro/2017', '0,5490', '0,5710']},
      {values: ['Setembro/2017', '0,5360', '0,5755']},
      {values: ['Agosto/2017', '0,5416', '0,5853']},
      {values: ['Julho/2017', '0,5425', '0,5991']},
      {values: ['Junho/2017', '0,5957', '0,6233']},
      {values: ['Maio/2017', '0,6316', '0,6401']},
      {values: ['Abril/2017', '0,6496', '0,6496']},
      {values: ['Março/2017', '0,6782', '0,6839']},
      {values: ['Fevereiro/2017', '0,7128', '0,6899']},
      {values: ['Janeiro/2017', '0,7473', '0,6879']},
      {values: ['Dezembro/2016', '0,7839', '0,6819']},
      {values: ['Novembro/2016', '0,7797', '0,6624']},
      {values: ['Outubro/2016', '0,7435', '0,6459']},
      {values: ['Setembro/2016', '0,6887', '0,6273']},
      {values: ['Agosto/2016', '0,6461', '0,6122']},
      {values: ['Julho/2016', '0,6269', '0,6028']},
      {values: ['Junho/2016', '0,6154', '0,5926']},
      {values: ['Maio/2016', '0,5749', '0,5812']},
      {values: ['Abril/2016', '0,5881', '0,5881']},
      {values: ['Março/2016', '0,6856', '0,5552']},
      {values: ['Fevereiro/2016', '0,6991', '0,5485']},
      {values: ['Janeiro/2016', '0,6810', '0,5354']},
      {values: ['Dezembro/2015', '0,6319', '0,5183']},
      {values: ['Novembro/2015', '0,5986', '0,5044']},
      {values: ['Outubro/2015', '0,5467', '0,4902']},
      {values: ['Setembro/2015', '0,5007', '0,4793']},
      {values: ['Agosto/2015', '0,4731', '0,4741']},
      {values: ['Julho/2015', '0,4653', '0,4734']},
      {values: ['Junho/2015', '0,4675', '0,4765']},
      {values: ['Maio/2015', '0,4737', '0,4820']},
      {values: ['Abril/2015', '0,4909', '0,4909']},
    ];
    */

    return (
      <View style={{ backgroundColor: '#F7F7F7' }}>
        <ScrollView style={styles.scrollViewContainer}>
          <View>
            <Text style={styles.title}>Consecana SP</Text>
          </View>
          {this.state.isLoading ?
            <View style={{ flex: 1 }}>
              <Spinner visible={true} textStyle={{ color: '#FFF' }} />
            </View>
            :
            <IndicatorTableReport
              title={'Consecana SP'}
              labels={labels}
              values={values}
              firstColDoubleSize={true}
            />
          }
        </ScrollView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  scrollViewContainer: {

  },
  container: {
    color: '#cc00ff',
    justifyContent: 'center',
    alignItems: 'center',
  },
  topContainer: {
    flexDirection: 'row',
    margin: 0,
    marginTop: 10,
    justifyContent: 'space-around',
    alignItems: 'center',
    marginBottom: 10,

  },
  tab: {
    padding: 0,
    alignItems: 'center',

  },
  activeIconStyle: {

  },
  inactiveIconStyle: {

  },

  activeTextStyle: {

  },
  inactiveTextStyle: {

  },
  img: {
    borderColor: '#D4D4D4',
    width: 190,
    height: 52
  },
  title: {
    fontSize: 23,
    marginTop: 10,
    marginBottom: 15,
    marginLeft: 15,
    fontWeight: 'bold',
    color: '#005499',
    fontFamily: 'Montserrat',
  },
  rate: {
    fontSize: 13,
    fontWeight: 'bold',
    color: 'black',
    fontFamily: 'Montserrat'
  },
  middle: {
    backgroundColor: '#EBEBEB',
    alignItems: 'center',
    justifyContent: 'center',
    fontFamily: 'Montserrat'
  },
  middleTitle: {
    color: '#005499',
    fontSize: 32,
    fontWeight: 'bold',
    paddingTop: 20,
    paddingBottom: 8,
    alignItems: 'center',
    justifyContent: 'center',
    fontFamily: 'Montserrat'
  },
  accountimg: {
    width: 45,
    height: 45,
    justifyContent: 'flex-end',
    alignItems: 'flex-end',
    marginRight: 30,
    fontFamily: 'Montserrat',
  },
  account: {
    justifyContent: 'flex-end',
    alignItems: 'flex-end',
    marginTop: 10,
    height: 60
  },
  Luis: {
    justifyContent: 'flex-end',
    alignItems: 'flex-end',
    color: '#005499',
    fontSize: 14,
    marginRight: 20
  },
  cardBox: {
    marginTop: 18,
    marginBottom: 18,
  },
  NameItem: {
    backgroundColor: 'white',
    paddingTop: 22,
    paddingBottom: 22,
    paddingLeft: 20,
    paddingRight: 20,
    marginLeft: 15,
    marginRight: 15,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 4,
    borderColor: '#F2F6F7',
    borderWidth: 2

  },
  name: {
    fontSize: 10,
    color: 'black',
    fontFamily: 'Montserrat',
  },
  boxTitle: {
    fontSize: 12,
    color: 'black',
    fontFamily: 'Montserrat',
    fontWeight: 'bold',
    marginBottom: 5
  },
  goalName: {
    color: '#00D5F7',
    fontSize: 50,
    fontWeight: 'bold',
    marginTop: -8,
    fontFamily: 'Montserrat',
  },
  ton: {
    fontWeight: 'bold',
    fontSize: 20,
    color: '#005499',
    fontFamily: 'Montserrat',
  },
  goalton: {
    color: '#005499',
    fontSize: 12,
    fontWeight: 'bold',
    fontFamily: 'Montserrat',
  },
  row: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    marginTop: -20,
    marginLeft: 15,
    marginRight: 15,
    flex: 1
  },
  column: {
    flexDirection: 'row',
    width: width
  },
  gridRowContent: {
    fontSize: 8,
    padding: 0,
    margin: 0
  },
  TextContent1Small: {
    backgroundColor: 'white',
    fontSize: 9,
    marginLeft: 1,
    marginTop: 20,
    paddingTop: 15,
    paddingBottom: 15,
    fontWeight: 'bold',
    paddingLeft: 18,
    color: 'black',
    fontFamily: 'Montserrat',
  },
  TextContent1: {
    backgroundColor: 'white',
    fontSize: 12,
    marginLeft: 1,
    marginTop: 20,
    paddingTop: 15,
    paddingBottom: 15,
    fontWeight: 'bold',
    paddingLeft: 18,
    color: '#005499',
    fontFamily: 'Montserrat',
  },
  NumberContent1: {
    backgroundColor: 'white',
    fontSize: 12,
    marginRight: 1,
    marginTop: 20,
    paddingTop: 15,
    paddingBottom: 15,
    fontWeight: 'bold',
    justifyContent: 'flex-end',
    color: '#005499',
    fontFamily: 'Montserrat',
  },
  TextContent2: {
    backgroundColor: 'white',
    fontSize: 12,
    paddingTop: 15,
    paddingBottom: 15,
    paddingLeft: 30,
    marginLeft: 1,
    marginTop: 20,
    fontWeight: 'bold',
    color: '#FF892A',
    alignItems: 'center',
    justifyContent: 'center',
    fontFamily: 'Montserrat',
  },
  NumberContent2: {
    backgroundColor: 'white',
    fontSize: 12,
    marginRight: 1,
    marginTop: 20,
    paddingTop: 15,
    paddingBottom: 15,
    fontWeight: 'bold',
    justifyContent: 'flex-end',
    color: '#FF892A',
    fontFamily: 'Montserrat',
  },
  goalmemo: {
    marginTop: 40,
    fontSize: 8,
    justifyContent: 'flex-start',
    paddingRight: 80,
    alignItems: 'flex-start',
    fontFamily: 'Montserrat',
  },
  name2: {
    fontWeight: 'bold',
    fontSize: 26,
    color: '#005499',
    fontFamily: 'Montserrat',
  },
  goalName2: {
    color: '#00D5F7',
    fontSize: 50,
    fontWeight: 'bold',
    marginTop: -4,
    fontFamily: 'Montserrat',
  },
});
