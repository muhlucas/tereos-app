#!/bin/bash

set -e

SOURCE_PATH="$1"
VERSION=1.0.0
ANDROID_ASSETS_DIR="android/app/src/main/res/"

usage() {
cat << EOF
VERSION: $VERSION
USAGE:
    $0 <source_dir>
DESCRIPTION:
    This script moves Android images to the correct directory.
    <source_dir>	The source image directory.
AUTHOR:
    Guilherme Ocampos <guilss@gmail.com>
LICENSE:
    This script is licensed under the terms of the MIT license.
EXAMPLE:
    $0 src/assets/img/
EOF
}

# Check param
if [[ $# -lt 1 ]] ; then
	usage
	exit 1
fi

if [ ! -d "$SOURCE_PATH" ] ; then
	error "source directory doesn't exist"
	exit 1
fi

count=0
total=($1/*.*)
total=${#total[@]}

for image in $SOURCE_PATH/*.png; do
	name=$(basename "$image"); ext="${name##*.}"; name="${name%.*}"; opt="";
  image=`echo $image | sed "s/\/\//\//"`
  original_path=`echo $SOURCE_PATH | echo $image`
  new_file=`echo "$image" | sed "s/\//_/g" | sed "s/__/_/g"`

  for image_dir in $ANDROID_ASSETS_DIR/*; do
    dir_name=$(basename "$image_dir")
    if [ -d "$image_dir" ]; then
      if [ "$dir_name" != "values" ]; then
        image_dir=`echo $image_dir | sed "s/\/\//\//"`
        command="cp ${original_path} ${image_dir}/${new_file}"
        echo $command
        eval $command
      fi
    fi
  done

  #echo "${original_path} ${new_path}"
	(( count = count + 1 ))
	percent=$((count * 100 / total))
	# echo -ne "Copying assets: ${percent}%\r"
  # echo "Copying assets: ${percent}%"
done
