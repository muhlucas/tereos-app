import React, { Component } from 'react';
import { AsyncStorage, NetInfo, Platform } from 'react-native';
import Settings from '../components/app/settings';

export const getConnectionInfo = async () => {
  if (Platform.OS === 'ios') {
    return new Promise((resolve, reject) => {
      const connectionHandler = connectionInfo => {
        NetInfo.removeEventListener('connectionChange', connectionHandler)

        resolve(connectionInfo)
      }

      NetInfo.addEventListener('connectionChange', connectionHandler)
    })
  }

  return NetInfo.getConnectionInfo()
}

export default Api = {


  get: async function(url, callback, requestNewToken) {
    let _this = this;

    console.log('url get', url);
    let strArguments = JSON.stringify(arguments);
    let md5Arguments = Helper.md5(strArguments);
    let md5Key = 'url_get:' + md5Arguments;

    return new Promise (
      async function(resolve, reject) {

        const netinfo = await getConnectionInfo();

        console.log('url netinfo', netinfo);

        console.log('url arguments: ', strArguments);
        console.log('url md5 arguments: ', md5Arguments);

        let notConnected = (netinfo.type == 'none');
        // let notConnected = (netinfo.type == 'none' || netinfo.type == 'unknown');

        // notConnected = true;
        if(notConnected) {
          cachedInfo = await AsyncStorage.getItem(md5Key);
          if(!cachedInfo) {
            return resolve(null);
          }

          responseJson = JSON.parse(cachedInfo);
          console.log('url cached returned', responseJson);
          return resolve(responseJson);
        } else {
          console.log('url not cached', url);
        }

        if(typeof requestNewToken == 'undefined' || requestNewToken == false) {
          requestNewToken = false;
          token = await AsyncStorage.getItem('@api_token');

          if(token == null || token.indexOf('erro') != -1) {
            requestNewToken = true;
          }

          console.log('url result token existent', token);
        }

        if(requestNewToken == true) {
          token = await _this.login();
          token = (token) ? token : '';
          await AsyncStorage.setItem('@api_token', token);
          // console.log('result token new ', token);
        }

        let fullUrl = Settings.urlApi + url;

        fetch(fullUrl, {
            headers: {
              Authorization: 'Bearer ' + token
            },
          })
          .then(function(response) {
            // console.log('response token', response);
            // console.log('requestNewToken token', requestNewToken);
            // console.log('response', response);
            if (response.status === 200) {
              // alert('response 1: ' + JSON.stringify(response))
              return response.json()
            } else if( response.status == '401' && requestNewToken != true) {
              return _this.get(url, callback, true)
            }
          })
          .then((responseJson) => {

            jsonToStore = (responseJson) ? JSON.stringify(responseJson) : '';
            console.log('url caching ' + url, md5Key);
            AsyncStorage.setItem(md5Key, jsonToStore);

            resolve(responseJson);
          })
          .catch((error) => {
            console.log('error', error);
            reject(error);
          });
      }
    );
  },

  post: async function(url, params, callback, requestNewToken, returnType) {
    let _this = this;

    if(typeof requestNewToken == 'undefined' || requestNewToken == false) {
      requestNewToken = false;
      token = await AsyncStorage.getItem('@api_token');

      if(token == null || token.indexOf('erro') != -1) {
        requestNewToken = true;
      }
    }

    if(requestNewToken == true) {
      token = await _this.login();
      token = (token) ? token : '';
      await AsyncStorage.setItem('@api_token', token);
    }

    let fullUrl = Settings.urlApi + url;

    console.log('url post', fullUrl);
    console.log('url post params', params);

    return new Promise (
      function(resolve, reject) {
          fetch(fullUrl, {
            method: 'POST',
            headers: {
              Authorization: 'Bearer ' + token,
              Accept: 'application/json',
             'Content-Type': 'application/json',
            },
            body: (typeof params == 'string') ? params : JSON.stringify(params),
          })
          .then(function(response) {
            console.log('response token', response);
            // console.log('requestNewToken token', requestNewToken);
            console.log('url post response.status', response.status);
            if (response.status === 200) {
              if(typeof returnType != 'undefined' && returnType == 'text') {
                return response.text();
              }
              return response.json();
            } else if( response.status == '401' && requestNewToken != true) {
              return _this.post(url, params, callback, true)
            }

            console.log('url responseJSON', url);
            resolve(response.text());
            return true;
          })
          .then((responseJson) => {
            resolve(responseJson);
          })
          .catch((error) => {
            console.log('error', error);
            reject(error);
          });
          /* .catch((error) => {
            console.log('error', error);
            reject(error);
          }); */
      }
    );
  },

  put: async function(url, params, callback, requestNewToken, returnType) {
    let _this = this;

    if(typeof requestNewToken == 'undefined' || requestNewToken == false) {
      requestNewToken = false;
      token = await AsyncStorage.getItem('@api_token');

      if(token == null || token.indexOf('erro') != -1) {
        requestNewToken = true;
      }
    }

    if(requestNewToken == true) {
      token = await _this.login();
      token = (token) ? token : '';
      await AsyncStorage.setItem('@api_token', token);
    }

    console.log('url put', url);
    console.log('url put params', params);

    return new Promise (
      function(resolve, reject) {
          fetch(Settings.urlApi + url, {
            method: 'PUT',
            headers: {
              Authorization: 'Bearer ' + token,
              Accept: 'application/json',
             'Content-Type': 'application/json',
            },
            body: (typeof params == 'string') ? params : JSON.stringify(params),
          })
          .then(function(response) {
            console.log('response token', response);
            // console.log('requestNewToken token', requestNewToken);
            console.log('url put response.status', response.status);
            if (response.status === 200) {
              if(typeof returnType != 'undefined' && returnType == 'text') {
                return response.text();
              }
              return response.json();
            } else if( response.status == '401' && requestNewToken != true) {
              return _this.put(url, params, callback, true)
            }

            console.log('url responseJSON', url);
            resolve(response.text());
            return true;
          })
          .then((responseJson) => {
            resolve(responseJson);
          })
          .catch((error) => {
            console.log('error', error);
            reject(error);
          });
          /* .catch((error) => {
            console.log('error', error);
            reject(error);
          }); */
      }
    );
  },

  login: async function() {
    const value = await AsyncStorage.getItem('@login');
    valueObj = JSON.parse(value);

    let fullUrl = Settings.urlApi + 'api/usuario/login';
    console.log('url', fullUrl);

    return new Promise(
      function(resolve, reject) {
        return fetch(fullUrl, {
          method: 'POST',
          headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
          },
          body: JSON.stringify(
            {
            "CPF": valueObj.cpf,
            "Senha": valueObj.password,
            "ManterConectado": 1
          }),
        })
        .then((response) => {
          return response.json();
        })
        .then((responseJson) => {
          if(responseJson == 'Usuário sem permissão') {
            throw 'Usuário sem permissão';
          }
          console.log('url login responseJSON', responseJson);
          resolve(responseJson);
        })
        .catch((error) => {
          // console.error(error);
          reject(error);
        });
      });
  },

  updatePlayerId(id, playerId) {
    return this.post('api/usuario/atualizarplayer', {Cpf: id, 'Playerid': playerId}, null, false, 'text');
  },

  removePlayerId(id, playerId) {
    return this.post('api/usuario/removerplayer', {Cpf: id, 'Playerid': playerId}, null, false, 'text');
    // return this.put('api/usuario/removerplayer', id, null, false, 'text');
  },

  getMainReport: function(id, year, month) {
    return this.get('api/financeiro/producaosintetico/' + id + '/' + year + '-' + month);
  },

  getReportMonth: function(id, year) {
    // return this.get('api/dados?cpfFornecedor=01508342849&cpfGestor=07750093850&ano=' + year);
    return this.get('api/dados?cpfFornecedor=' + id + '&ano=' + year);
  },

  getIndexes: function() {
    return this.get('api/indice/listartodos');
    // return this.get('api/Indice?dataIni=01-07-2018&dataFim=01-07-2018&flagConsecana=U');
    // return this.get('api/Indice?dataIni=' + startDate + '&dataFim=' + endDate + '&flagConsecana=U');
  },

  getLastConsecanaIndex: function() {
    return this.get('api/indice/listarconsecana/U');
  },

  getFullConsecanaIndexes: function() {
    return this.get('api/indice/listarconsecana/T');
  },

  getEvents: async function(id) {
    return this.get('api/evento/eventosusuariologado/' + id);
    // return this.get('api/Evento');
  },

  confirmEventInterest(eventId, id) {
    console.log('url ok', 'confirmEventInterest');
    return this.post('api/evento/alterarstatus', {"CPF": id, "IdEvento": eventId, "IdStatus": 1}, null, false, 'text');
  },

  confirmEventPresence(eventId, id) {
    // return this.post('api/eventos/alterarstatus?IdUsuarioConvidado=' + id + '&IdEvento=' + eventId + '&IdStatus=1');
    return this.post('api/evento/alterarstatus', {"CPF": id, "IdEvento": eventId, "IdStatus": 2}, null, false, 'text');
  },

  removeEventPresence(eventId, id) {
    // return this.post('api/eventos/alterarstatus?IdUsuarioConvidado=' + id + '&IdEvento=' + eventId + '&IdStatus=1');
    return this.post('api/evento/alterarstatus', {"CPF": id, "IdEvento": eventId, "IdStatus": 3}, null, false, 'text');
  },

  getPlants(id, year) {
    return this.get('api/dados/buscartodasusinasdousuariologado/' + year + '/' + id)
    // return this.get('api/fornecedor/recuperarusinas');
  },

  getFinancialReportsByYear(id, plant, year) {
    if(plant == 'TODAS') {
      return this.get('api/financeiro/buscarporusinananossumarizada/' + id + '/' + year + '/3');
    }
    return this.get('api/financeiro/buscarporusinaxanos/' + id + '/' + plant + '/' + year + '/3');
  },

  getFinancialReportsByMonth(id, plant, year, month) {
    if(plant == 'TODAS') {
      return this.get('api/financeiro/buscarporusinasumarizada/' + id + '/' + year + '/' + month + '/4');
    }
    // return this.get('api/financeiro/buscarporusina/16051122834/' + plant + '/' + year + '/' + month);
    return this.get('api/financeiro/listarporusina/' + id + '/' + plant + '/' + year + '/' + month + '/4');
  },

  getDailyProductionReport(id, year, month) {
    // return this.get('api/financeiro/producaomensal/16051122834/' + year + '-' + month);
    return this.get('api/financeiro/producaomensal/' + id + '/' + year + '/' + month);
  },

  getHomeInfo(id, year, month) {
    // return this.get('api/dados/mensal/05266880000247/' + year + '/' + month);
    return this.get('api/dados/mensal/' + id + '/' + year + '/' + month);
  },

  getMonthlySummaryData(id, year, month) {
    return this.get('api/financeiro/homesumarizadosmensal/' + id + '/' + year + '/' + month);
    // return this.get('api/financeiro/buscarsumarizadomensal/' + id + '/' + year + '/' + month);

    // return this.get('api/financeiro/buscarsumarizadomensal/21465048898/' + year + '/' + month);
  },

  getAnualSummaryData(id, year) {
    return this.get('api/financeiro/homesumarizadosanual/' + id + '/' + year);
    // return this.get('api/financeiro/buscarsumarizadoanual/21465048898/' + year);
  },

  getFarms(id, year) {
    return this.get('api/dados/buscartodasfazendas/' + year + '/' + id);
  },

  getFarmSummaryReportsByYear(id, year) {
    return this.get('api/financeiro/buscarporfazendasumarizadoxanos/' + id + '/' + year + '/3');
  },

  getFarmSummaryReportsByMonth(id, year, month) {
    return this.get('api/financeiro/buscarporfazendamensalsumarizado/' + id + '/' + year + '/' + month + '/3');
  },

  getFarmReportsByYear(id, farm, year) {
    return this.get('api/financeiro/buscarporfazendaxanos/' + id + '/' + farm + '/' + year + '/3');
  },

  getFarmReportsByMonth(id, farm, year, month) {
    return this.get('api/financeiro/buscarporfazendaxmeses/' + id + '/' + farm + '/' + year + '/' + month + '/3');
  },

  getProfile(id, infoFornecedor = true) {
    return this.get('api/usuario/' + id + "?infoFornecedor="+infoFornecedor);
  },

  updateProfile(id) {
    return this.put('api/usuario', values, null, false, 'text');
  },

  getNews(startDate, endDate, pageIndex) {
    return this.get('api/Noticia?dtini=' + startDate + '&dtfim=' + endDate + '&pageIndex=' + pageIndex + '&pageSize=3');
  },

  getNewsDetail(id) {
    return this.get('api/noticia/' + id);
  },

  resetPassword(id) {
    return this.post('api/usuario/recuperarsenha', id);
  },

  getCities(state) {
    return this.get('api/usuario/listarcidades/' + state);
  },

  getNotifications: function(id) {
    return this.get('api/push/configuracao/notificacoes/' + id);
  },

  updateNotification: function(cpf, codigo, value) {
    return this.post('api/push/configuracao/notificacoes', {"Codigo" : codigo, "Ativo" : value, "CPF" : cpf});
  },

}
