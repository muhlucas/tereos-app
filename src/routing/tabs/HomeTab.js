import React, { Component } from 'react';
import { AsyncStorage, StyleSheet, Text, TouchableWithoutFeedback, Image, View, Dimensions} from 'react-native';
import PropTypes from 'prop-types';

var width = Dimensions.get('window').width;
var height = Dimensions.get('window').height;

class HomeTab extends Component {
    constructor(props) {
      super(props);
    }

    render() {
        const { navigate, state } = this.props.navigation;

        console.log('rendered', state);

        var tab2_active = (state.routeName == 'HomeScreen2');
        var tab1_active = (state.routeName == 'HomeScreen1') || (!tab2_active);

        return (
          <View style={styles.topContainer}>
              <TouchableWithoutFeedback onPress={() => {  navigate('HomeScreen1'); }}>
                  <View style={styles.tab}>
                      <Image style={styles.img}
                          source={tab1_active ? require('../../assets/img/button1.png') : require('../../assets/img/button11.png')}
                      />
                  </View>
              </TouchableWithoutFeedback>

              <TouchableWithoutFeedback onPress={() => {  navigate('HomeScreen2'); }}>
                  <View style={styles.tab}>
                      <Image style={styles.img}
                      source={tab2_active ? require('../../assets/img/button2.png') : require('../../assets/img/button22.png')}
                      />
                  </View>
              </TouchableWithoutFeedback>
          </View>
        );
    }
}

HomeTab.propTypes = {
    // start react-navigation props
    navigation: PropTypes.object.isRequired
};

export default HomeTab;


const styles = StyleSheet.create({
    topContainer: {
        flexDirection: 'row',
        margin:0,
        marginTop:10,
        justifyContent:'space-around',
        alignItems: 'center',
        marginBottom:10,
        backgroundColor: '#F7F7F7'
    },
    tab: {
        padding:0,
        alignItems: 'center',
    },
    img:{
      borderColor:'#D4D4D4',
      width:width*0.45,
      height:(width*0.45)/3.46
    },
});
