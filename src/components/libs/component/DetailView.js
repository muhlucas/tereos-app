import React, { Component } from 'react';
import {View,Text,ScrollView,TouchableOpacity} from 'react-native';
import Image from 'react-native-remote-svg';
import { StyleSheet} from 'react-native';
import PropTypes from 'prop-types';

const styles = StyleSheet.create({
  img:{
    width:'100%',
    height:282,
  },
  textTitle:{
    fontSize:9,
    color:'white',
    marginTop:32,
    marginLeft:20,
    marginBottom:10,
    color:'#0c4F77',
  },

  textContent:{
    fontSize:20,
    color:'white',
    marginTop:-10,
    marginLeft:20,
    color:'#0c4F77',
    fontWeight:'bold',
    fontFamily: 'Montserrat',
    lineHeight:37
  },

  textProfile:{
    fontSize:12,
    paddingLeft:20,
    paddingRight:20,
    fontFamily: 'Montserrat',
    marginTop:30,
    lineHeight:16
  }
  ,close:{
    height:53,
    padding:17
  },img1:{
    width:84,
    height:21
  }
});


export default class DetailView extends Component {

  static propTypes = {
      navigation: PropTypes.shape({
          navigate: PropTypes.func
      })
  }

  static defaultProps = {
      navigation: {
          navigate: () => {}
      }
  }

  constructor(){
      super();
      this.onClose = this.onClose.bind(this);
      this.onNext = this.onNext.bind(this);
  }

  onClose() {
      this.props.navigation.goBack();
  }
  onNext() {
      this.props.navigation.navigate('DetailViewScreen');
  }



  render() {
    return (
      <View>
        <ScrollView>
          <View>

              <TouchableOpacity style={styles.close} onPress={this.onClose}>
                <Image style={styles.img1}
                    source={require('../../../assets/img/close.png')}
                />
              </TouchableOpacity>

              <Image style={styles.img}
                  source={require('../../../assets/img/pic1.png')}
              />
              <Text style={styles.textTitle}>10/10/2017 - Conteúdo Exclusivo</Text>
              <Text style={styles.textContent}>SEM ROTA 2030, ANFAVEA</Text>
              <Text style={styles.textContent}>PREVE MENOS INVESTIMENTO</Text>

          </View>


        <View>

            <Text style={styles.textProfile}>A demora do governo em anunciar o Rota 2030, programa que teria duração de 15 anos, pode resultar em redução de investimentos das montadoras no Brasil, afirma o presidente da Anfavea, Antonio Megale.
            </Text>
            <Text style={styles.textProfile}>O Rota, por exemplo, prevê incentivo fiscal anual de R$ 1,5 bilhão (o mesmo aplicado no Inovar-Auto, que terminou em dezembro), para projetos de pesquisa e desenvolvimento (P&D).
                Para ele, o maior entrave não é o valor, pois “o setor gera, por ano, cerca de R$ 40 bilhões em impostos”.
              O problema está na forma como o incentivo seria concedido.
            </Text>
            <Text style={styles.textProfile}>Uma das propostas do governo é usar a Lei do Bem, que abate o incentivo do Imposto de Renda a pagar.
                “O problema é que as empresas ainda estão dando prejuízo e, por isso, não vão pagar Imposto de Renda”, afirma Megale.
                </Text>
            <Text style={styles.textProfile}>Sem o incentivo, o investimento em P&D sairá do Brasil e voltará para as matrizes no exterior, prevê o executivo.
                Com isso, o investimento em etanol no País seria abandonado.
                </Text>
            <Text style={styles.textProfile}>Outra medida do Rota é estabelecer metas de eficiência energética para carros que circulam no País.
                No Inovar-Auto, a meta era reduzir o consumo em 12% e, quem ultrapassou esse porcentual obteve desconto extra no IPI.
                As montadoras alegam que fizeram elevados investimentos para atingir esses níveis e, sem a definição da próxima etapa, a alocação de novos recursos fica prejudicada
                </Text>
            <Text style={styles.textProfile}>O sistema Meiosi é muito econômico. É um sistema que tem uma alta rentabilidade e que pode trazer grandes vantagens tanto à usina como ao fornecedor.
              </Text>
            <Text style={styles.textProfile}>“A decisão da montadora vai depender de com qual cenário vamos trabalhar.
                 Se não há necessidade de um certo nível de eficiência, as montadoras podem reduzir investimentos”, diz o presidente da Anfavea.
                 </Text>
            <Text style={styles.textProfile}>Para Dan Ioschpe, presidente do Sindicato da Indústria de Componentes para Veículos (Sindipeças), sem novas regras para eficiência energética há o risco de “nosso mercado ser invadido por produtos” que não cumprem as exigências atuais.
                 Eles chegariam com preços abaixo dos modelos nacionais que incorporaram novas tecnologias para reduzir o consumo.
                 </Text>
            <Text style={styles.textProfile}>Em sua opinião, aspectos do Rota já definidos e que têm consenso de todos os formuladores do programa já poderiam entrar em vigor.</Text>

        </View>

        </ScrollView>
      </View>
    );
  }
}
