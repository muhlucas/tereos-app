import React, { Component } from 'react';
import {
    Text,
    View,
    Dimensions,
    ImageBackground,
    TextInput,
    TouchableOpacity,
    Alert,
    AlertIOS,
    Platform,
} from 'react-native';
import { Button } from 'native-base';
import Image from 'react-native-remote-svg';
import Spinner from 'react-native-loading-spinner-overlay';

import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';

import styles from './styles';

var width = Dimensions.get('window').width;
var height = Dimensions.get('window').height;

export default class ForgotPassword extends Component {

  constructor(props) {
    super(props);

    this.state = {
      cpf: null
    }

    this.sendPassword = this.sendPassword.bind(this);
  }

  sendPassword() {
    let _this = this;
    let cpf = this.state.cpf;

    this.setState({isLoading: true}, function() {
      Api.resetPassword(cpf).
      then(function(data) {
        if(data == 'Email enviado para o usuário') {
          message = 'Uma nova senha foi enviada para o seu e-mail';
        } else {
          message = data;
        }

        _this.setState({isLoading: false}, function() {
          setTimeout(() => {
                alert(message);
          }, 100);
        });
      });
    });
  }

  render() {
    const { switchValue } = this.state;
    const { navigate, state } = this.props.navigation;

    return (
      <KeyboardAwareScrollView style={styles.container} contentContainerStyle={{alignItems:'center'}} behavior="padding" enabled>
          <Image source={require('../../../assets/img/bg2.png')} style={styles.backgroundImage} />

              <View style={styles.boxBackButton}>
                <Button transparent onPress={() => navigate('Login')}>
                  <Image style={styles.backIcon}
                    source={require('../../../assets/img/back.png')}
                  />
                </Button>
              </View>
              <View style={{height:height}}>
              <Image style={[styles.logo, {marginTop:(height*0.15)-25.5}]}
                  source={require('../../../assets/img/logo_full.png')}
              />
              <View style={styles.loginForm}>

              {this.state.isLoading ?
              <View style={{ flex: 1 }}>
                <Spinner visible={true} textStyle={{color: '#FFF'}} />
              </View>
              : null }

              <View>
                <Text style={styles.headerAccountAccess}>Esqueceu sua senha?</Text>
              </View>

              <View>
                <Text style={styles.text}>Digite seu CPF ou nome de usuário abaixo para receber instruções por email sobre recuperação de senha:</Text>
              </View>

              <TextInput style={styles.TextInput}
                placeholder="CPF ou Nome de usuário"
                underlineColorAndroid='transparent'
                onChangeText={(text) => this.setState({cpf: text})}
              />

              <TouchableOpacity
                  style={[styles.button, {marginTop:25}]}
                  onPress={() => this.sendPassword()}
              >
                <Text style={styles.textButton}>Enviar</Text>
              </TouchableOpacity>
            </View>
          </View>
      </KeyboardAwareScrollView>
    );
  }
}
