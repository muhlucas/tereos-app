import React, { Component } from 'react';
import { AsyncStorage } from 'react-native';

export default User = {

  getUser: async function() {
    result = await AsyncStorage.getItem('@login');
    data = JSON.parse(result);
    return data;
  },

  getId: async function() {
    result = await AsyncStorage.getItem('@login');
    data = JSON.parse(result);
    return data.cpf;
  },

  getManagerId: async function() {
    result = await AsyncStorage.getItem('@login');
    data = JSON.parse(result);
    return data.managerId;
  },

  setPlayerId: async function(playerId) {
    result = await AsyncStorage.setItem('@player_id', playerId);
    return result;
  },

  getPlayerId: async function() {
    result = await AsyncStorage.getItem('@player_id');
    return result;
  },

  removePlayerId: async function() {
    result = await AsyncStorage.removeItem('@player_id');
    return result;
  },


  enableNotificationPreference: async function() {
    result = await AsyncStorage.setItem('@notification', 'ok');
    return result;
  },

  getNotificationPreference: async function() {
    result = await AsyncStorage.getItem('@notification');
    return result;
  },

  removeNotificationPreference: async function() {
    result = await AsyncStorage.removeItem('@notification');
    return result;
  }
}
